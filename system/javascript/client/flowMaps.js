/*
controllerClient.js

basics, to fork 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2022

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

'use strict'

// core elements 
import OSAP from '../osapjs/core/osap.js'
import TIME from '../osapjs/core/time.js'
import PK from '../osapjs/core/packets.js'

// virtual machines 
import PowerSwitchVM from '../osapjs/vms/powerSwitches.js'
import AXLActuator from '../osapjs/vms/axlActuator.js'
import TempVM from '../osapjs/vms/tempVirtualMachine.js'
import LoadVM from '../osapjs/vms/loadcellVirtualMachine.js'
import FilamentSensorVM from '../osapjs/vms/filamentSensorVirtualMachine.js'

import { SaveFile } from '../osapjs/client/utes/saveFile.js'

// ui elements 
import Grid from '../osapjs/client/interface/grid.js' // main drawing API 
import { Button, EZButton, TextBlock, TextInput } from '../osapjs/client/interface/basics.js'
import TempPanel from '../osapjs/client/components/tempPanel.js'
import AutoPlot from '../osapjs/client/components/autoPlot.js'
import AXLCore from '../osapjs/vms/axlCore.js'

console.log(`------------------------------------------`)
console.log("hello lpf controller")

// -------------------------------------------------------- The main UI... thing 

let grid = new Grid()

// -------------------------------------------------------- OSAP Object
let osap = new OSAP("flow-mapper")

// -------------------------------------------------------- SETUP NETWORK / PORT 
let wscVPort = osap.vPort("wscVPort")

// -------------------------------------------------------- Ute to switch system power at the motion-head circuit

// this one is built "the new way" 
// the psu-head / modular-motion-head board: which is just a bus router and power switches to us, 
let powerSwitchVM = new PowerSwitchVM(osap)

// these are older code, we want global vars for but can't constructor them until we have a route:
// the extruder motor 
let extruderMVM = {}
let effectiveDriveGearDiameter = 8.2 * 0.9
// temperature / heater module 
let tempVM = {}
// filament sensor 
let fsVM = {}
// load sensor, 
let loadVM = {}

// -------------------------------------------------------- Setup Code 

let setup = async () => {
  try {
    console.warn(`SETUP: finding switches...`)
    await powerSwitchVM.setup()
    console.warn(`SETUP: cycling power...`)
    await powerSwitchVM.setPowerStates(false, false)
    await TIME.delay(500)
    await powerSwitchVM.setPowerStates(true, false)
    await TIME.delay(250)
    await powerSwitchVM.setPowerStates(true, true)
    await TIME.delay(2500)
    // start a-lookin, 
    console.warn(`SETUP: collecting graph...`)
    let graph = await osap.nr.sweep()
    // collect and setup the extruder;
    console.warn(`SETUP: collecting extruder motor...`)
    extruderMVM = new AXLActuator(
      osap,
      PK.VC2VMRoute((await osap.nr.find("rt_axl-stepper_e", graph)).route),
      {
        name: "rt_axl-stepper_e",
        accelLimits: [1000, 1000, 1000, 1000], // should become a 4dof axl instance, derp... 
        velocityLimits: [100, 100, 100, 100],
        queueStartDelay: 500,
        actuatorID: 0,
        axis: 0,
        invert: false,
        microstep: 16,
        spu: (16 * 200) / (effectiveDriveGearDiameter * Math.PI),
        cscale: 0.55
      })
    await extruderMVM.setup()
    // collect and setup tempvm, 
    console.warn(`SETUP: collecting heater module...`)
    tempVM = new TempVM(osap, PK.VC2VMRoute((await osap.nr.find("rt_heater-module", graph)).route))
    console.warn(`SETUP: collecting filament sensor...`)
    fsVM = new FilamentSensorVM(osap, PK.VC2VMRoute((await osap.nr.find("rt_filament-sensor", graph)).route))
    console.warn(`SETUP: collecting loadcells...`)
    loadVM = new LoadVM(osap, PK.VC2VMRoute((await osap.nr.find("rt_loadcell-amp", graph)).route))
    console.warn(`SETUP: DONE`)
    // comment / uncomment these three lines to run the experiment or just plot (to watch temps drop for safe power down)
    // runLoop = true 
    // plotLoop()
    // return 
    // ------------------------------ here's a new experiment: the spin-down, 
    // let temp = 290            // startup temp, 
    // let rates = [
    //   2, 5, 10, 15, 20 
    // ]
    // ... I would avoid rates below 10mm^3/sec for the 0.8mm shnozz: 
    // esp. where filaments are of lower quality / lower diameter, it tends to happen 
    // that as pressure increases, filament seeps back up around the cold stuff and clogs 
    // the heat brake... 
    // and I find most bonks happen around 700k loadcell reading... 
    // add 25, rm "2" for 0.8mm noz ? 
    await runSpinDownExperiment({
      temp: 290, 
      rate: 5,
      count: 2,
      materialName: "biopetg",
      nozSize: "04",
    })
    return 
    // ------------------------------ below is ye-olden experiment, 
    // here's the experiment; 
    // OK we're ready to restart w/ new sensor fw ... 
    let tempSamples = []
    let temp = 160
    // ... the await temps, laggy 
    // ... the early temps, we're still not sure about 
    while (temp <= 290) {
      // start each temp at rate = 1 
      let rate = 1
      // await temp setup here, 
      // first we would want to set a temp and wait for it,
      console.warn(`LOOP: set / await temp ${temp} ...`)
      runLoop = true
      plotLoop()
      await tempVM.setExtruderTemp(temp)
      // let's await like this... 
      await tempVM.awaitExtruderTemp(temp)
      // we should purge at our new temp, since last cycle likely shredded some filament 
      let dpt = await getDataPoint(temp, 0.5, 1000)
      // now we can collect at these temps, 
      let rateSamples = []
      while (rate <= 15) {
        // carry on... 
        let dpt = await getDataPoint(temp, rate, 2500)
        if ((dpt.rate / dpt.requestedRate) < 0.80 || dpt.broken) {
          rateSamples.push(dpt)
          console.log(`LOOP: breaking for slip at t: ${temp}, rate: ${rate}`)
          break;
        } else {
          rateSamples.push(dpt)
          rate += 1
        }
        // await TIME.delay(2000)
        // if((dpt.rate / dpt.requestedRate) < 0.80){
        //   console.warn(`LOOP: breaking at temp ${temp} and rate ${rate} for under-extrusion`)
        //   // pick a new rate, just knock it down 3 ticks, 
        //   rate -= 3
        //   if(rate < 1) rate = 1
        //   console.warn(`LOOP: new rate for ${rate}`)
        //   break
        // }
        // rate += 1 
        // if(rate < 5){
        //   rate += 1
        // } else {
        //   rate += 3
        // }
      } // end while-rates, 
      tempSamples.push(rateSamples)
      temp += 5
      // if(temp < 210){
      //   temp += 5
      // } else {
      //   temp += 20 
      // }
    } // end while-temps, 
    // for(let temp = 180; temp <= 260; temp += 10){
    //   let rateSamples = []
    //   for(let rate = 1; rate <= 15; rate += 2){
    //     let dpt = await getDataPoint(temp, rate, 10000)
    //     rateSamples.push(dpt)
    //     await TIME.delay(2000)
    //     if((dpt.rate / dpt.requestedRate) < 0.75){
    //       console.warn(`LOOP: breaking at ${rate} for under-extrusion`)
    //       break
    //     }
    //   }
    //   tempSamples.push(rateSamples)
    // }
    SaveFile(tempSamples, 'json', 'test-trials')
    console.log(`------------------------------------------`)
    await run()
  } catch (err) {
    console.error(err)
  }
}

let spinStop = false 

let stopSpinDownButton = new EZButton(10, 10, 100, 100, `halt spindown`)
stopSpinDownButton.onClick(() => {
  spinStop = true 
})

let runSpinDownExperiment = async (params) => {
  try {
    let temp = params.temp 
    // convert local rate (linear) from spec'd cubic mm/sec 
    let flowRate = params.rate
    let rate = flowRate / (((1.75/2) * (1.75/2)) * Math.PI)
    let count = params.count 
    let materialName = params.materialName
    let nozSize = params.nozSize 
    // do for two counts, 
    outerloop: for(let c = 0; c < count; c ++){
      console.warn(`SD: set / await ${temp} degs C to test rate ${flowRate} for ${materialName} with ${nozSize} dia`)
      runLoop = true 
      plotLoop()
      await tempVM.setExtruderTemp(temp)
      await tempVM.awaitExtruderTemp(temp)
      // let's purge for some time, 
      console.warn(`SD: purge...`)
      await extruderMVM.gotoVelocity([0.5, 0, 0, 0])
      await TIME.delay(15000)
      // now we setup the cooldown-and-run, 
      await extruderMVM.gotoVelocity([rate, 0, 0, 0])
      await TIME.delay(2000)
      await tempVM.setExtruderTemp(0)
      // marks, 
      let startTime = TIME.getTimeStamp()
      let runSamples = []
      // and gather-while, 
      let slipEstimate = 1 
      let slipAlpha = 0.15
      innerLoop: while(true){
        if(spinStop){
          spinStop = false 
          stopSpinDownButton.good()
          break innerLoop;
        }
        let stat = await gather()
        stat.requestedRate = rate 
        stat.time = TIME.getTimeStamp() - startTime 
        let slip = stat.rate / rate 
        runSamples.push(stat)
        slipEstimate = slipEstimate * (1 - slipAlpha) + slip * slipAlpha
        console.warn(`slip ${slip.toFixed(2)} : ${slipEstimate.toFixed(2)} : ${stat.time}`)
        await TIME.delay(100)
        if(slipEstimate < 0.75) break innerLoop;
      }
      // stap ! 
      await extruderMVM.gotoVelocity([0,0,0,0])
      // check runtime 
      let runTime = TIME.getTimeStamp() - startTime
      console.warn(`SD: done rate-run... was ${(runTime / 1000).toFixed(2)} secs`)
      SaveFile(runSamples, 'json', `spindown-${materialName}-${nozSize}-${flowRate.toFixed(2)}`)
    } // end while rate < maxRate 
    console.warn(`done parameter set...`)
    console.warn(`------------------------------------------`)
  } catch (err) {
    console.error(err)
  }
}

setTimeout(setup, 750)

// -------------------------------------------------------- UI Elements 

// -------------------------------------------------------- Temp Panel / Etc 

let tPanel = new AutoPlot(130, 10, 650, 230, 'hotend',
  { top: 40, right: 20, bottom: 30, left: 60 })
tPanel.setHoldCount(4000)
let rPanel = new AutoPlot(130, 250, 650, 230, 'rates',
  { top: 40, right: 20, bottom: 30, left: 60 })
rPanel.setHoldCount(4000)
let lPanel = new AutoPlot(130, 490, 650, 230, 'loads',
  { top: 40, right: 20, bottom: 30, left: 60 })
lPanel.setHoldCount(4000)

// ok then... a gather-data routine
let gather = async () => {
  try {
    let results = await Promise.all([tempVM.getExtruderTemp(), loadVM.getReading(false, true), fsVM.getReadings()])
    let time = TIME.getTimeStamp()
    // console.log(results[0])
    tPanel.pushPt([time, results[0]])
    tPanel.redraw()
    lPanel.pushPt([time, results[1][0]])
    lPanel.redraw()
    // rate is more complex; we have a wheel of some diameter, an 2^14 bits / revolution, so 
    let diameter = effectiveDriveGearDiameter // * 0.95
    // the above ~ should be true, since the same drive gear is on the encoder & the motor 
    // we take rates, etc, from the filament sensor in rads/sec, 
    let circ = diameter * Math.PI
    let radsToLinear = circ / (2 * Math.PI)
    let rate = results[2].rate * radsToLinear
    rPanel.pushPt([time, rate])
    rPanel.redraw()
    return {
      temp: results[0],
      load: results[1][0],
      rate: rate //results[2].rate
    }
  } catch (err) {
    throw err
  }
}

let runLoop = false
let plotLoop = async () => {
  try {
    while (runLoop) {
      await gather()
      await TIME.awaitFutureTime(TIME.getTimeStamp() + 100)
    }
  } catch (err) {
    console.error(err)
  }
}

let getDataPoint = async (temp, rate, sampleTime) => {
  try {
    console.warn(`GDP: set motor request, and purging`)
    await extruderMVM.gotoVelocity([rate, 0, 0, 0])
    // do a (very small pieces of) little purging, 
    await TIME.delay(500)
    runLoop = false
    await TIME.delay(100)
    console.warn(`GDP: collecting points at ${rate} mm/sec ...`)
    let stash = []
    let startTime = TIME.getTimeStamp()
    let lastGather = TIME.getTimeStamp()
    let slipCount = 0
    let broken = false
    while (TIME.getTimeStamp() - sampleTime < startTime) {
      // get the new point 
      let stat = await gather()
      stash.push(stat)
      let slip = stat.rate / rate
      if (slip < 0.75) {
        slipCount++
        console.warn(`slip ${slip.toFixed(2)}`)
      } else {
        console.log(`slip ${slip.toFixed(2)}`)
      }
      if (slipCount > 2) {
        console.error(`break cond`)
        broken = true
        break;
      }
      // so, we could / should do some breaking above, likely ? 
      // hold-up until 100ms since previous collection, 
      await TIME.awaitFutureTime(lastGather + 100)
      lastGather = TIME.getTimeStamp()
    }
    // calcs: 
    await extruderMVM.gotoVelocity([0, 0, 0, 0])
    // next... would work out averages & resolve them in a return, 
    let avgTemp = 0
    let avgLoad = 0
    let avgRate = 0
    for (let i = 0; i < stash.length; i++) {
      avgTemp += stash[i].temp
      avgLoad += stash[i].load
      avgRate += stash[i].rate
    }
    avgTemp /= stash.length
    avgLoad /= stash.length
    avgRate /= stash.length
    console.warn(`GDP: collected ${stash.length} data pts at ${temp}, ${rate}; ${((avgRate / rate) * 100).toFixed(2)} %`)
    return {
      temp: avgTemp,
      load: avgLoad,
      rate: avgRate,
      broken: broken,
      requestedRate: rate,
      requestedTemp: temp,
    }
  } catch (err) {
    console.error(err)
  }
}

let run = async () => {
  try {
    while (1) {
      let res = await gather()
      await TIME.delay(100)
    }
  } catch (err) {
    console.error(err)
  }
}

// -------------------------------------------------------- Set Extruder Rates...

let extRate = 1

// 29.89 -> 119.94 (100mm requested, 90mm)
// 29.89 -> -63.24 (100mm requested, 93mm)
// 0.9 -> 102

let ratePosBtn = new EZButton(10, 120, 100, 100, `set +${extRate}mm/s`)
let rateZeroBtn = new EZButton(10, 230, 100, 100, `set 0 mm/s`)
let rateNegBtn = new EZButton(10, 340, 100, 100, `set -${extRate}mm/s`)

ratePosBtn.onClick(() => {
  extruderMVM.gotoVelocity([extRate, 0, 0, 0]).then(() => {
    ratePosBtn.good()
  }).catch((err) => {
    console.error(err)
    ratePosBtn.bad()
  })
})

rateZeroBtn.onClick(() => {
  extruderMVM.gotoVelocity([0, 0, 0, 0]).then(() => {
    rateZeroBtn.good()
  }).catch((err) => {
    console.error(err)
    rateZeroBtn.bad()
  })
})

rateNegBtn.onClick(() => {
  extruderMVM.gotoVelocity([-extRate, 0, 0, 0]).then(() => {
    rateNegBtn.good()
  }).catch((err) => {
    console.error(err)
    rateNegBtn.bad()
  })
})

let hotTemp = 240
let setHotBtn = new EZButton(10, 450, 100, 100, `set ${hotTemp}`)
let setColdBtn = new EZButton(10, 560, 100, 100, `set 0`)

setHotBtn.onClick(() => {
  tempVM.setExtruderTemp(hotTemp).then(() => {
    setHotBtn.good()
  }).catch((err) => {
    console.error(err)
    setHotBtn.bad()
  })
})

setColdBtn.onClick(() => {
  tempVM.setExtruderTemp(0).then(() => {
    setColdBtn.good()
  }).catch((err) => {
    console.error(err)
    setColdBtn.bad()
  })
})

/*

// -------------------------------------------------------- Filament Sensor 

let fsTestBtn = new Button(120, 10, 84, 184, 'fs test')
let fsTest = () => {
  fsVM.getBundle().then((data) => {
    fsTestBtn.setHTML(`
    dia: ${data.diameter.toFixed(3)}<br>pos: ${data.posEstimate.toFixed(2)}<br>rate: ${data.rateEstimate.toFixed(2)}
    `)
    //console.log(data)
    setTimeout(fsTest, 50)
  }).catch((err) => {
    console.error(err)
  })
}
fsTestBtn.onClick(fsTest)


// // collect a # of samples, 
// let lastTemp = 0
// let dataStore = []
// let gather = async (samples, temp = 500) => {
//   let collect = () => {
//     return new Promise((resolve, reject) => {
//       // we can gather all of the data w/ this promise.all thing: 
//       Promise.all(
//         [tempVM.getExtruderTemp(), loadVM.getReading()] // fsVM.getStates(), loadVM.getReading(),]
//       ).then((values) => {
//         //console.log('all', values)
//         let time = TIMES.getTimeStamp()
//         tPanel.pushPt([time, values[0]])
//         rPanel.pushPt([time, values[1].rateEstimate])
//         lPanel.pushPt([time, values[2][0]])
//         lastTemp = values[0]
//         dataStore.push([values[0], values[1].rateEstimate, values[2][0]])
//         tPanel.redraw()
//         rPanel.redraw()
//         lPanel.redraw()
//         resolve()
//       }).catch((err) => {
//         console.error(err)
//       })
//     })
//   }
//   while (samples > 0) {
//     try {
//       await collect()
//       await TIMES.delay(100)
//       if (lastTemp > temp) break;
//       //console.log(samples)
//       samples--
//     } catch (err) {
//       console.error(err)
//     }
//   }
// }

// -------------------------------------------------------- Experiment 
// this one can be nasteh simple, 

let fsPollBtn = new Button(120, 210, 84, 184, 'run...')
let pollPt = async (temp, rate, feed) => {
  // assuming everything is reset here... 
  try {
    console.warn(`generating pt for ${temp}, ${rate}, ${feed}`)
    // reconfig motor, 
    //console.warn(`reconfig motor rate...`)
    extruderMVM.motion.settings = {
      junctionDeviation: 0.05,
      accelLimits: [200],
      velLimits: [rate]
    }
    await extruderMVM.setup()
    // await temp, 
    //console.warn(`awaiting ${temp}...`)
    await tempVM.awaitExtruderTemp(temp)
    // request + 5mm, to purge...
    //console.warn(`pushing 15mm purge`)
    await extruderMVM.motion.delta([5])
    await TIMES.delay((5 / rate) * 1000 + 1000)
    // get another zero, 
    //console.warn(`gathering zero...`)
    let data = await fsVM.getBundle()
    fsTestBtn.setHTML(`
    dia: ${data.diameter.toFixed(3)}<br>pos: ${data.posEstimate.toFixed(2)}<br>rate: ${data.rateEstimate.toFixed(2)}
    `)
    let zeroWheelPos = data.posEstimate
    // extrude... 
    //console.warn(`extruding ${feed} mm`)
    await extruderMVM.motion.delta([feed])
    await TIMES.delay((feed / rate) * 1000 + 1000)
    data = await fsVM.getBundle()
    console.warn(`requested ${feed} at ${temp} with ${rate} mm/s rolled ${(data.posEstimate - zeroWheelPos).toFixed(2)}`)
    //console.warn('DONE!')
    return data.posEstimate - zeroWheelPos
  } catch (err) {
    throw err
  }
}
// should loop thru temps, rates... 
// aye
// i 
// would like to add a power-cycle between temp passes, as there is some mixed up motor 
// code (or something) that is messing w/ the integrator, causing it to step backwards 
// also I could write code that intelligently increases rates up to some % rolloff, like 20% say, 
// then we actually would see expanding temp... up to some max of whatever the hotend can do 
let results = []
let shutdown = false 
let sweepPts = async (exP) => {
  try {
    for (let temp = exP.tempStart; temp <= exP.tempEnd; temp += exP.tempIncrement) {
      // power cycle...
      await powerVM.setPowerStates(false, false)
      await TIMES.delay(50)
      await powerVM.setPowerStates(true, true)
      await TIMES.delay(500)
      for (let rate = exP.rateStart; rate <= exP.rateEnd; rate += exP.rateIncrement) {
        let polls = []
        for (let c = 0; c < exP.count; c++) {
          if(shutdown) throw new Error('bailing for shutdown')
          polls.push(await pollPt(temp, rate, exP.feed))
        }
        let result = {
          temp: temp,
          rate: rate,
          feedRequested: exP.feed,
          feedActual: polls // await pollPt(temp, rate, exP.feed),
        }
        results.push(result)
        // calculate % actual feed,
        let actual = polls.reduce((a, b) => { return a + b }, 0) / polls.length 
        let percent = actual / exP.feed 
        console.warn(`MADE ${percent.toFixed(2)}...`)
        if(percent < 0.7){
          console.warn(`BAILING w/ ${percent.toFixed(2)} extrusion`)
          break; // should break rate-loop, right ? 
        }
      }
    }// end for-temps, 
    console.log(results)
  } catch (err) {
    console.error(err)
  }
  // try to get partials... 
  SaveFile(results, 'json', 'pla-esun')
  // for(let temp = start; temp < end; temp += increment){
  //   try {
  //     for(let i = 0; i < count; i ++){
  //       await pollPt(temp, 2, 25)
  //     }
  //   } catch (err) {
  //     console.error(err)
  //   }
  // }
  console.warn(`DONE SWEEP!`)
}
fsPollBtn.onClick(() => {
  sweepPts({
    tempStart: 190,
    tempEnd: 290, // beyond 300 degs we have maybe a calibration issue w/ the t-couple 
    tempIncrement: 15,
    rateStart: 2,
    rateEnd: 22,
    rateIncrement: 4,
    count: 3,
    feed: 25,
  })
})

let shutdownBtn = new Button(120, 420, 84, 84, 'shdn')
shutdownBtn.onClick(async () => {
  shutdown = true 
  await powerVM.setPowerStates(true, true)
})

// alright next... what would be next ? motion controller ? pressures ? bed level ?
// where in the gantt are we ? 

// -------------------------------------------------------- Config Extruder Motor 

let effectiveDriveGearDiameter = 8.2 * 0.9

extruderMVM.motion.settings = {
  junctionDeviation: 0.05,
  accelLimits: [100],
  velLimits: [10]
}

extruderMVM.settings.motor = {
  axis: 0,
  invert: false,
  microstep: 16,
  spu: (16 * 200) / (effectiveDriveGearDiameter * Math.PI),
  cscale: 0.25
}

// we'll want to track motor setup, 

let configStateBtn = new Button(10, 10, 100, 100, `setup config...`)
let configSys = async () => {
  try {
    await powerVM.setPowerStates(true, false)
    await extruderMVM.setup()
    configStateBtn.green('sys ok')
  } catch (err) {
    console.error(err)
    configStateBtn.red('bad setup...')
  }
}
setTimeout(configSys, 500)



// -------------------------------------------------------- Panels 

// spare temp controller to load / unload: 
let tempPanel = new TempPanel(tempVM, 220, 10, 200, 'hotend')

*/

/*
let tPanel = new AutoPlot(220, 10, 650, 230, 'hotend',
  { top: 40, right: 20, bottom: 30, left: 60 })
tPanel.setHoldCount(4000)
let rPanel = new AutoPlot(220, 250, 650, 230, 'rates',
  { top: 40, right: 20, bottom: 30, left: 60 })
rPanel.setHoldCount(4000)
let lPanel = new AutoPlot(220, 490, 650, 230, 'loads',
  { top: 40, right: 20, bottom: 30, left: 60 })
lPanel.setHoldCount(4000)

// ok then... a gather-data routine

// collect a # of samples, 
let lastTemp = 0
let dataStore = []
let gather = async (samples, temp = 500) => {
  let collect = () => {
    return new Promise((resolve, reject) => {
      // we can gather all of the data w/ this promise.all thing: 
      Promise.all(
        [tempVM.getExtruderTemp(), quantickVM.getStates(), loadVM.getReading(),]
      ).then((values) => {
        //console.log('all', values)
        let time = TIMES.getTimeStamp()
        tPanel.pushPt([time, values[0]])
        rPanel.pushPt([time, values[1].rateEstimate])
        lPanel.pushPt([time, values[2][0]])
        lastTemp = values[0]
        dataStore.push([values[0], values[1].rateEstimate, values[2][0]])
        tPanel.redraw()
        rPanel.redraw()
        lPanel.redraw()
        resolve()
      }).catch((err) => {
        console.error(err)
      })
    })
  }
  while (samples > 0) {
    try {
      await collect()
      await TIMES.delay(100)
      if (lastTemp > temp) break;
      //console.log(samples)
      samples--
    } catch (err) {
      console.error(err)
    }
  }
}

let runExperiment = async () => {
  try {
    console.warn('setup')
    await tempVM.setPCF(0)
    await gather(10)
    console.warn('setting torque')
    await quantickVM.setTorque(-0.3)
    console.warn('gathering')
    await gather(50)
    for (let temp = 170; temp <= 240; temp += 10) {
      console.warn(`setting ${temp}...`)
      await tempVM.setExtruderTemp(temp)
      await gather(2000, temp)
      console.warn(`gathering for ${temp}`)
      await gather(400)
    }
    console.warn('done')
    SaveFile(dataStore, 'json', 'extruderData')
    await tempVM.setExtruderTemp(0)
    await gather(200)
  } catch (err) {
    console.error(err)
  }
}
*/

// this starts the experiment ! 
// setTimeout(runExperiment, 500)

// -------------------------------------------------------- Power States 
// also, keepalive & connection indicator 
/*
let kaIndicator = new Button(10, 10, 84, 84, 'connection ?')
kaIndicator.yellow()
let v5State = false
let v24State = false
let v5Btn = new Button(10, 110, 84, 44, '5V Power')
let v24Btn = new Button(10, 170, 84, 44, '24V Power')
v24Btn.yellow()
v5Btn.yellow()
let powerTimerLength = 1000
let powerTimer = {}

// we want to run these on a loop... 
let checkPowerStates = () => {
  motionVM.getPowerStates().then((data) => {
    kaIndicator.green('Connection OK')
    if (data[0]) {
      v5Btn.green('5V Power')
      v5State = true
    } else {
      v5Btn.grey('5V Power')
      v5State = false
    }
    if (data[1]) {
      v24Btn.green('24V Power')
      v24State = true
    } else {
      v24Btn.grey('24V Power')
      v24State = false
    }
    clearTimeout(powerTimer)
    powerTimer = setTimeout(checkPowerStates, powerTimerLength)
  }).catch((err) => {
    console.error(err)
    kaIndicator.red('keepalive broken, see console')
    v5Btn.red('see console')
    v24Btn.red('see console')
  })
}

v5Btn.onClick(async () => {
  v5Btn.yellow()
  await motionVM.setPowerStates(!v5State, v24State)
  checkPowerStates()
})

v24Btn.onClick(async () => {
  v24Btn.yellow()
  await motionVM.setPowerStates(v5State, !v24State)
  checkPowerStates()
})

// startup w/ this loop; 
// comment this line out if you don't want the keepalive to bother you while you try 
// hardware-less code stuff 
powerTimer = setTimeout(checkPowerStates, 750)
*/

// -------------------------------------------------------- Initializing the WSC Port 

// verbosity 
let LOGPHY = false
// to test these systems, the client (us) will kickstart a new process
// on the server, and try to establish connection to it.
console.log("making client-to-server request to start remote process,")
console.log("and connecting to it w/ new websocket")

let wscVPortStatus = "opening"
// here we attach the "clear to send" function,
// in this case we aren't going to flowcontrol anything, js buffers are infinite
// and also impossible to inspect  
wscVPort.cts = () => { return (wscVPortStatus == "open") }
// we also have isOpen, similarely simple here, 
wscVPort.isOpen = () => { return (wscVPortStatus == "open") }

// ok, let's ask to kick a process on the server,
// in response, we'll get it's IP and Port,
// then we can start a websocket client to connect there,
// automated remote-proc. w/ vPort & wss medium,
// for args, do '/processName.js?args=arg1,arg2'
jQuery.get('/startLocal/osapSerialBridge.js', (res) => {
  if (res.includes('OSAP-wss-addr:')) {
    let addr = res.substring(res.indexOf(':') + 2)
    if (addr.includes('ws://')) {
      wscVPortStatus = "opening"
      // start up, 
      console.log('starting socket to remote at', addr)
      let ws = new WebSocket(addr)
      ws.binaryType = "arraybuffer"
      // opens, 
      ws.onopen = (evt) => {
        wscVPortStatus = "open"
        // implement rx
        ws.onmessage = (msg) => {
          let uint = new Uint8Array(msg.data)
          wscVPort.receive(uint)
        }
        // implement tx 
        wscVPort.send = (buffer) => {
          if (LOGPHY) console.log('PHY WSC Send', buffer)
          ws.send(buffer)
        }
      }
      ws.onerror = (err) => {
        wscVPortStatus = "closed"
        console.log('sckt err', err)
      }
      ws.onclose = (evt) => {
        wscVPortStatus = "closed"
        console.log('sckt closed', evt)
      }
    }
  } else {
    console.error('remote OSAP not established', res)
  }
})