/*
slumpMaps.js

builds temp-slump maps for lpf project 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2022

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

'use strict'

// core elements 
import OSAP from '../osapjs/core/osap.js'
import TIME from '../osapjs/core/time.js'
import PK from '../osapjs/core/packets.js'

// virtual machines 
import PowerSwitchVM from '../osapjs/vms/powerSwitches.js'
import AXLActuator from '../osapjs/vms/axlActuator.js'
import AXLCore from '../osapjs/vms/axlCore.js'
import TempVM from '../osapjs/vms/tempVirtualMachine.js'
import LoadVM from '../osapjs/vms/loadcellVirtualMachine.js'
import FilamentSensorVM from '../osapjs/vms/filamentSensorVirtualMachine.js'

import snakeGen from '../osapjs/test/snakeGen.js'

import { SaveFile } from '../osapjs/client/utes/saveFile.js'

// ui elements 
import Grid from '../osapjs/client/interface/grid.js' // main drawing API 
import { Button, EZButton, TextBlock, TextInput } from '../osapjs/client/interface/basics.js'
import TempPanel from '../osapjs/client/components/tempPanel.js'
import AutoPlot from '../osapjs/client/components/autoPlot.js'

console.log(`------------------------------------------`)
console.log("hello lpf controller")

// -------------------------------------------------------- The main UI... thing 

let grid = new Grid()

// -------------------------------------------------------- OSAP Object
let osap = new OSAP("slump-tester")

// -------------------------------------------------------- SETUP NETWORK / PORT 
let wscVPort = osap.vPort("wscVPort")


// -------------------------------------------------------- Yonder Clank VM and Motors 
/*
//OSAP osap("axl-stepper_z-rear-left");
//OSAP osap("axl-stepper_z-front-left");
//OSAP osap("axl-stepper_z-rear-right");
//OSAP osap("axl-stepper_z-front-right");
//OSAP osap("axl-stepper_y-left");
//OSAP osap("axl-stepper_y-right");
//OSAP osap("axl-stepper_x");
OSAP osap("axl-stepper_e");
*/

let xyMicroStep = 8
let xySPU = 40
let xyCScale = 0.3

let zMicroStep = 4
let zSPU = 53.57142857 * 2
let zCScale = 0.3

// we should ~ fudge EDGD if anything, 
let effectiveDriveGearDiameter = 8.2 * 1.0
let eMicroStep = 8
// microstep * 200 = total steps / revolution 
// gearDia * PI is circumference, for steps / linear mm, 
// then /2.41, for linear mm -> cubic mm (one mm of 1.75mm dia filament is 2.41 mm^3 of filament)
let eSPU = ((eMicroStep * 200) / (effectiveDriveGearDiameter * Math.PI)) / 2.41
let eCScale = 0.25

let clank = new AXLCore(osap, {
  bounds: [254, 122, 199.4, 100],
  accelLimits: [1500, 1500, 100, 100],
  velocityLimits: [150, 150, 40, 25],
  queueStartDelay: 500,
  junctionDeviation: 0.1
}, [
  {
    name: "rt_axl-stepper_x",
    axis: 0,
    invert: false,
    microstep: xyMicroStep,
    spu: xySPU,
    cscale: xyCScale
  },
  {
    name: "rt_axl-stepper_y-left",
    axis: 1,
    invert: true,
    microstep: xyMicroStep,
    spu: xySPU,
    cscale: xyCScale
  },
  {
    name: "rt_axl-stepper_y-right",
    axis: 1,
    invert: false,
    microstep: xyMicroStep,
    spu: xySPU,
    cscale: xyCScale
  },
  {
    name: "rt_axl-stepper_z-rear-left",
    axis: 2,
    invert: false,
    microstep: zMicroStep,
    spu: zSPU,
    cscale: zCScale
  },
  {
    name: "rt_axl-stepper_z-rear-right",
    axis: 2,
    invert: true,
    microstep: zMicroStep,
    spu: zSPU,
    cscale: zCScale
  },
  {
    name: "rt_axl-stepper_z-front-left",
    axis: 2,
    invert: true,
    microstep: zMicroStep,
    spu: zSPU,
    cscale: zCScale
  },
  {
    name: "rt_axl-stepper_z-front-right",
    axis: 2,
    invert: false,
    microstep: zMicroStep,
    spu: zSPU,
    cscale: zCScale
  },
  {
    name: "rt_axl-stepper_e",
    axis: 3,
    invert: false,
    microstep: eMicroStep,
    spu: eSPU,
    cscale: eCScale
  },
])

// this clank has 1:1 transforms, this is a hack to overwrite 
// the default (shouldn't be) axl corexy tfs
clank.cartesianToActuatorTransform = (vals, position = false) => {
  let tfVals = JSON.parse(JSON.stringify(vals))
  return tfVals
}

clank.actuatorToCartesianTransform = (vals, position = false) => {
  let tfVals = JSON.parse(JSON.stringify(vals))
  return tfVals
}

// -------------------------------------------------------- Ute to switch system power at the motion-head circuit

// this one is built "the new way" 
// the psu-head / modular-motion-head board: which is just a bus router and power switches to us, 
let powerSwitchVM = new PowerSwitchVM(osap)

// these are older code, we want global vars for but can't constructor them until we have a route:
// the extruder motor 
let extruderMVM = {}
// temperature / heater module(s)
let tempVM = {}
let bedTempVM = {}
// filament sensor 
let fsVM = {}
// load sensor, 
let loadVM = {}

// -------------------------------------------------------- Setup Code 

let setup = async () => {
  try {
    setupBtn.yellow('switches...')
    console.warn(`SETUP: finding switches...`)
    await powerSwitchVM.setup()
    setupBtn.yellow('power cycle...')
    console.warn(`SETUP: cycling power...`)
    await powerSwitchVM.setPowerStates(false, false)
    await TIME.delay(500)
    await powerSwitchVM.setPowerStates(true, false)
    await TIME.delay(250)
    await powerSwitchVM.setPowerStates(true, true)
    await TIME.delay(1500)
    // start a-lookin, 
    setupBtn.yellow('graph traverse...')
    console.warn(`SETUP: collecting graph...`)
    let graph = await osap.nr.sweep()
    // console.log(graph)
    // return 
    // can we get the peripherals ? 
    // collect and setup tempvm, 
    setupBtn.yellow('heater...')
    console.warn(`SETUP: collecting heater module...`)
    tempVM = new TempVM(osap, PK.VC2VMRoute((await osap.nr.find("rt_heater-module", graph)).route))
    await tempVM.setExtruderTemp(200)
    setupBtn.yellow('loadcell...')
    console.warn(`SETUP: collecting loadcells...`)
    loadVM = new LoadVM(osap, PK.VC2VMRoute((await osap.nr.find("rt_loadcell-amp", graph)).route))
    runLoop = true
    plotLoop()
    // console.warn(`SETUP: collecting bed heater...`)
    // bedTempVM = new TempVM(osap, PK.VC2VMRoute((await osap.nr.find("rt_bed-heater-module", graph)).route))
    // console.warn(`SETUP: collecting filament sensor...`)
    // fsVM = new FilamentSensorVM(osap, PK.VC2VMRoute((await osap.nr.find("rt_filament-sensor", graph)).route))
    // collect and setup our clank;
    setupBtn.yellow('machine...')
    console.warn(`SETUP: hooking machine...`)
    await clank.setup(graph)
    console.warn(`SETUP: clank appears to be config'd, etc`)
    setupBtn.yellow('homing...')
    await clank.home(graph)
    console.warn(`SETUP: clank is homed...`)
    setupBtn.green('done!')
    console.log(`------------------------------------------`)
    return
    // comment / uncomment these to run the experiment or just plot (to watch temps drop for safe power down)
    // runLoop = true 
    // plotLoop()
    // return 
    // SaveFile(tempSamples, 'json', 'abs-natural-matterhackers')
    // await run()
  } catch (err) {
    console.error(err)
  }
}

setTimeout(setup, 750)

// -------------------------------------------------------- Running... 

let runBtn = new Button({
  xPlace: 10,
  yPlace: 670,
  width: 100,
  height: 100,
  defaultText: `run test...`
})

runBtn.onClick(() => {
  // non-slumpy:  200, 5, 20, 1.0
  // slumpy:      260, 22, 10, 0.0
  slumpTest(270, 25, 15, 0.0)
  // I'm sort of sick of this, so I want to 
  // show for one-hundo that we can get some signal, 
  // let's do 260, 22, 10 w/ no PCF, 
  // then the same w/ maxed out PCF... 
})

let slumpTest = async (temp, rate, sideLength, pcf) => {
  // rate should be expressed in cubic mm / sec, right ? then we transform to 
  // linear etc 
  try {
    // we should do a purge, non ? 
    // we'll use a long snakegen thing,
    let purge = snakeGen({
      width: 120,
      depth: 10,
      height: 0.4,
      trackWidth: 0.4,
      trackHeight: 0.4,
      segmentLength: 120,
      flowRate: rate 
    })
    console.log(`purge = `, purge)
    for(let seg of purge){
      seg.target[0] += 10 
      seg.target[1] += 20 
    }
    let absoluteEValueHack = purge[purge.length - 1].target[3]
    // set the temp, and turn the cooling fan off 
    await tempVM.setPCF(pcf)
    await tempVM.awaitExtruderTemp(temp)
    // this'll be useful, 
    // let sideLength = Math.sqrt(area)
    // we'll rebuild this as a full-fledged path now, doing some flow-rate maths 
    // to set our linear rate... 
    purge[0].rate = 10 // move *down* at this rate, not too fast... 
    for (let seg of purge) {
      await clank.addMoveToQueue(seg)
    }
    await clank.awaitMotionEnd()
    // ok then, should be purge-tino, let's do the real deal... 
    // let's do one test layer over here to the right... 
    // and these should be big enough to get "real signal" from, 
    let oneLayer = snakeGen({
      width: sideLength,
      depth: sideLength,
      height: 0.4,
      trackWidth: 0.4,
      trackHeight: 0.4,
      segmentLength: sideLength,
      flowRate: rate
    })
    // now run it on the floor, and in the air:
    let floorRun = JSON.parse(JSON.stringify(oneLayer))
    let airRun = JSON.parse(JSON.stringify(oneLayer))
    for (let s in floorRun) {
      // shift floor run over to the right, 
      floorRun[s].target[0] += sideLength + 10
      floorRun[s].target[1] += 40
      floorRun[s].target[3] += absoluteEValueHack
    }
    absoluteEValueHack = floorRun[floorRun.length - 1].target[3]
    for(let s in airRun){
      // we basically need / should-do relative E moves, at least into the core
      // i'll do that next... 
      // then we should be OK to try this, which is getting better and better otherwise... 
      // shift air run the same, and... into the air, 
      airRun[s].target[0] += sideLength + 10
      airRun[s].target[1] += 20
      airRun[s].target[2] += 40
      airRun[s].target[3] += absoluteEValueHack
    }
    airRun[0].rate = 10
    absoluteEValueHack = airRun[airRun.length - 1].target[3]
    // and... yep, run 'em, 
    for (let seg of airRun) {
      await clank.addMoveToQueue(seg)
    }
    await clank.awaitMotionEnd()
    await TIME.delay(2500)
    // for (let seg of airRun) {
    //   await clank.addMoveToQueue(seg)
    // }
    // await clank.awaitMotionEnd()
    // // hang for a little 
    // await TIME.delay(5000)
    // and run the real dealio;
    let slumpPath = snakeGen({
      width: sideLength,
      depth: sideLength,
      height: 20,
      trackWidth: 0.4,
      trackHeight: 0.4,
      segmentLength: sideLength,
      flowRate: rate
    })
    // and run it... 
    slumpPath[0].rate = 10 
    for (let seg of slumpPath) {
      // if(seg.target[2] == 0.4) seg.rate = seg.rate * 0.2 // do a slow 1st layer 
      seg.target[0] += 80
      seg.target[1] += 80
      seg.target[3] += absoluteEValueHack
      console.log(`e ${seg.target[3].toFixed(2)}`)
      await clank.addMoveToQueue(seg)
    }
    await clank.awaitMotionEnd()
    await clank.moveRelative([0, 0, 80, 0])
    SaveFile(dataStash, 'json', 'hotStash')
  } catch (err) {
    console.error(err)
    runBtn.red('err!')
    try {
      tempVM.setExtruderTemp(0)
    } catch (err) {
      console.error(`failed to cooldown extruder temp, also (!)`)
      console.error(err)
    }
  }
}

// ok we do need... the purge, then the zero-spec pressure, and the empty-space pressure... 
// then if we can show hi-pressure, lo-pressure, 
// and a dropoff during a print, we have slumping signal 
/*

this is tough... I think maybe I should purge, then air print, then do the 1st layer and 
onwards, rather than trying to go down, up, down... which maybe is about to be fixed though 
... but maybe i.e. non-slumping prints eventually recover *up* from the air print
whereas slumping prints always stay (even at the ground) near the air-print temps ?? 
it's also worth considering... just lowering the layer height, for more pressure 
and ~ using a 0.6mm nozzle for more "fidelity" / less overall sensitivity (?) 

and maybe some other measure, like pressure-per-flowrate, is better than these 
absolute values, when the machine is accelerating all over the place ? 

shit man, alright, I'm trying with:
(1) the purge 
(2) the air run 
(3) 0->N layers 

Thesis being that... 
- slumpy prints will either quickly drop, or stay at air-run pressure 
- non-slumpy prints will either recover from a shit first layer, or ~ won't drop to these levels ?
- i.e. slumpy: avg. down slope
- non-slumpy: avg. flat or lifting slope 

I think I'm going to quit here either way, it's 320 and I need enough time to look at the "analysis"
... though might do one more w/ the same size here, but hot and fast and w/o PCF... it's more legible 

on return to the machine,
- we want to be able to do "per layer average nozzle pressure" and plot those, basically 
  - so, get "air layer nozzle pressure" then subsequent...
  - should allow us to calculate... like, asymtotic approach... slope and final resting place 
- and like, we would want to measure layer times, as well, during the test... use a little 
  more theory when developing this test in general: what do you expect will be the pattern? 

PAPER NOTES

want to show that we can "observe" changes to the machine model, or ~
observe that changes to the machine, change the machine:material model, 
so we should have two sets of slumping data, one with the fan on full tilt
and the other w/o the fan... 
*/

/*
NOTES

for the next (closed loop realtime) steps, we should totally get in touch with jacob white 
about what level of systems ID *he* would start with, to develop controllers 
for this thing automagically... 

takeaways from this slumping session... 
- PCF does a lot, hot dang 
  - new hotend should get big fukken blowers then, pwm'able, yeah ? 
- pressure... is even when extrusion is even, and can see ~ that over-extrusion hairiness 

it'd be tite to figure out why the response to these limit switch inputs is so 
gd long ? OSAP-cores should measure avg loop cycle times... 

and... the new hotend design: it'd be awesome to be able to have good view to the nozzle 

also thinking about the bus: we could do some two-step drop-and-head non-contentious scheduling,
so the head always arbitrates, taps, and does timing, but i.e. drops announce they have 
packets, then the head dishes time accordingly ?? 

it looks like, for the hotend CAD, the part cooling fan direction also counts a great deal: 
we want all sides (or at least two, symmetric) to get hit evenly 
*/

// -------------------------------------------------------- UI Elements 

// -------------------------------------------------------- Temp Panel / Etc 

let tPanel = new AutoPlot(130, 10, 650, 230, 'hotend',
  { top: 40, right: 20, bottom: 30, left: 60 })
tPanel.setHoldCount(4000)
// let bPanel = new AutoPlot(130, 250, 650, 230, 'bed',
//   { top: 40, right: 20, bottom: 30, left: 60 })
// bPanel.setHoldCount(2000)
let lPanel = new AutoPlot(130, 250, 650, 230, 'loads',
  { top: 40, right: 20, bottom: 30, left: 60 })
lPanel.setHoldCount(4000)

let dataStash = []

// ok then... a gather-data routine
let gather = async () => {
  try {
    // let results = await Promise.all([tempVM.getExtruderTemp(), loadVM.getReading(false, true), fsVM.getReadings(), bedTempVM.getExtruderTemp()])
    let time = TIME.getTimeStamp()
    let results = await Promise.all([tempVM.getExtruderTemp(), loadVM.getReading(false, true)])
    // console.log(results[0])
    tPanel.pushPt([time, results[0]])
    tPanel.redraw()
    lPanel.pushPt([time, results[1][0]])
    lPanel.redraw()
    dataStash.push({ temp: results[0], load: results[1][0] })
    // bPanel.pushPt([time, await bedTempVM.getExtruderTemp()])
    // bPanel.redraw()
    /*
    // rate is more complex; we have a wheel of some diameter, an 2^14 bits / revolution, so 
    let diameter = 17.5   // TODO this... is a hack (!) real diameter is ~ 20.72, now, but we *need* to redesign 
      // the filament sensor... so that it's rate == the motor's rate, no guessy guessy 
    let ticks = 16384
    let circ = diameter * Math.PI
    let tickToLinear = circ / ticks
    let rate = results[2].rate * tickToLinear
    rPanel.pushPt([time, rate])
    rPanel.redraw()
    */
    return
    return {
      temp: results[0],
      bedTemp: results[1],
    }
  } catch (err) {
    throw err
  }
}

let runLoop = false
let plotLoop = async () => {
  try {
    while (runLoop) {
      await gather()
      await TIME.awaitFutureTime(TIME.getTimeStamp() + 100)
    }
  } catch (err) {
    console.error(err)
  }
}

let getDataPoint = async (temp, rate, sampleTime) => {
  try {
    // first we would want to set a temp and wait for it,
    console.warn(`GDP: set / await temp...`)
    runLoop = true
    plotLoop()
    await tempVM.setExtruderTemp(temp)
    // let's await like this... 
    await tempVM.awaitExtruderTemp(temp)
    console.warn(`GDP: set motor request, and purging`)
    await extruderMVM.gotoVelocity([rate, 0, 0])
    // do a little purging, 
    await TIME.delay(5000)
    runLoop = false
    await TIME.delay(100)
    console.warn(`GDP: collecting points...`)
    let stash = []
    let startTime = TIME.getTimeStamp()
    let lastGather = TIME.getTimeStamp()
    while (TIME.getTimeStamp() - sampleTime < startTime) {
      // get new pt, 
      stash.push(await gather())
      // hold-up until 100ms since previous collection, 
      await TIME.awaitFutureTime(lastGather + 100)
      lastGather = TIME.getTimeStamp()
    }
    await extruderMVM.gotoVelocity([0, 0, 0])
    // next... would work out averages & resolve them in a return, 
    let avgTemp = 0
    let avgLoad = 0
    let avgRate = 0
    for (let i = 0; i < stash.length; i++) {
      avgTemp += stash[i].temp
      avgLoad += stash[i].load
      avgRate += stash[i].rate
    }
    avgTemp /= stash.length
    avgLoad /= stash.length
    avgRate /= stash.length
    console.warn(`GDP: collected ${stash.length} data pts at ${temp}, ${rate}; ${((avgRate / rate) * 100).toFixed(2)} %`)
    return {
      temp: avgTemp,
      load: avgLoad,
      rate: avgRate,
      requestedRate: rate,
      requestedTemp: temp,
    }
  } catch (err) {
    console.error(err)
  }
}

let run = async () => {
  try {
    while (1) {
      let res = await gather()
      await TIME.delay(200)
    }
  } catch (err) {
    console.error(err)
  }
}

// -------------------------------------------------------- Onlining... 

let setupBtn = new Button({
  xPlace: 10,
  yPlace: 10,
  width: 100,
  height: 100,
  defaultText: `setup...`
})

// -------------------------------------------------------- Set Extruder Rates...

let extRate = 5

let ratePosBtn = new EZButton(10, 120, 100, 100, `set +${extRate}mm/s`)
let rateZeroBtn = new EZButton(10, 230, 100, 100, `set 0 mm/s`)
let rateNegBtn = new EZButton(10, 340, 100, 100, `set -${extRate}mm/s`)

ratePosBtn.onClick(() => {
  clank.gotoVelocity([0, 0, 0, extRate]).then(() => {
    ratePosBtn.good()
  }).catch((err) => {
    console.error(err)
    ratePosBtn.bad()
  })
})

rateZeroBtn.onClick(() => {
  clank.gotoVelocity([0, 0, 0, 0]).then(() => {
    rateZeroBtn.good()
  }).catch((err) => {
    console.error(err)
    rateZeroBtn.bad()
  })
})

rateNegBtn.onClick(() => {
  clank.gotoVelocity([0, 0, 0, -extRate]).then(() => {
    rateNegBtn.good()
  }).catch((err) => {
    console.error(err)
    rateNegBtn.bad()
  })
})

let hotTemp = 220
let setHotBtn = new EZButton(10, 450, 100, 100, `set ${hotTemp}`)
let setColdBtn = new EZButton(10, 560, 100, 100, `set 0`)

setHotBtn.onClick(() => {
  tempVM.setExtruderTemp(hotTemp).then(() => {
    setHotBtn.good()
  }).catch((err) => {
    console.error(err)
    setHotBtn.bad()
  })
})

setColdBtn.onClick(() => {
  tempVM.setExtruderTemp(0).then(() => {
    setColdBtn.good()
  }).catch((err) => {
    console.error(err)
    setColdBtn.bad()
  })
})

// -------------------------------------------------------- Motor Disable

let noMotorBtn = new Button({
  xPlace: 10,
  yPlace: 780,
  width: 100,
  height: 100,
  defaultText: `disable motors...`
})

noMotorBtn.onClick(async () => {
  try {
    SaveFile(dataStash, 'json', 'hotStash')
    await clank.disableMotors()
  } catch (err) {
    noMotorBtn.red('err disabling!')
    console.error(err)
  }
})

// -------------------------------------------------------- Initializing the WSC Port 

// verbosity 
let LOGPHY = false
// to test these systems, the client (us) will kickstart a new process
// on the server, and try to establish connection to it.
console.log("making client-to-server request to start remote process,")
console.log("and connecting to it w/ new websocket")

let wscVPortStatus = "opening"
// here we attach the "clear to send" function,
// in this case we aren't going to flowcontrol anything, js buffers are infinite
// and also impossible to inspect  
wscVPort.cts = () => { return (wscVPortStatus == "open") }
// we also have isOpen, similarely simple here, 
wscVPort.isOpen = () => { return (wscVPortStatus == "open") }

// ok, let's ask to kick a process on the server,
// in response, we'll get it's IP and Port,
// then we can start a websocket client to connect there,
// automated remote-proc. w/ vPort & wss medium,
// for args, do '/processName.js?args=arg1,arg2'
jQuery.get('/startLocal/osapSerialBridge.js', (res) => {
  if (res.includes('OSAP-wss-addr:')) {
    let addr = res.substring(res.indexOf(':') + 2)
    if (addr.includes('ws://')) {
      wscVPortStatus = "opening"
      // start up, 
      console.log('starting socket to remote at', addr)
      let ws = new WebSocket(addr)
      ws.binaryType = "arraybuffer"
      // opens, 
      ws.onopen = (evt) => {
        wscVPortStatus = "open"
        // implement rx
        ws.onmessage = (msg) => {
          let uint = new Uint8Array(msg.data)
          wscVPort.receive(uint)
        }
        // implement tx 
        wscVPort.send = (buffer) => {
          if (LOGPHY) console.log('PHY WSC Send', buffer)
          ws.send(buffer)
        }
      }
      ws.onerror = (err) => {
        wscVPortStatus = "closed"
        console.log('sckt err', err)
      }
      ws.onclose = (evt) => {
        wscVPortStatus = "closed"
        console.log('sckt closed', evt)
      }
    }
  } else {
    console.error('remote OSAP not established', res)
  }
})