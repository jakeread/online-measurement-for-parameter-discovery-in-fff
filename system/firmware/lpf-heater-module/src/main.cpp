#include <Arduino.h>
#include "indicators.h"
#include "drivers/peripheral_nums.h"
#include "thermalConfig.h"

#include "osape/core/osap.h"
#include "osape/vertices/endpoint.h"
#include "osape_arduino/vp_arduinoSerial.h"
#include "osape_ucbus/vb_ucBusDrop.h"

// -------------------------------------------------------- DRIVERS

// pa22 on TC4, peripheral e
void startTC4(void){
  PORT->Group[0].DIRSET.reg = (1 << 22);
  PORT->Group[0].PINCFG[22].bit.PMUXEN = 1;
  PORT->Group[0].PMUX[22 >> 1].reg = PORT_PMUX_PMUXE(PERIPHERAL_E);
  // setup
  TC4->COUNT16.CTRLA.bit.ENABLE = 0;
  // clock
  MCLK->APBCMASK.reg |= MCLK_APBCMASK_TC4;
  // send clk, 
  GCLK->PCHCTRL[TC4_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(0);
  // setup
  TC4->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 | TC_CTRLA_PRESCALER_DIV64
    | TC_CTRLA_PRESCSYNC_GCLK;
  TC4->COUNT16.WAVE.reg = TC_WAVE_WAVEGEN_NPWM;
  TC4->COUNT16.CC[0].reg = 1; // timer counters in 16 bit mode have the top of the compare at the max width
  // start 
  while(TC4->COUNT16.SYNCBUSY.bit.ENABLE);
  TC4->COUNT16.CTRLA.bit.ENABLE = 1;  
  while(TC4->COUNT16.SYNCBUSY.bit.ENABLE);
}

void writeOutputA(float duty){
  // duty 0-1, pwm counter 0-65535
  if(duty < 0.0F){
    duty = 0.0F;
  } else if (duty > MAX_PWM){
    duty = MAX_PWM;
  }
  uint16_t set = duty * 65535;
  TC4->COUNT16.CCBUF[0].reg = set;
}

// PB11 on TC5-1
void startTC5(void){
  PORT->Group[1].DIRSET.reg = (1 << 11);
  PORT->Group[1].PINCFG[11].bit.PMUXEN = 1;
  PORT->Group[1].PMUX[11 >> 1].reg = PORT_PMUX_PMUXO(PERIPHERAL_E);
  // setup
  TC5->COUNT16.CTRLA.bit.ENABLE = 0;
  // clock
  MCLK->APBCMASK.reg |= MCLK_APBCMASK_TC5;
  // send clk, 
  GCLK->PCHCTRL[TC5_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(0);
  // setup
  TC5->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 | TC_CTRLA_PRESCALER_DIV64
    | TC_CTRLA_PRESCSYNC_GCLK;
  TC5->COUNT16.WAVE.reg = TC_WAVE_WAVEGEN_NPWM;
  TC5->COUNT16.CC[1].reg = 1; 
  // start 
  while(TC5->COUNT16.SYNCBUSY.bit.ENABLE);
  TC5->COUNT16.CTRLA.bit.ENABLE = 1;  
  while(TC5->COUNT16.SYNCBUSY.bit.ENABLE);
}

void writeOutputD(float duty){
  if(duty < 0.0F){
    duty = 0.0F;
  } else if (duty > 1.0F){
    duty = 1.0F;
  }
  uint16_t set = duty * 65535;
  TC5->COUNT16.CCBUF[1].reg = set;
}

float readThermA(void){
  // get 4, average 
  uint32_t sum = 0;
  for(uint8_t i = 0; i < 4; i ++){
    sum += analogRead(A4);
  }
  float voltage = sum / 4 * (3.3 / 1024); // div sum, div vout 
  // sysError(String(voltage));
  // find the bounding temps, 
  uint8_t lb = 0;
  uint8_t ub = 0;
  for(uint8_t i = 0; i < TABLE_SIZE - 1; i ++){
    #if TABLE_DIR_UP
    if(voltages[i] <= voltage && voltages[i + 1] > voltage){
      lb = i + 1;
      ub = i;
      break;
    }
    #else
    if(voltages[i] >= voltage && voltages[i + 1] < voltage){
      lb = i;
      ub = i + 1;
      break;
    }
    #endif 
  }
  if(lb == 0 && ub == 0) return 0.0F; // no interval found, bail. 
  // ok, have lb and ub, find position within 
  float rel = (voltages[lb] - voltage) / (voltages[lb] - voltages[ub]);
  // and interp. for temps, 
  float temp = temps[lb] - (temps[lb] - temps[ub]) * rel;
  return temp;
}

// -------------------------------------------------------- CONTROLLER VARS

unsigned long lastRun = 0;
unsigned long loopTime = 100; // ms,

float _setPoint = 0.0F;         // desired temp, degs C
float _tempReading = 0.0F;      // temp, degs C
float _tempEstimate = 0.0F;     // filtered estimate 
float _lastTempEstimate = 0.0F; // last estimate 
float _alpha = 0.2F;            // filter alpha 
float _currentOutput = 0.0F;    // 0-1, effort 
float _PTerm = -1.5F;           // proportional 
float _ITerm = 0.0F;            // integral (not used atm)
float _DTerm = -2.6F;           // derivative

// -------------------------------------------------------- OSAP 

#ifdef IS_HOTEND
OSAP osap("heater-module");
#else
OSAP osap("bed-heater-module");
#endif 

// -------------------------------------------------------- VPORTS 

VPort_ArduinoSerial vpUSBSerial(&osap, "arduinoUSBSerial", &Serial);

VBus_UCBusDrop vbUCBusDrop(&osap, "ucBusDrop");

// -------------------------------------------------------- ENDPOINTS 

// -------------------------------------------------------- Temp Set

EP_ONDATA_RESPONSES onTempSetData(uint8_t* data, uint16_t len){
  // set temp, float32, and write to endpoint 
  chunk_float32 chunk = { .bytes = { data[0], data[1], data[2], data[3] } };
  _setPoint = chunk.f;
  //sysError("temp set: " + String(chunk.f));
  return EP_ONDATA_ACCEPT;
}

Endpoint tempSetEP(&osap, "tempSet", onTempSetData);

// -------------------------------------------------------- Temp Query 

Endpoint currentTempEP(&osap, "currentTemp");

// -------------------------------------------------------- Effort Query 

Endpoint currentOutputEP(&osap, "currentOutput");

// -------------------------------------------------------- STATE UPDATES 

void updateStates(void){
  chunk_float32 chunkE = { .f = _currentOutput };
  chunk_float32 chunkT = { .f = _tempEstimate };
  // write 2 endpoints:
  currentTempEP.write(chunkT.bytes, 4);
  currentOutputEP.write(chunkE.bytes, 4);
  // that's it ? 
}

// -------------------------------------------------------- PID Terms 

EP_ONDATA_RESPONSES onPIDTermData(uint8_t* data, uint16_t len){
  uint16_t rptr = 0;
  chunk_float32 chunk[3];
  chunk[0] = { .bytes = { data[rptr ++], data[rptr ++], data[rptr ++], data[rptr ++] } };
  chunk[1] = { .bytes = { data[rptr ++], data[rptr ++], data[rptr ++], data[rptr ++] } };
  chunk[2] = { .bytes = { data[rptr ++], data[rptr ++], data[rptr ++], data[rptr ++] } };
  _PTerm = chunk[0].f;
  _ITerm = chunk[1].f;
  _DTerm = chunk[2].f;
  return EP_ONDATA_ACCEPT;
}

Endpoint pidTermsEP(&osap, "pidTerms", onPIDTermData);

// -------------------------------------------------------- CONTROLLER

// smoothing y [ n ] = α x [ n ] + ( 1 − α ) y [ n − 1 ]

void runPID(void){
  // 1st, read temp & write into endpoint, 
  // then generate output & also write it both ways 
  float sum = 0;
  for(uint8_t i = 0; i < 4; i ++){
    sum += readThermA();
  }
  _tempReading = sum / 4; // readThermA();
  // filter
  _tempEstimate = _tempReading * _alpha + _lastTempEstimate * (1 - _alpha);
  // do error (positive error is too hot)
  float err = _tempEstimate - _setPoint;
  // derivative, with loop time of <loopTime> ms, 
  float dt = (_tempEstimate - _lastTempEstimate) / ((float)loopTime / 1000);
  _currentOutput = _PTerm * err + _DTerm * dt;
  // write outputs to hardware
  writeOutputA(_currentOutput);
  // track,
  _lastTempEstimate = _tempEstimate;
}

// -------------------------------------------------------- CHD Write 

#ifdef IS_HOTEND

EP_ONDATA_RESPONSES onChDData(uint8_t* data, uint16_t len){
  chunk_float32 chunk = { .bytes = { data[0], data[1], data[2], data[3] } };
  writeOutputD(chunk.f);
  return EP_ONDATA_ACCEPT;
}

Endpoint chDEP(&osap, "chD", onChDData);

#endif 

// -------------------------------------------------------- SETUP
// from right to left, heater channels are ABCDEF
// A: PA22, B: PA20, C: PB10 (TC5-0), D: PB11 (TC5-1), E: PB17, F: PA12 

void setup() {
  CLKLIGHT_SETUP;
  ERRLIGHT_SETUP;
  // setup comms 
  vpUSBSerial.begin();
  vbUCBusDrop.begin();
  // start controller... 
  startTC4();
  // hotend, turn on a fan on PB10
  #ifdef IS_HOTEND
  // CHD / part cooling fan, 
  //PORT->Group[1].DIRSET.reg = (1 << 11);
  //PORT->Group[1].OUTSET.reg = (1 << 11);
  startTC5();
  TC5->COUNT16.CCBUF[1].reg = 6000;
  // hotend cooling fan is always on, 
  PORT->Group[1].DIRSET.reg = (1 << 10);
  PORT->Group[1].OUTSET.reg = (1 << 10);
  #endif
}

// -------------------------------------------------------- LOOP 

void loop() {
  // loop osap 
  osap.loop();
  // run loop, occasionally 
  if(millis() > lastRun + loopTime){
    lastRun = millis();
    runPID();
    updateStates();
  }
}

// -------------------------------------------------------- HANDLES

void ucBusDrop_onRxISR(void){

}

void ucBusDrop_onPacketARx(uint8_t* data, uint16_t len){
  //ERRLIGHT_TOGGLE;
}