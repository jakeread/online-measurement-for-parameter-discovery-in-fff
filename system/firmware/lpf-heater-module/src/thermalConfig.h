#ifndef THERMAL_CONFIG_H_
#define THERMAL_CONFIG_H_

// hotend, or hot bed? 
// on both, THERM_A is on PA04 / arduino A4 
// also on both, GATE_A is on PA22 / TC4, 
// difference: hotbed has RTD1K therm, so linearization is different 
#define IS_HOTEND
// #define IS_HOTBED

#ifdef IS_HOTBED

#define MAX_PWM 1.0F
#define TABLE_SIZE 16
#define TABLE_DIR_UP false
// reading temps... spreadsheet & table in the log
// for PN 478-10979-1-ND
float voltages[TABLE_SIZE] = {
  3.29972, 3.29866, 3.29498, 3.29099, 3.28448,
  3.27428, 3.25884, 3.23625, 3.20422, 3.16020,
  3.10154, 3.02574, 2.81573, 2.52731, 2.17958,
  1.80939, 
};

float temps[TABLE_SIZE] = {
  -50, -30, -10, 0, 10,
  20, 30, 40, 50, 60,
  70, 80, 100, 120, 140,
  160,
};

/*
// the below is config for the RTD 223-1563-1-ND
#define TABLE_SIZE 23
#define TABLE_DIR_UP true 
// reading temps... spreadsheet & table in the log
float voltages[TABLE_SIZE] = {
  1.35333, 1.40628, 1.45776, 1.50798, 1.55674,
  1.60334, 1.65000, 1.69455, 1.73780, 1.77968,
  1.82025, 1.85964, 1.89777, 1.93478, 1.97069,
  2.00553, 2.03940, 2.07228, 2.10422, 2.13528,
  2.16555, 2.19495, 2.22364
};

float temps[TABLE_SIZE] = {
  -60, -50, -40, -30, -20,
  -10, 0, 10, 20, 30,
  40, 50, 60, 70, 80,
  90, 100, 110, 120, 130,
  140, 150, 160
};
*/

#endif

#ifdef IS_HOTEND

#define MAX_PWM 1.0F
#define TABLE_SIZE 24
#define TABLE_DIR_UP false
// reading temps... spreadsheet & table in the log
float voltages[TABLE_SIZE] = {
  3.29963, 3.29847, 3.29471, 3.29072, 3.28427,
  3.26733, 3.23642, 3.20421, 3.15945, 3.02034,
  2.97307, 2.79764, 2.48679, 2.10866, 1.70887,
  1.33138, 1.01658, 0.76466, 0.57363, 0.43218,
  0.32837, 0.25229, 0.20141, 0.0,
};

float temps[TABLE_SIZE] = {
  -50, -30, -10, 0, 10,
  25, 40, 50, 60, 80,
  85, 100, 120, 140, 160,
  180, 200, 220, 240, 260,
  280, 300, 320, 400,
};

#endif

#endif 