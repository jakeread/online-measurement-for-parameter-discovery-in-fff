/*
drivers/ads1231.h

reads TI ADS1231 loadcell amplifier 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2021

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo
projects. Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#ifndef ADS_1231_H_
#define ADS_1231_H_

#include <Arduino.h>

class ADS_1231 {
    private:
        static ADS_1231* instance;
    public:
        ADS_1231(void);
        static ADS_1231* getInstance(void);
        void init(void);
        void loop(void);
        int32_t latest[3] = {0,0,0};
};

extern ADS_1231* ads_1231;

#endif 