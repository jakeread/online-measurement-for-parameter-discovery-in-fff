/*
drivers/ads1231.cpp

reads TI ADS1231 loadcell amplifier 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2021

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo
projects. Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#include "ads1231.h"

ADS_1231* ADS_1231::instance = 0;

ADS_1231* ADS_1231::getInstance(void){
    if(instance == 0){
        instance = new ADS_1231();
    }
    return instance;
}

ADS_1231* ads_1231 = ADS_1231::getInstance();

ADS_1231::ADS_1231(void){}

uint32_t misoPins[3] = {11, 15, 7};
uint32_t clkPins[3] = {8, 13, 5};
uint32_t pinPorts[3] = {0, 1, 0};

void ADS_1231::init(void){
    // there's one mode pin on the board, parallel to all three channels:
    // SPEED (mode pin) on PB16: set high for 80 Hz, low for 10 Hz
    // fair warning: I've seen some erratic behaviour at 80 Hz 
    PORT->Group[1].DIRSET.reg = (1 << 16);
    PORT->Group[1].OUTCLR.reg = (1 << 16);
    // and one power-down pin, we just turn the things on all the time: 
    // PDWN on PB22: low to reset, high to enable 
    PORT->Group[1].DIRSET.reg = (1 << 22);
    PORT->Group[1].OUTSET.reg = (1 << 22);
    // one clock mode pin, same net for each channel as well: 
    // CLKIN on PB17 (either external src ~ 4Mhz or tie low for internal osc)
    PORT->Group[1].DIRSET.reg = (1 << 17);
    PORT->Group[1].OUTCLR.reg = (1 << 17);
    // now each channel has an independent CLK and MISO line,
    for(uint8_t i = 0; i < 3; i ++){
        // dataRead / dataOut (MISO) pin: goes low when data ready, then is dout on clk edge 
        PORT->Group[pinPorts[i]].DIRCLR.reg = (1 << misoPins[i]);
        PORT->Group[pinPorts[i]].PINCFG[misoPins[i]].bit.INEN = 1;
        // and clks are outputs:
        PORT->Group[pinPorts[i]].DIRSET.reg = (1 << clkPins[i]);
        PORT->Group[pinPorts[i]].OUTCLR.reg = (1 << clkPins[i]);
    }
}

#define DRDY_HIGH(ch) PORT->Group[pinPorts[ch]].IN.reg & (1 << misoPins[ch])
#define CLK_HIGH(ch) PORT->Group[pinPorts[ch]].OUTSET.reg = (1 << clkPins[ch])
#define CLK_LOW(ch) PORT->Group[pinPorts[ch]].OUTCLR.reg = (1 << clkPins[ch])

void ADS_1231::loop(void){
    // hopefully this catches each fast enough (?)
    if(!(DRDY_HIGH(0)) && !(DRDY_HIGH(1)) && !(DRDY_HIGH(2))){
        for(uint8_t ch = 0; ch < 3; ch ++){
            // make sure clk is low, 
            CLK_LOW(ch);
            delayMicroseconds(1);
            // do 24 clock pulses... one per bit 
            int32_t reading = 0;
            for(uint8_t i = 0; i < 24; i ++){
                CLK_HIGH(ch);
                delayMicroseconds(1); // clock delay on datasheet is only 50ns, 
                if(DRDY_HIGH(ch)){
                    reading |= (1 << (23 - i));
                }
                CLK_LOW(ch);
                delayMicroseconds(1);
            }
            // fill in leading zeros for -ve vals 
            if(reading & (1 << 23)){
                for(uint8_t i = 0; i < 8; i ++){
                    reading |= (1 << (24 + i));
                }
            }
            // now one more pulse to reset the DRDY to high
            CLK_HIGH(ch);
            delayMicroseconds(1);
            CLK_LOW(ch);
            // done here
            latest[ch] = reading;
        }
    }
}
