#include <Arduino.h>
#include "indicators.h"

#include "osape/core/osap.h"
#include "osape/vertices/endpoint.h"
#include "osape_arduino/vp_arduinoSerial.h"
#include "osape_ucbus/vb_ucBusDrop.h"

// loadcell 
#include "drivers/ads1231.h"

// -------------------------------------------------------- OSAP 

OSAP osap("loadcell-amp");

// -------------------------------------------------------- VPORTS 

VPort_ArduinoSerial vpUSBSerial(&osap, "arduinoUSBSerial", &Serial);

VBus_UCBusDrop vbUCBusDrop(&osap, "ucBusDrop");

// -------------------------------------------------------- ENDPOINTS 

// -------------------------------------------------------- Read Loadcell: 2

Endpoint loadcellReadEP(&osap, "loadcells");

// -------------------------------------------------------- Comparator: 3 

boolean comparePositiveEdge = true;
int32_t lastValue = 0;
int32_t compareValue = 0;

EP_ONDATA_RESPONSES onComparatorData(uint8_t* data, uint16_t len){
  comparePositiveEdge = data[0] ? true : false;
  uint16_t rptr = 1;
  compareValue = ts_readInt32(data, &rptr);
  // OSAP::debug("compare positive " + String(comparePositiveEdge) + " to val " + String(compareValue));
  return EP_ONDATA_REJECT;
}

Endpoint comparatorEP(&osap, "loadcell-comparator", onComparatorData);

// -------------------------------------------------------- "Driver" - update states & talk to comparator 

uint8_t datagram[12];
uint32_t lastErrLightOnTime = 0;

void updateStates(void){
  // read lc channels & publish, 
  chunk_int32 rd[3] = {
      { i: ads_1231->latest[0] },
      { i: ads_1231->latest[1] },
      { i: ads_1231->latest[2] }
    };
  memcpy(datagram, rd[0].bytes, 4);
  memcpy(&(datagram[4]), rd[1].bytes, 4);
  memcpy(&(datagram[8]), rd[2].bytes, 4);  
  loadcellReadEP.write(datagram, 12);
  // check comparator:
  datagram[0] = 1;
  int32_t val = ads_1231->latest[0];
  //OSAP::debug(String(val));
  if(comparePositiveEdge){
    if(val > compareValue && lastValue < compareValue){
      //OSAP::debug("firing comp");
      ERRLIGHT_ON;
      lastErrLightOnTime = millis();
      comparatorEP.write(datagram, 1);
    }
  } else {
    if(val < compareValue && lastValue > compareValue){
      //OSAP::debug("firing comp");
      ERRLIGHT_ON;
      lastErrLightOnTime = millis();
      comparatorEP.write(datagram, 1);
    }
  }
  lastValue = val;
}

// -------------------------------------------------------- SETUP / RUN 

void setup() {
  CLKLIGHT_SETUP;
  ERRLIGHT_SETUP;
  // setup comms 
  vpUSBSerial.begin();
  vbUCBusDrop.begin();
  // startup loadcell 
  ads_1231->init();
}

// -------------------------------------------------------- LOOP 

uint32_t lastUpdate = 0;
uint32_t updatePerioduS = 12500; // loadcell amp can do 80hz at best 

uint32_t lastBlink = 0;
uint32_t blinkPeriod = 250;

void loop() {
  osap.loop();
  ads_1231->loop();
  if(lastUpdate + updatePerioduS < micros()){
    lastUpdate = micros();
    updateStates();
  }
  if(lastBlink + blinkPeriod < millis()){
    lastBlink = millis();
    CLKLIGHT_TOGGLE;
  }
  if(lastErrLightOnTime + 250 < millis()){
    ERRLIGHT_OFF;
  }
}

// -------------------------------------------------------- HANDLES

void ucBusDrop_onRxISR(void){

}

void ucBusDrop_onPacketARx(uint8_t* data, uint16_t len){

}