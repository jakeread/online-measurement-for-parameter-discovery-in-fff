/*
hmc.h

holonic motion control 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2022

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the osap project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef AXL_COORDINATOR_H_
#define AXL_COORDINATOR_H_

#include <Arduino.h>
#include "axl_config.h"

#define AXL_MODE_ACCEL 1
#define AXL_MODE_VELOCITY 2
#define AXL_MODE_POSITION 3 
#define AXL_MODE_QUEUE 4

#define AXL_QUEUESTATE_EMPTY 1
#define AXL_QUEUESTATE_AWIATING_START 2
#define AXL_QUEUESTATE_RUNNING 3
#define AXL_QUEUESTATE_INCREMENTING 4 

#define AXL_HALT_NONE 0 
#define AXL_HALT_SOFT 1 
#define AXL_HALT_CASCADE 3 
#define AXL_HALT_ACK_NOT_PICKED 4
#define AXL_HALT_MOVE_COMPLETE_NOT_PICKED 5
#define AXL_HALT_BUFFER_STARVED 6
#define AXL_HALT_OUT_OF_ORDER_ARRIVAL 7 

struct vect_t {
  float axis[AXL_NUM_DOF];
};

typedef struct axlPlannedSegment_t {
  // we're given these data over the network:
  uint32_t segmentNumber = 0;               // continuity counter 
  uint8_t returnActuator = 0;               // which of ye will ack ? 
  boolean isLastSegment = false;            // should we expect end of queue here ? 
  vect_t unitVector;                        // unit vector, direction
  float vi = 0.0F;                          // start velocity 
  float accel = 0.0F;                       // accel rate 
  float vmax = 0.0F;                      // max rate 
  float vf = 0.0F;                          // end velocity 
  float distance = 0.0F;                    // how far total 
  // queueing flags... 
  boolean isReady = false;                  // is... next up, needs to be executed, 
  boolean isRunning = false;                // currently ticking, 
  // linked list next, previous, and pos
  axlPlannedSegment_t* next = nullptr;         // will link these... 
  axlPlannedSegment_t* previous = nullptr;     // ... 
  uint8_t indice = 0;                       // it can be nice to know, for list debuggen
} axlPlannedSegment_t;

typedef struct axlState_t {
  // these triplets, always:
  vect_t positions;
  vect_t velocities;
  vect_t accelerations;
  // halt state 
  uint8_t haltState = AXL_HALT_NONE;
  // mode: vel, position, queue,
  uint8_t mode;
  uint8_t queueState;
  // queue-al distance in segment, 
  float segDistance;
  float segVel;
  float segAccel;
  // modal target, 
  vect_t target;
  // queue state, 
  axlPlannedSegment_t* head;  // ingest at 
  axlPlannedSegment_t* tail;  // execute at 
} axlState_t;

typedef struct axlSettings_t {
  // dynamics limits 
  vect_t accelLimits;
  vect_t velocityLimits;
  // queue kit 
  uint32_t queueStartDelayMS = 500;
  uint32_t queueStartTime = 0; 
  // id'ing, 
  uint8_t ourActuatorID = 0;
} axlSettings_t;

void axl_setup(void);
void axl_integrator(void);
void axl_integrateMode(void);
void axl_integrateQueue(void);

// ute for apps (motors)
void axl_onPositionDelta(uint8_t axis, float delta, float absolute);

// queue... stuff ?
boolean axl_hasQueueSpace(void);
void axl_addSegmentToQueue(axlPlannedSegment_t move);
uint16_t axl_getSegmentAckMsg(uint8_t* msg);
uint16_t axl_getSegmentCompleteMsg(uint8_t* msg);

// halt stuff...
void axl_halt(void);
void axl_halt(uint8_t haltCode);
uint16_t axl_getHaltPacket(uint8_t* data);

// set current position == this... 
void axl_setPosition(vect_t posns);
// seems likely we'll use these just for modal stuff, but... 
void axl_setSettings(axlSettings_t _settings);

// modal requests, could be more like "setState" - but that's TMI 
void axl_setPositionTarget(vect_t targ);
void axl_setVelocityTarget(vect_t targ);

// get the whole chunk 
uint16_t axl_getState(uint8_t* data);
boolean axl_isMoving(void);

#endif 