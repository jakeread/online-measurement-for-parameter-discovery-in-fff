/*
hmc.cpp

holonic motion control 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2022

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the osap project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "axl.h"
#include "axl_config.h"
#include "../indicators.h"
#include "../osape/core/ts.h"
#include "../osape/core/osap.h"

// -------------------------------------------------------- File Scoped Vars 

// check it out, big state object:
volatile axlState_t state;

// we have settings... 
axlSettings_t settings;

// queue,
axlPlannedSegment_t queue[AXL_QUEUE_LEN];

// how long is a tick ? 
float delT = (float)AXL_TICKER_INTERVAL_US / 1000000.0F;

// stash / temp var,
vect_t dist;

// messages out... 
uint8_t segmentAckMsg[128];
uint16_t segmentAckMsgLen = 0;
uint8_t segmentCompleteMsg[128];
uint16_t segmentCompleteMsgLen = 0;

// halt packet, 
uint8_t haltPck[128];
uint16_t haltPckLen = 0;
String haltMessage;

// -------------------------------------------------------- Setup 

void axl_setup(void){
  // zero / init states, 
  state.mode = AXL_MODE_POSITION;
  state.queueState = AXL_QUEUESTATE_EMPTY;
  state.segDistance = 0.0F;
  state.segVel = 0.0F;
  state.segAccel = 0.0F;
  // per-axis states, 
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    state.positions.axis[a] = 0.0F;
    state.velocities.axis[a] = 0.0F;
    state.accelerations.axis[a] = 0.0F;
    state.target.axis[a] = 0.0F;
    // and limits / settings, 
    settings.accelLimits.axis[a] = 100.0F;
    settings.velocityLimits.axis[a] = 100.0F;
  }
  // link the ringbuffer, 
  for(uint8_t i = 0; i < AXL_QUEUE_LEN; i ++){
    queue[i].indice = i;
    if(i != AXL_QUEUE_LEN - 1) queue[i].next = &(queue[i+1]);
    if(i != 0) queue[i].previous = &(queue[i-1]);
  }
  queue[0].previous = &(queue[AXL_QUEUE_LEN - 1]);
  queue[AXL_QUEUE_LEN - 1].next = &(queue[0]);
  // init ptrs, 
  state.head = &(queue[0]);  // where to write-in, 
  state.tail = &(queue[0]);  // which is ticking along... 
}

// -------------------------------------------------------- Integrators  

void axl_integrator(void){
  // else continue, 
  state.mode == AXL_MODE_QUEUE ? axl_integrateQueue() : axl_integrateMode();
}

// -------------------------------------------------------- Mode Integrator 
void axl_integrateMode(void){
  switch(state.mode){
    case AXL_MODE_POSITION:
      for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
        // calculate distance from target, 
        dist.axis[a] = state.target.axis[a] - state.positions.axis[a];
        // if we're in a stopping condition, do nothing, 
        if(abs(dist.axis[a]) < AXL_POSITIONING_EPSILON && abs(state.velocities.axis[a] < AXL_POSITIONING_VEL_EPSILON)){
          state.accelerations.axis[a] = 0.0F;
          state.velocities.axis[a] = 0.0F;
          continue;
        }
        // otherwise, set accel dir blindly, based on sign, 
        if(dist.axis[a] <= 0.0F){
          state.accelerations.axis[a] = - settings.accelLimits.axis[a];
        } else {
          state.accelerations.axis[a] = settings.accelLimits.axis[a];
        }
        // now find a stopping distance... this will always be positive since (vel^2)
        float stopDistance = (state.velocities.axis[a] * state.velocities.axis[a]) / (2.0F * settings.accelLimits.axis[a]);
        if(stopDistance >= abs(dist.axis[a])){
          // so accel needs to be -ve to the sign of velocity, 
          // this is a strange formulation, better would be to get the gd signs right 
          if(state.velocities.axis[a] <= 0.0F){
            state.accelerations.axis[a] = settings.accelLimits.axis[a];
          } else {
            state.accelerations.axis[a] = -settings.accelLimits.axis[a];
          }
        }
      }
      break;
    case AXL_MODE_VELOCITY:
      // I'd like to do "lookahead" to set accels to zero & rates to exact-counts 
      // at each time-step, a-la positioning epsilon code, below... and consider then 
      // switch up the switch statements, i.e. take more out of the lower half, put it in these two sections, 
      for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
        // if under, go up, if over, go down... 
        if(state.velocities.axis[a] < state.target.axis[a]){
          state.accelerations.axis[a] = settings.accelLimits.axis[a];
        } else if (state.velocities.axis[a] > state.target.axis[a]){
          state.accelerations.axis[a] = -settings.accelLimits.axis[a];
        }
        // precalculate expected speed delta 
        float velDelta = state.accelerations.axis[a] * delT;
        // gap between current velocity and target velocity, 
        float velGap = state.target.axis[a] - state.velocities.axis[a];
        // if we're going to surpass target vel, set vel = targetVel, accel = 0 
        if(abs(velGap) < abs(velDelta)){
          state.velocities.axis[a] = state.target.axis[a];
          state.accelerations.axis[a] = 0.0F;
        }
      }
      break;
    case AXL_MODE_ACCEL:
      // noop, it all just happens in the integrator,
      break;
    default:
      // ?
      break;
  } // ------------------------------------------ End Mode Switch 
  // -------------------------------------------- Integrate 
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    // ok we have an accel, now integrate vel, 
    state.velocities.axis[a] += state.accelerations.axis[a] * delT;
    // guard against velocity limits... 
    // (this could be a tite macro, is used all over... )
    if(state.velocities.axis[a] > settings.velocityLimits.axis[a]){
      state.velocities.axis[a] = settings.velocityLimits.axis[a];
    } else if(state.velocities.axis[a] < -settings.velocityLimits.axis[a]){
      state.velocities.axis[a] = -settings.velocityLimits.axis[a];
    }
    // now update position, 
    float posDelta = state.velocities.axis[a] * delT;
    // checking position overshoots:
    if(state.mode == AXL_MODE_POSITION){
      // if we're about to make / overshoot it, 
      if(dist.axis[a] - posDelta < AXL_POSITIONING_EPSILON && dist.axis[a] - posDelta > 0.0F){
        // real delta is... aka hit it direct: 
        posDelta = dist.axis[a];
        // zero, zero... 
        state.velocities.axis[a] = 0.0F;
        state.accelerations.axis[a] = 0.0F;
      }
    }
    // uuuhpdate 
    state.positions.axis[a] += posDelta;
    axl_onPositionDelta(a, posDelta, state.positions.axis[a]);
  } // end integrate-per-axis loop, 
} // end mode-based integrate, 

// -------------------------------------------------------- QUEUE INTEGRATOR
void axl_integrateQueue(void){
  // if we are in queue mode, we do this integration step... which ultimately updates the same base states 
  // finishing the project means that these two mode-switch cleanly, i.e. when we run out of segments and 
  // resort to some minimum-energy safety policy 
  pickup:
  // check if we have / are within a valid move, 
  if(!(state.tail->isReady)) return;
  // past this point we have at least one move to make, 
  switch(state.queueState){
    case AXL_QUEUESTATE_RUNNING: // we were previously running, carry on:
      break;
    case AXL_QUEUESTATE_EMPTY: // we were previously empty, this is new, setup the delay: 
      state.segDistance = 0.0F;
      state.queueState = AXL_QUEUESTATE_AWIATING_START;
      settings.queueStartTime = millis() + settings.queueStartDelayMS;
      return;
    case AXL_QUEUESTATE_AWIATING_START:  // we have setup the delay, are now waiting... 
      if(millis() > settings.queueStartTime){
        state.queueState = AXL_QUEUESTATE_INCREMENTING;
      }
      return;
    case AXL_QUEUESTATE_INCREMENTING:
      // we've just collected a new tail, so we should do 
      state.segVel = state.tail->vi; 
      state.segAccel = state.tail->accel; // although this statemachine decides this again... 
      state.queueState = AXL_QUEUESTATE_RUNNING;
      // OSAP::debug("vi:\t" + String(state.tail->vi, 2) + "\tvf:\t" + String(state.tail->vf, 2) + "\tdst:\t" + String(state.tail->distance, 2));
      break;
    default:
      break;
  }
  // we want to know if it's time to start stopping... so, we have 
  // vf^2 = vi^2 + 2ad 
  // vf^2 - vi^2 = 2ad
  // note that accel is always +ve in the move, and here we are considering deceleration, so it flips 
  // stopDistance will be -ve when vf > vi, i.e. when the move is accelerating... 
  float stopDistance = ((state.tail->vf * state.tail->vf) - (state.segVel * state.segVel)) / (2 * (- state.tail->accel));
  // if we are just past stopping distance, we should start slowing down 
  // (ideally we would anticipate the next integral, so that we don't overshoot *at all*)
  if(stopDistance > (state.tail->distance - state.segDistance)){
    state.segAccel = -state.tail->accel;
  } else {
    state.segAccel = state.tail->accel;
  }
  // OSAP::debug("stopdist:\t" + String(stopDistance, 2) + "\t" + String(segTail->distance - segDistance));
  // then we want to integrate our linear velocity,
  state.segVel += state.segAccel * delT;
  // no negative rates, that would be *erroneous* and also *bad* 
  if(state.segVel < -0.001F){
    state.segVel = 0.0F;
    OSAP::error("negative rate " + String(state.segVel), MEDIUM);
    return;
  }
  // and hit vmax ceilings, 
  if(state.segVel > state.tail->vmax) state.segVel = state.tail->vmax;
  // integrate per-segment position, 
  state.segDistance += state.segVel * delT;
  // check check, 
  // OSAP::debug("vel: " + String(segVel, 2) + " stopDist: " + String(stopDistance, 2) + " dist:" + String(segDistance, 2));
  // -------------------------------------------- Check Segment Completion 
  // are we done? goto the next move,
  if(state.segDistance >= state.tail->distance){
    // this move gets a reset, so queue observers know it's "empty" 
    state.tail->isReady = false;
    state.tail->isRunning = false;
    // was it us who was tapped for the ack ?
    if(state.tail->returnActuator == settings.ourActuatorID){
      // was previous ack picked up in time ? bad if not 
      if(segmentCompleteMsgLen != 0){
        axl_halt(AXL_HALT_MOVE_COMPLETE_NOT_PICKED);
      }
      // otherwise carry on, 
      segmentCompleteMsgLen = 0;
      // segment #, and our actuator ID... 
      ts_writeUint32(state.tail->segmentNumber, segmentCompleteMsg, &segmentCompleteMsgLen);
      ts_writeUint8(settings.ourActuatorID, segmentCompleteMsg, &segmentCompleteMsgLen);
      // our current position, for remote check-in ? this is (allegedly) superfluous 
      for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
        ts_writeFloat32(state.positions.axis[a], segmentCompleteMsg, &segmentCompleteMsgLen);
      }
      // that's it, the ack will be picked up... 
    } // ------------------------------ End Move Complete Write 
    // before we increment, stash this extra distance into the next segment... 
    state.segDistance = state.segDistance - state.tail->distance;
    // was it the last ?
    boolean wasLastMove = state.tail->isLastSegment;
    // now increment the pointer, 
    state.tail = state.tail->next;
    // is that ready ? then grab another, if not, set moves-complete, 
    if(!state.tail->isReady){
      // if this is true and our velocities != 0, we are probably starved:
      if(!wasLastMove) axl_halt(AXL_HALT_BUFFER_STARVED);
      // we're empty now, so 
      state.queueState = AXL_QUEUESTATE_EMPTY;
      // and set vels to 0, 
      for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
        state.velocities.axis[a] = 0.0F;
      }
      // if we're empty, don't goto pickup, just bail... 
      return; 
    } else {
      state.queueState = AXL_QUEUESTATE_INCREMENTING;
      state.tail->isRunning = true;
    }
    goto pickup;
  } // end is-move-complete section, 
  // -------------------------------------------- Integrate -> States... 
  #warning yeah agreed with the below, something potentially fkd with looping through two pickups in one run of this fn?
  // like, the pickup should happen on the next tick, non ? 
  // WARN: possible here to have two accel increases, w/ no position integration ? right ? 
  // now update each axis' vels & accels to mirror linear states, 
  // and integrate each position using this & the unit vector, 
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    // *update* accel and velocity states... we aren't using these, just mirroring from linear rep 
    // but we want to store them to read-out elsewhere... 
    state.accelerations.axis[a] = state.segAccel * state.tail->unitVector.axis[a];
    state.velocities.axis[a] = state.segVel * state.tail->unitVector.axis[a];
    // integrate position using blind integration: segments have absolute, but not global, position info, 
    // this is potentially fine-detail buggy, fair warning 
    float posDelta = state.velocities.axis[a] * delT;
    state.positions.axis[a] += posDelta;
    axl_onPositionDelta(a, posDelta, state.positions.axis[a]);
  }
} // end axl_integrator() 

// -------------------------------------------------------- Queue Utilities 

uint32_t nextSegmentNumber = 0;

void axl_addSegmentToQueue(axlPlannedSegment_t segment){
  // get a handle for the next, 
  if(state.head->isReady){
    OSAP::error("on moveToQueue, looks full ahead", MEDIUM);
    return;
  }
  // trust issues:
  if(segment.distance <= 0.0F){
    OSAP::error("zero distance move", MEDIUM);
    return;
  }
  // then we just stick it in there, don't we, trusting others... 
  // since we to state.head->isReady = true *at the end* we 
  // won't have the integrator step into this block during an interrupt... 
  // kind of awkward copy, innit? 
  // anyways these are the vals we get from the net:
  state.head->segmentNumber = segment.segmentNumber;
  if(nextSegmentNumber != state.head->segmentNumber){
    axl_halt(AXL_HALT_OUT_OF_ORDER_ARRIVAL);
    haltMessage = " rx'd " + String(segment.segmentNumber) + " expected " + String(nextSegmentNumber);
  } else {
    nextSegmentNumber ++; 
  }
  state.head->returnActuator = segment.returnActuator;
  state.head->isLastSegment = segment.isLastSegment;
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    state.head->unitVector.axis[a] = segment.unitVector.axis[a];
  }
  state.head->distance = segment.distance;
  state.head->vi = segment.vi; 
  state.head->accel = segment.accel;
  state.head->vmax = segment.vmax;
  state.head->vf = segment.vf;
  // and formulate our ack message... if it was req'd 
  if(state.head->returnActuator == settings.ourActuatorID){
    // was previous ack picked up in time ? bad if not 
    if(segmentAckMsgLen != 0){
      axl_halt(AXL_HALT_MOVE_COMPLETE_NOT_PICKED);
    }
    // otherwise carry on & write it... 
    segmentAckMsgLen = 0;
    // segment #, and our actuator ID... 
    ts_writeUint32(state.head->segmentNumber, segmentAckMsg, &segmentAckMsgLen);
    ts_writeUint8(settings.ourActuatorID, segmentAckMsg, &segmentAckMsgLen);
    // our current position, for remote check-in ? this is (allegedly) superfluous 
    for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
      ts_writeFloat32(state.positions.axis[a], segmentAckMsg, &segmentAckMsgLen);
    }
  }
  // and set these to allow read-out of the move
  state.head->isReady = true;
  state.mode = AXL_MODE_QUEUE;
  // alright then, increment the head, right? 
  state.head = state.head->next;
}

boolean axl_hasQueueSpace(void){
  // if next-to-fill hasn't been set clear, it's full... 
  if(state.head->next->isReady){
    return false;
  } else {
    return true;
  }
}

// -------------------------------------------------------- Getters 

// this one's awkward... it's hefty, I'm just going to serialize it straight out, 
uint16_t axl_getState(uint8_t* data){
  uint16_t wptr = 0;
  __disable_irq();
  // vect_t's 
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    ts_writeFloat32(state.positions.axis[a], data, &wptr);
    ts_writeFloat32(state.velocities.axis[a], data, &wptr);
    ts_writeFloat32(state.accelerations.axis[a], data, &wptr);
    ts_writeFloat32(state.target.axis[a], data, &wptr);
  }
  // inter-segment state, 
  ts_writeFloat32(state.segDistance, data, &wptr);
  ts_writeFloat32(state.segVel, data, &wptr);
  ts_writeFloat32(state.segAccel, data, &wptr);
  // mode, halt state, queue state, and queue pointer positions 
  ts_writeUint8(state.mode, data, &wptr);
  ts_writeUint8(state.haltState, data, &wptr);
  ts_writeUint8(state.queueState, data, &wptr);
  ts_writeUint8(state.head->indice, data, &wptr);
  ts_writeUint8(state.tail->indice, data, &wptr);
  __enable_irq();
  return wptr;
}

// useful... 
boolean axl_isMoving(void){
  // any velocity ? 
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    if(abs(state.velocities.axis[a]) > AXL_VEL_EPSILON) return true;
  }
  // any acceleration ?
  if(state.mode == AXL_MODE_ACCEL) return true;
  // any delta to targets ?
  if(state.mode == AXL_MODE_POSITION){
    for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
      if(abs(state.positions.axis[a] - state.target.axis[a]) > AXL_POSITIONING_EPSILON) return true;
    }
  }
  // any queued motion ?
  if(state.mode == AXL_MODE_QUEUE){
    if(state.tail != state.head) return true;
  }
  // if all pass,
  return false;
}

uint16_t axl_getSegmentAckMsg(uint8_t* msg){
  if(segmentAckMsgLen > 0){
    __disable_irq();
    memcpy(msg, segmentAckMsg, segmentAckMsgLen);
    uint16_t len = segmentAckMsgLen;
    segmentAckMsgLen = 0;
    __enable_irq();
    return len;
  } else {
    return 0;
  }
}

uint16_t axl_getSegmentCompleteMsg(uint8_t* msg){
  if(segmentCompleteMsgLen > 0){
    __disable_irq();
    memcpy(msg, segmentCompleteMsg, segmentCompleteMsgLen);
    uint16_t len = segmentCompleteMsgLen;
    segmentCompleteMsgLen = 0;
    __enable_irq();
    return len;
  } else {
    return 0;
  }
}

// -------------------------------------------------------- Halting

// request to halt all, i.e. set velocities to zero 
// also triggers a cascade msg-out if positive halt edge 
void axl_halt(uint8_t haltCode){
  if(haltCode == 0) return; // noop... 
  __disable_irq();
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    state.target.axis[a] = 0.0F;
  }
  state.mode = AXL_MODE_VELOCITY;
  __enable_irq();
  // if we weren't previously halted, report and cascade if not-soft, 
  if(state.haltState == 0){
    OSAP::error("halting for " + String(haltCode));
    state.haltState = haltCode;
    // if it isn't a "soft halt" then we should cascade it, 
    if(state.haltState != AXL_HALT_SOFT){
      haltPck[0] = AXL_HALT_CASCADE;
      haltPckLen = 1;
    }
  } 
}

uint16_t axl_getHaltPacket(uint8_t* data){
  if(haltPckLen){
    __disable_irq();
    memcpy(data, haltPck, haltPckLen);
    __enable_irq();
    ts_writeString(haltMessage, data, &haltPckLen);
    uint16_t len = haltPckLen;
    haltPckLen = 0;
    return len;
  } else {
    return 0;
  }
}

// -------------------------------------------------------- Setters 

void axl_setSettings(axlSettings_t _settings){
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    settings.accelLimits.axis[a] = _settings.accelLimits.axis[a];
    settings.velocityLimits.axis[a] = _settings.velocityLimits.axis[a];
  }
  settings.queueStartDelayMS = _settings.queueStartDelayMS;
  settings.queueStartTime = 0; // not really settable, that 
  settings.ourActuatorID = _settings.ourActuatorID;
}

// set *the current* position 
void axl_setPosition(vect_t posns){
  // ... might be good to block this in here ? 
  if(axl_isMoving()) return;
  // pretty simple AFAIK? 
  __disable_irq();
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    state.positions.axis[a] = posns.axis[a];
  }
  __enable_irq();
}

// request to go *to* this position, from current... 
void axl_setPositionTarget(vect_t targ){
  // no IRQ while we operate, are going to mess w/ targets etc, 
  // if this is slow, could stash temps & swap in, but i.e. delta might change during calculation... so 
  __disable_irq();
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    // assign new target, assuming we start from current position (won't be the case when linking) 
    state.target.axis[a] = targ.axis[a];
  }
  state.mode = AXL_MODE_POSITION;
  __enable_irq();
}

// request to go at this rate... 
void axl_setVelocityTarget(vect_t targ){
  __disable_irq();
  for(uint8_t a = 0; a < AXL_NUM_DOF; a ++){
    state.target.axis[a] = targ.axis[a];
  }
  state.mode = AXL_MODE_VELOCITY;
  __enable_irq();
}