/*
axl_config.h

holonic motion control 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2022

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the osap project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef AXL_CONFIG_H_
#define AXL_CONFIG_H_

// rate for integrator 
#define AXL_TICKER_INTERVAL_US 100
// number of DOF total,
#define AXL_NUM_DOF 4
// maximum length of the embedded queue 
#define AXL_QUEUE_LEN 32
// epsilon for positioning moves... i.e. when to stop / not-accel, 
#define AXL_POSITIONING_EPSILON 0.01F
#define AXL_POSITIONING_VEL_EPSILON 0.01F
#define AXL_VEL_EPSILON 0.01F

#define AXL_DEBUG_ADDMOVE false
#define AXL_DEBUG_QUEUES false 

#endif 