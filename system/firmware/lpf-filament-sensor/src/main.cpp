#include <Arduino.h>
#include "indicators.h"
#include "drivers/as5047.h"

#include "osape/core/osap.h"
#include "osape/vertices/endpoint.h"
#include "osape_arduino/vp_arduinoSerial.h"
#include "osape_ucbus/vb_ucBusDrop.h"

// -------------------------------------------------------- HALL SENSOR CODE 

// we'll run the LERP calibration & filtering here, since we are going to be delivering 
// this data to some other embedded element that does the actual control... 

float hallEstimate = 0.0f;   // current estimate 
float hallAlpha = 0.1f;         // trust in new data 

void updateHallEstimate(void){
  float newObservation = analogRead(A0);
  hallEstimate = newObservation * hallAlpha + hallEstimate * (1 - hallAlpha);
}

// lerp data,  
#define LERP_DATA_POINTS 4 
float lerp_diameters[LERP_DATA_POINTS] = { 1.0f, 1.5f, 1.8f, 2.4f };
float lerp_readings[LERP_DATA_POINTS] = { 2500.0f, 2013.0f , 1851.0f , 1547.0f };

float lerp(float reading){
  // find bounds:
  boolean found = false; 
  uint8_t bound = 0;
  for(uint8_t i = 0; i < LERP_DATA_POINTS - 1; i ++){
    if(reading < lerp_readings[i] && reading > lerp_readings[i + 1]){
      found = true;
      bound = i;
      break;
    }
  }
  if(found){
    // get posn-in-readings-span, should be 0-1... 
    float inSpan = (lerp_readings[bound] - reading) / (lerp_readings[bound] - lerp_readings[bound + 1]);
    // calculate span-plus 
    float val = lerp_diameters[bound] + inSpan * (lerp_diameters[bound + 1] - lerp_diameters[bound]);
    return val;
  } else {
    return 0.0f;
  }
}

// -------------------------------------------------------- 10ms (100hz) loop... 

// tick in MS
#define UPDATE_TICK 10
// wrapping aids, 
uint16_t lastReading = 0;
uint16_t encoderRange = 16383;
int32_t encoderWraps = 0;
// ute, 
float floatReading = 0.0F;
// estimates 
float posReading = 0.0F;
float posEstimate = 0.0F;
float lastPosEstimate = 0.0F;
float posAlpha = 0.2F; // as trust in new measurements 
float rateEstimate = 0.0F;
float rateAlpha = 0.05F;
float delT = 0.001F * (float)UPDATE_TICK;

void sensorLoop(void){
  // update & filter this, 
  updateHallEstimate();
  // get a new reading (transacting twice for freshy data) 
  // 1st bit is parity IIRC, so... 
  uint16_t reading = as5047_transact(AS5047_SPI_READ_POS);
  reading = as5047_transact(AS5047_SPI_READ_POS) & 0b0011111111111111;
  // calculate for wraps, 
  if(reading < 1000 && lastReading > encoderRange - 1000){
    encoderWraps ++;
  } else if (reading > encoderRange - 1000 && lastReading < 1000){
    encoderWraps --;
  }
  lastReading = reading; 
  // let's go from 0-16383 to 0-2PI, floating points, for these maths:
  // i.e. store it all as radians, 
  floatReading = ((float)(reading) * TWO_PI) / 16383.0F;
  // the complete position is the wrap-count... plus the current reading, 
  posReading = ((float)encoderWraps * TWO_PI) + floatReading;
  // filter that... 
  posEstimate = (float)posReading * posAlpha + posEstimate * (1 - posAlpha);
  // rates... 
  rateEstimate = ((posEstimate - lastPosEstimate) / delT) * rateAlpha + rateEstimate * (1 - rateAlpha);
  lastPosEstimate = posEstimate;
}

// -------------------------------------------------------- OSAP 

OSAP osap("filament-sensor");

// -------------------------------------------------------- VPORTS 

VPort_ArduinoSerial vpUSBSerial(&osap, "arduinoUSBSerial", &Serial);

VBus_UCBusDrop vbUCBusDrop(&osap, "ucBusDrop");

// ---------------------------------------------- HALL ENDPOINT 

Endpoint bundleEP(&osap, "bundle");

uint8_t dataBytes[16];       // temp write buffer 

void updateEndpoint(void){
  // LERP the current observation 
  float lerpd = lerp(hallEstimate);
  // write -> bytes, 
  uint16_t wptr = 0;
  ts_writeFloat32(lerpd, dataBytes, &wptr);
  ts_writeFloat32(posEstimate, dataBytes, &wptr);
  ts_writeFloat32(rateEstimate, dataBytes, &wptr);
  // now write that to the endpoint... 
  bundleEP.write(dataBytes, 16);
}

// -------------------------------------------------------- SYSTEM SETUP 

void setup() {
  //while(!Serial);
  // setup indicators 
  CLKLIGHT_SETUP;
  BUSLIGHT_SETUP;
  // hall effect doesn't need setup... arduino does that ?
  analogReference(AR_INTERNAL2V0);
  analogReadResolution(12);
  // setup comms 
  vpUSBSerial.begin();
  vbUCBusDrop.begin(12);
  // setup AS5047 werk
  as5047_setup();
}

// in ms, how often to update hall 
#define PUBLISH_DIV 5
unsigned long last_tick = 0;
uint8_t divs = 0;

void loop() {
  // sys 
  osap.loop();
  // blink 
  if(millis() > last_tick + UPDATE_TICK){
    sensorLoop();
    last_tick = millis();
    divs ++; 
    if(divs >= PUBLISH_DIV - 1){
      divs = 0;
      BUSLIGHT_TOGGLE;
      updateEndpoint();
    }
  }
}

void ucBusDrop_onPacketARx(uint8_t* inBufferA, volatile uint16_t len){} 

void ucBusDrop_onRxISR(void){} 