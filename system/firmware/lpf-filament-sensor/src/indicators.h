// circuit specific indicators: D21 filament sensor, w/ ucbus interface 

// pin helper macros 
#define PIN_BM(pin) (uint32_t)(1 << pin)
#define PIN_HI(port, pin) PORT->Group[port].OUTSET.reg = PIN_BM(pin) 
#define PIN_LO(port, pin) PORT->Group[port].OUTCLR.reg = PIN_BM(pin) 
#define PIN_TGL(port, pin) PORT->Group[port].OUTTGL.reg = PIN_BM(pin)
#define PIN_SETUP_OUTPUT(port, pin) PORT->Group[port].DIRSET.reg = PIN_BM(pin) 
#define PIN_SETUP_INPUT(port, pin) PORT->Group[port].DIRCLR.reg = PIN_BM(pin)

// PA23: clock 
#define CLKLIGHT_ON PIN_HI(0, 23)
#define CLKLIGHT_OFF PIN_LO(0, 23)
#define CLKLIGHT_TOGGLE PIN_TGL(0, 23)
#define CLKLIGHT_SETUP PIN_SETUP_OUTPUT(0, 23); CLKLIGHT_OFF

// PA22: bus light 
#define BUSLIGHT_ON PIN_HI(0, 22)
#define BUSLIGHT_OFF PIN_LO(0, 22)
#define BUSLIGHT_TOGGLE PIN_TGL(0, 22)
#define BUSLIGHT_SETUP PIN_SETUP_OUTPUT(0, 22); BUSLIGHT_OFF

#define ERRLIGHT_ON 
#define ERRLIGHT_OFF 