/*
drivers/as5047.h

reads an as5047x on SER0 on SAMD21

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2022

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo
projects. Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#include "as5047.h"
#include "../indicators.h"

/* 
we have the AS5047 on
PA08: 0-0 MOSI
PA09: 0-1 CLK
PA10: 0-2 CS
PA11: 0-3 MISO 
*/

#define SER0SPI SERCOM0->SPI

void as5047_setup(void){
  // pin configs, 
  PIN_SETUP_OUTPUT(0, 8);
  PIN_SETUP_OUTPUT(0, 9);
  PIN_SETUP_OUTPUT(0, 10);
  PIN_SETUP_INPUT(0, 11);
  // mux pins, 
  PORT_WRCONFIG_Type wrconfig;
  wrconfig.bit.WRPMUX = 1;          // writes to pmux, 
  wrconfig.bit.WRPINCFG = 1;        // writes to pinconfig 
  wrconfig.bit.PMUX = MUX_PA08C_SERCOM0_PAD0;
  wrconfig.bit.PMUXEN = 1;          // writing to pmux... 
  wrconfig.bit.HWSEL = 0;           // writing to lower half
  wrconfig.bit.PINMASK = PIN_BM(8); // mask at pin 8, 
  PORT->Group[0].WRCONFIG.reg = wrconfig.reg; 
  wrconfig.bit.PMUX = MUX_PA09C_SERCOM0_PAD1;
  wrconfig.bit.PINMASK = PIN_BM(9); // mask at pin 9, 
  // PIN10 is CS, we'll operate it manually... 
  // PORT->Group[0].WRCONFIG.reg = wrconfig.reg; 
  // wrconfig.bit.PMUX = MUX_PA10C_SERCOM0_PAD2;
  // wrconfig.bit.PINMASK = PIN_BM(10); // mask at pin 10, 
  PORT->Group[0].WRCONFIG.reg = wrconfig.reg; 
  wrconfig.bit.PMUX = MUX_PA11C_SERCOM0_PAD3;
  wrconfig.bit.PINMASK = PIN_BM(11); // mask at pin 11, 
  PORT->Group[0].WRCONFIG.reg = wrconfig.reg; 
  // ok... now setup the sercom, right? 
  PM->APBCMASK.reg |= PM_APBCMASK_SERCOM0;
  // hook that up to main CPU clock, 
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 |
                      GCLK_CLKCTRL_ID_SERCOM0_CORE;
  while (GCLK->STATUS.bit.SYNCBUSY);
  // do the actual setup... 
  SER0SPI.CTRLA.bit.SWRST = 1;
  while(SER0SPI.SYNCBUSY.bit.SWRST);
  // and setup... 
  SER0SPI.CTRLA.reg = //SERCOM_SPI_CTRLA_CPOL |
                      SERCOM_SPI_CTRLA_CPHA |
                      SERCOM_SPI_CTRLA_DIPO(3) |
                      SERCOM_SPI_CTRLA_DOPO(0) |
                      SERCOM_SPI_CTRLA_MODE(3);
  SER0SPI.CTRLB.reg = SERCOM_SPI_CTRLB_RXEN;
  SER0SPI.BAUD.reg = SERCOM_SPI_BAUD_BAUD(12);
  // enable ! 
  SER0SPI.CTRLA.bit.ENABLE = 1;
  while(SER0SPI.SYNCBUSY.bit.ENABLE);
}

// 00 : ? 
// 01 : ? 
// 11 : ?
// 10 : ?

uint16_t as5047_transact(uint16_t outWord){
  // probbo *not* going to just blind toggle, now that we're hooked up to peripheral 
  uint16_t inWord = 0;
  PIN_LO(0, 10);
  SER0SPI.DATA.reg = (outWord >> 8);
  while(!SER0SPI.INTFLAG.bit.TXC);
  inWord |=  SER0SPI.DATA.reg << 8;
  SER0SPI.DATA.reg = (outWord & 255);
  while(!SER0SPI.INTFLAG.bit.TXC);
  inWord |= SER0SPI.DATA.reg & 255;
  PIN_HI(0, 10);
  return inWord;
}