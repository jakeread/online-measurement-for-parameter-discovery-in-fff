/*
drivers/as5047.h

reads an as5047x on SER0 on SAMD21

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2022

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo
projects. Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#ifndef ENC_AS5047_H_
#define ENC_AS5047_H_

#include <Arduino.h>

#define AS5047_SPI_READ_POS (0b1100000000000000 | 0x3FFF)
#define AS5047_SPI_READ_POS_UNC (0b1100000000000000 | 0x3FFE)
#define AS5047_SPI_NO_OP (0b1000000000000000)

#define AS5047_SPI_READ_MAG (0b1100000000000000 | 0x3FFD)
#define AS5047_SPI_READ_DIAG (0b1100000000000000 | 0x3FFC)

typedef struct {
    boolean magHi;
    boolean magLo;
    boolean cordicOverflow;
    boolean compensationComplete;
    uint8_t angularGainCorrection;
} as5047_diag_t;

void as5047_setup(void);
uint16_t as5047_transact(uint16_t outWord);

#endif 