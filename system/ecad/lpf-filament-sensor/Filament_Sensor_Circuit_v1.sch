<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="7" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="microcontrollers">
<packages>
<package name="TQFP-32">
<wire x1="-3.55" y1="-3.55" x2="-3.55" y2="3.55" width="0.127" layer="51"/>
<wire x1="-3.55" y1="3.55" x2="3.55" y2="3.55" width="0.127" layer="51"/>
<wire x1="3.55" y1="3.55" x2="3.55" y2="-3.55" width="0.127" layer="51"/>
<wire x1="3.55" y1="-3.55" x2="-3.55" y2="-3.55" width="0.127" layer="51"/>
<wire x1="-3.25" y1="3.55" x2="-3.55" y2="3.55" width="0.127" layer="21"/>
<wire x1="-3.55" y1="3.55" x2="-3.55" y2="3.25" width="0.127" layer="21"/>
<wire x1="3.25" y1="3.55" x2="3.55" y2="3.55" width="0.127" layer="21"/>
<wire x1="3.55" y1="3.55" x2="3.55" y2="3.25" width="0.127" layer="21"/>
<wire x1="-3.55" y1="-3.25" x2="-3.55" y2="-3.55" width="0.127" layer="21"/>
<wire x1="-3.55" y1="-3.55" x2="-3.25" y2="-3.55" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3.55" x2="3.55" y2="-3.55" width="0.127" layer="21"/>
<wire x1="3.55" y1="-3.55" x2="3.55" y2="-3.25" width="0.127" layer="21"/>
<text x="-3.202909375" y="5.80526875" size="0.8135375" layer="25">&gt;NAME</text>
<text x="-3.40625" y="-6.211390625" size="0.81429375" layer="27">&gt;VALUE</text>
<circle x="-5.8" y="2.8" radius="0.1" width="0.2" layer="21"/>
<circle x="-5.8" y="2.8" radius="0.1" width="0.2" layer="51"/>
<smd name="1" x="-4.18" y="2.8" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="2" x="-4.18" y="2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="3" x="-4.18" y="1.2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="4" x="-4.18" y="0.4" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="5" x="-4.18" y="-0.4" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="6" x="-4.18" y="-1.2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="7" x="-4.18" y="-2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="8" x="-4.18" y="-2.8" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="9" x="-2.8" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="10" x="-2" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="11" x="-1.2" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="12" x="-0.4" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="13" x="0.4" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="14" x="1.2" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="15" x="2" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="16" x="2.8" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="17" x="4.18" y="-2.8" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R180"/>
<smd name="18" x="4.18" y="-2" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R180"/>
<smd name="19" x="4.18" y="-1.2" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R180"/>
<smd name="20" x="4.18" y="-0.4" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R180"/>
<smd name="21" x="4.18" y="0.4" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R180"/>
<smd name="22" x="4.18" y="1.2" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R180"/>
<smd name="23" x="4.18" y="2" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R180"/>
<smd name="24" x="4.18" y="2.8" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R180"/>
<smd name="25" x="2.8" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R270"/>
<smd name="26" x="2" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R270"/>
<smd name="27" x="1.2" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R270"/>
<smd name="28" x="0.4" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R270"/>
<smd name="29" x="-0.4" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R270"/>
<smd name="30" x="-1.2" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R270"/>
<smd name="31" x="-2" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R270"/>
<smd name="32" x="-2.8" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R270"/>
</package>
<package name="TQFP-32-FAB">
<wire x1="-3.55" y1="-3.55" x2="-3.55" y2="3.55" width="0.127" layer="51"/>
<wire x1="-3.55" y1="3.55" x2="3.55" y2="3.55" width="0.127" layer="51"/>
<wire x1="3.55" y1="3.55" x2="3.55" y2="-3.55" width="0.127" layer="51"/>
<wire x1="3.55" y1="-3.55" x2="-3.55" y2="-3.55" width="0.127" layer="51"/>
<wire x1="-3.25" y1="3.55" x2="-3.55" y2="3.55" width="0.127" layer="21"/>
<wire x1="-3.55" y1="3.55" x2="-3.55" y2="3.25" width="0.127" layer="21"/>
<wire x1="3.25" y1="3.55" x2="3.55" y2="3.55" width="0.127" layer="21"/>
<wire x1="3.55" y1="3.55" x2="3.55" y2="3.25" width="0.127" layer="21"/>
<wire x1="-3.55" y1="-3.25" x2="-3.55" y2="-3.55" width="0.127" layer="21"/>
<wire x1="-3.55" y1="-3.55" x2="-3.25" y2="-3.55" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3.55" x2="3.55" y2="-3.55" width="0.127" layer="21"/>
<wire x1="3.55" y1="-3.55" x2="3.55" y2="-3.25" width="0.127" layer="21"/>
<text x="-3.202909375" y="5.80526875" size="0.8135375" layer="25">&gt;NAME</text>
<text x="-3.40625" y="-6.211390625" size="0.81429375" layer="27">&gt;VALUE</text>
<circle x="-5.8" y="2.8" radius="0.1" width="0.2" layer="21"/>
<circle x="-5.8" y="2.8" radius="0.1" width="0.2" layer="51"/>
<smd name="1" x="-4.355" y="2.8" dx="1.25" dy="0.35" layer="1" roundness="25"/>
<smd name="2" x="-4.18" y="2" dx="1.6" dy="0.35" layer="1" roundness="25"/>
<smd name="3" x="-4.18" y="1.2" dx="1.6" dy="0.35" layer="1" roundness="25"/>
<smd name="4" x="-4.18" y="0.4" dx="1.6" dy="0.35" layer="1" roundness="25"/>
<smd name="5" x="-4.18" y="-0.4" dx="1.6" dy="0.35" layer="1" roundness="25"/>
<smd name="6" x="-4.18" y="-1.2" dx="1.6" dy="0.35" layer="1" roundness="25"/>
<smd name="7" x="-4.18" y="-2" dx="1.6" dy="0.35" layer="1" roundness="25"/>
<smd name="8" x="-4.355" y="-2.8" dx="1.25" dy="0.35" layer="1" roundness="25"/>
<smd name="9" x="-2.8" y="-4.355" dx="1.25" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="10" x="-2" y="-4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="11" x="-1.2" y="-4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="12" x="-0.4" y="-4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="13" x="0.4" y="-4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="14" x="1.2" y="-4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="15" x="2" y="-4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="16" x="2.8" y="-4.355" dx="1.25" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="17" x="4.355" y="-2.8" dx="1.25" dy="0.35" layer="1" roundness="25" rot="R180"/>
<smd name="18" x="4.18" y="-2" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R180"/>
<smd name="19" x="4.18" y="-1.2" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R180"/>
<smd name="20" x="4.18" y="-0.4" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R180"/>
<smd name="21" x="4.18" y="0.4" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R180"/>
<smd name="22" x="4.18" y="1.2" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R180"/>
<smd name="23" x="4.18" y="2" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R180"/>
<smd name="24" x="4.355" y="2.8" dx="1.25" dy="0.35" layer="1" roundness="25" rot="R180"/>
<smd name="25" x="2.8" y="4.355" dx="1.25" dy="0.35" layer="1" roundness="25" rot="R270"/>
<smd name="26" x="2" y="4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R270"/>
<smd name="27" x="1.2" y="4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R270"/>
<smd name="28" x="0.4" y="4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R270"/>
<smd name="29" x="-0.4" y="4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R270"/>
<smd name="30" x="-1.2" y="4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R270"/>
<smd name="31" x="-2" y="4.18" dx="1.6" dy="0.35" layer="1" roundness="25" rot="R270"/>
<smd name="32" x="-2.8" y="4.355" dx="1.25" dy="0.35" layer="1" roundness="25" rot="R270"/>
</package>
</packages>
<symbols>
<symbol name="ATSAMD21E18A-AF">
<wire x1="48.26" y1="-33.02" x2="-20.32" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-33.02" x2="-20.32" y2="35.56" width="0.254" layer="94"/>
<wire x1="-20.32" y1="35.56" x2="48.26" y2="35.56" width="0.254" layer="94"/>
<wire x1="48.26" y1="35.56" x2="48.26" y2="-33.02" width="0.254" layer="94"/>
<text x="-20.3338" y="35.5978" size="1.780409375" layer="95">&gt;NAME</text>
<text x="-20.338" y="-35.614" size="1.78115" layer="96">&gt;VALUE</text>
<pin name="(ADA_PXL)PA00/TCC2-0/SER1-0/XIN32" x="53.34" y="33.02" length="middle" rot="R180"/>
<pin name="(ADA_PXL)PA01/TCC2-1/SER1-1/XOUT32" x="53.34" y="30.48" length="middle" rot="R180"/>
<pin name="(ADA_D1A1)PA02/AIN-0/DAC-0" x="53.34" y="27.94" length="middle" rot="R180"/>
<pin name="PA03/VREFA" x="53.34" y="25.4" length="middle" rot="R180"/>
<pin name="(ADA_D0_TX)PA04/VREFB/AIN4/AIN0/TCC0-0/SER0-0" x="53.34" y="22.86" length="middle" rot="R180"/>
<pin name="(ADA_D2_RX)PA05/AIN5/AIN1/TCC0-1/SER0-1" x="53.34" y="20.32" length="middle" rot="R180"/>
<pin name="PA06/AIN6/AIN2/TCC1-0/SER0-2" x="53.34" y="17.78" length="middle" rot="R180"/>
<pin name="PA07/AIN7/AIN3/TCC1-1/SER0-3" x="53.34" y="15.24" length="middle" rot="R180"/>
<pin name="VDDANA" x="-25.4" y="25.4" length="middle" direction="pwr"/>
<pin name="GND" x="-25.4" y="-30.48" length="middle" direction="pwr"/>
<pin name="PA08/AIN16/TCC0-0/TCC1-2/SER0-0/SER2-0" x="53.34" y="12.7" length="middle" rot="R180"/>
<pin name="PA09/AIN17/TCC0-1/TCC1-3/SER0-1/SER2-1" x="53.34" y="10.16" length="middle" rot="R180"/>
<pin name="PA10/AIN18/TCC0-2/TCC1-0/SER0-2/SER2-2" x="53.34" y="7.62" length="middle" rot="R180"/>
<pin name="PA11/AIN19/TCC0-3/TCC1-1/SER0-3/SER2-3" x="53.34" y="5.08" length="middle" rot="R180"/>
<pin name="PA14/TC3-1/TCC0-4/SER2-2/XIN" x="53.34" y="2.54" length="middle" rot="R180"/>
<pin name="PA15/TC3-1/TCC0-5/SER2-3/XOUT" x="53.34" y="0" length="middle" rot="R180"/>
<pin name="PA16/TCC2-0/TCC0-6/SER1-0/SER3-0" x="53.34" y="-2.54" length="middle" rot="R180"/>
<pin name="PA17/TCC2-1/TCC0-7/SER1-1/SER3-1" x="53.34" y="-5.08" length="middle" rot="R180"/>
<pin name="PA18/TC3-0/TCC0-2/SER1-2/SER3-2" x="53.34" y="-7.62" length="middle" rot="R180"/>
<pin name="PA19/TC3-1/TCC0-3/SER1-3/SER3-3" x="53.34" y="-10.16" length="middle" rot="R180"/>
<pin name="PA22/TC4-0/TCC0-4/SER3-0" x="53.34" y="-12.7" length="middle" rot="R180"/>
<pin name="(ADA_D13)PA23/TC4-1/TCC0-5/SER3-1/USB-SOF" x="53.34" y="-15.24" length="middle" rot="R180"/>
<pin name="PA24/TC5-0/TCC1-2/SER3-2/USB-DM" x="53.34" y="-17.78" length="middle" rot="R180"/>
<pin name="PA25/TC5-1/TCC1-3/SER3-3/USB-DP" x="53.34" y="-20.32" length="middle" rot="R180"/>
<pin name="PA27" x="53.34" y="-22.86" length="middle" rot="R180"/>
<pin name="!RESET" x="-25.4" y="-17.78" length="middle" direction="in"/>
<pin name="PA28" x="53.34" y="-25.4" length="middle" rot="R180"/>
<pin name="VDDCORE" x="-25.4" y="17.78" length="middle" direction="pwr"/>
<pin name="VDDIN" x="-25.4" y="33.02" length="middle" direction="pwr"/>
<pin name="PA30/TCC1-0/SER1-2/SWDCLK" x="53.34" y="-27.94" length="middle" rot="R180"/>
<pin name="PA31/TCC1-1/SER1-3/SWDIO" x="53.34" y="-30.48" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATSAMD21E18A-AF" prefix="U">
<description>The SAM D21 is a series of low-power microcontrollers using the 32-bit ARM®
 Cortex®
-M0+ processor,
and ranging from 32- to 64-pins with up to 256KB Flash and 32KB of SRAM. The SAM D21 operate at a
maximum frequency of 48MHz and reach 2.46 CoreMark®
/MHz. &lt;a href="https://pricing.snapeda.com/parts/ATSAMD21E18A-AF/Microchip/view-part?ref=eda"&gt;Check prices&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ATSAMD21E18A-AF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP-32">
<connects>
<connect gate="G$1" pin="!RESET" pad="26"/>
<connect gate="G$1" pin="(ADA_D0_TX)PA04/VREFB/AIN4/AIN0/TCC0-0/SER0-0" pad="5"/>
<connect gate="G$1" pin="(ADA_D13)PA23/TC4-1/TCC0-5/SER3-1/USB-SOF" pad="22"/>
<connect gate="G$1" pin="(ADA_D1A1)PA02/AIN-0/DAC-0" pad="3"/>
<connect gate="G$1" pin="(ADA_D2_RX)PA05/AIN5/AIN1/TCC0-1/SER0-1" pad="6"/>
<connect gate="G$1" pin="(ADA_PXL)PA00/TCC2-0/SER1-0/XIN32" pad="1"/>
<connect gate="G$1" pin="(ADA_PXL)PA01/TCC2-1/SER1-1/XOUT32" pad="2"/>
<connect gate="G$1" pin="GND" pad="10 28"/>
<connect gate="G$1" pin="PA03/VREFA" pad="4"/>
<connect gate="G$1" pin="PA06/AIN6/AIN2/TCC1-0/SER0-2" pad="7"/>
<connect gate="G$1" pin="PA07/AIN7/AIN3/TCC1-1/SER0-3" pad="8"/>
<connect gate="G$1" pin="PA08/AIN16/TCC0-0/TCC1-2/SER0-0/SER2-0" pad="11"/>
<connect gate="G$1" pin="PA09/AIN17/TCC0-1/TCC1-3/SER0-1/SER2-1" pad="12"/>
<connect gate="G$1" pin="PA10/AIN18/TCC0-2/TCC1-0/SER0-2/SER2-2" pad="13"/>
<connect gate="G$1" pin="PA11/AIN19/TCC0-3/TCC1-1/SER0-3/SER2-3" pad="14"/>
<connect gate="G$1" pin="PA14/TC3-1/TCC0-4/SER2-2/XIN" pad="15"/>
<connect gate="G$1" pin="PA15/TC3-1/TCC0-5/SER2-3/XOUT" pad="16"/>
<connect gate="G$1" pin="PA16/TCC2-0/TCC0-6/SER1-0/SER3-0" pad="17"/>
<connect gate="G$1" pin="PA17/TCC2-1/TCC0-7/SER1-1/SER3-1" pad="18"/>
<connect gate="G$1" pin="PA18/TC3-0/TCC0-2/SER1-2/SER3-2" pad="19"/>
<connect gate="G$1" pin="PA19/TC3-1/TCC0-3/SER1-3/SER3-3" pad="20"/>
<connect gate="G$1" pin="PA22/TC4-0/TCC0-4/SER3-0" pad="21"/>
<connect gate="G$1" pin="PA24/TC5-0/TCC1-2/SER3-2/USB-DM" pad="23"/>
<connect gate="G$1" pin="PA25/TC5-1/TCC1-3/SER3-3/USB-DP" pad="24"/>
<connect gate="G$1" pin="PA27" pad="25"/>
<connect gate="G$1" pin="PA28" pad="27"/>
<connect gate="G$1" pin="PA30/TCC1-0/SER1-2/SWDCLK" pad="31"/>
<connect gate="G$1" pin="PA31/TCC1-1/SER1-3/SWDIO" pad="32"/>
<connect gate="G$1" pin="VDDANA" pad="9"/>
<connect gate="G$1" pin="VDDCORE" pad="29"/>
<connect gate="G$1" pin="VDDIN" pad="30"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Warning"/>
<attribute name="DESCRIPTION" value=" ARM® Cortex®-M0+ Automotive, AEC-Q100, SAM D21E, Functional Safety (FuSa) Microcontroller IC 32-Bit 48MHz 256KB (256K x 8) FLASH 32-TQFP (7x7) "/>
<attribute name="MF" value="Microchip"/>
<attribute name="MP" value="ATSAMD21E18A-AF"/>
<attribute name="PACKAGE" value="TQFP-32 Microchip"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/ATSAMD21E18A-AF/?ref=eda"/>
</technology>
</technologies>
</device>
<device name="FAB" package="TQFP-32-FAB">
<connects>
<connect gate="G$1" pin="!RESET" pad="26"/>
<connect gate="G$1" pin="(ADA_D0_TX)PA04/VREFB/AIN4/AIN0/TCC0-0/SER0-0" pad="5"/>
<connect gate="G$1" pin="(ADA_D13)PA23/TC4-1/TCC0-5/SER3-1/USB-SOF" pad="22"/>
<connect gate="G$1" pin="(ADA_D1A1)PA02/AIN-0/DAC-0" pad="3"/>
<connect gate="G$1" pin="(ADA_D2_RX)PA05/AIN5/AIN1/TCC0-1/SER0-1" pad="6"/>
<connect gate="G$1" pin="(ADA_PXL)PA00/TCC2-0/SER1-0/XIN32" pad="1"/>
<connect gate="G$1" pin="(ADA_PXL)PA01/TCC2-1/SER1-1/XOUT32" pad="2"/>
<connect gate="G$1" pin="GND" pad="10 28"/>
<connect gate="G$1" pin="PA03/VREFA" pad="4"/>
<connect gate="G$1" pin="PA06/AIN6/AIN2/TCC1-0/SER0-2" pad="7"/>
<connect gate="G$1" pin="PA07/AIN7/AIN3/TCC1-1/SER0-3" pad="8"/>
<connect gate="G$1" pin="PA08/AIN16/TCC0-0/TCC1-2/SER0-0/SER2-0" pad="11"/>
<connect gate="G$1" pin="PA09/AIN17/TCC0-1/TCC1-3/SER0-1/SER2-1" pad="12"/>
<connect gate="G$1" pin="PA10/AIN18/TCC0-2/TCC1-0/SER0-2/SER2-2" pad="13"/>
<connect gate="G$1" pin="PA11/AIN19/TCC0-3/TCC1-1/SER0-3/SER2-3" pad="14"/>
<connect gate="G$1" pin="PA14/TC3-1/TCC0-4/SER2-2/XIN" pad="15"/>
<connect gate="G$1" pin="PA15/TC3-1/TCC0-5/SER2-3/XOUT" pad="16"/>
<connect gate="G$1" pin="PA16/TCC2-0/TCC0-6/SER1-0/SER3-0" pad="17"/>
<connect gate="G$1" pin="PA17/TCC2-1/TCC0-7/SER1-1/SER3-1" pad="18"/>
<connect gate="G$1" pin="PA18/TC3-0/TCC0-2/SER1-2/SER3-2" pad="19"/>
<connect gate="G$1" pin="PA19/TC3-1/TCC0-3/SER1-3/SER3-3" pad="20"/>
<connect gate="G$1" pin="PA22/TC4-0/TCC0-4/SER3-0" pad="21"/>
<connect gate="G$1" pin="PA24/TC5-0/TCC1-2/SER3-2/USB-DM" pad="23"/>
<connect gate="G$1" pin="PA25/TC5-1/TCC1-3/SER3-3/USB-DP" pad="24"/>
<connect gate="G$1" pin="PA27" pad="25"/>
<connect gate="G$1" pin="PA28" pad="27"/>
<connect gate="G$1" pin="PA30/TCC1-0/SER1-2/SWDCLK" pad="31"/>
<connect gate="G$1" pin="PA31/TCC1-1/SER1-3/SWDIO" pad="32"/>
<connect gate="G$1" pin="VDDANA" pad="9"/>
<connect gate="G$1" pin="VDDCORE" pad="29"/>
<connect gate="G$1" pin="VDDIN" pad="30"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors" urn="urn:adsk.eagle:library:513">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SAMTECH_FTSH-105-01" urn="urn:adsk.eagle:footprint:37965/1" library_version="1">
<description>&lt;h3&gt;ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.05"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.samtec.com/ftppub/cpdf/FTSH-1XX-XX-XXX-DV-XXX-MKT.pdf"&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CORTEX_DEBUG&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.175" y1="1.7145" x2="3.175" y2="-1.7145" width="0.127" layer="51"/>
<wire x1="3.175" y1="-1.7145" x2="-3.175" y2="-1.7145" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.7145" x2="-3.175" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="1.7145" x2="3.175" y2="1.7145" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.7145" x2="-3.175" y2="1.7145" width="0.127" layer="51"/>
<wire x1="-3.175" y1="1.7145" x2="-3.0226" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="3.0226" y1="1.7145" x2="3.175" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.7145" x2="3.175" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.7145" x2="3.0226" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="-3.0226" y1="-1.7145" x2="-3.175" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="-2.0574" y1="1.7145" x2="-1.7526" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="-0.7874" y1="1.7145" x2="-0.4826" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="0.4826" y1="1.7145" x2="0.7874" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="1.7526" y1="1.7145" x2="2.0574" y2="1.7145" width="0.2032" layer="21"/>
<wire x1="2.0574" y1="-1.7145" x2="1.7526" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="0.7874" y1="-1.7145" x2="0.4826" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="-0.4826" y1="-1.7145" x2="-0.7874" y2="-1.7145" width="0.2032" layer="21"/>
<wire x1="-1.7526" y1="-1.7145" x2="-2.0574" y2="-1.7145" width="0.2032" layer="21"/>
<rectangle x1="-0.2032" y1="1.7145" x2="0.2032" y2="2.921" layer="51"/>
<rectangle x1="1.0668" y1="1.7145" x2="1.4732" y2="2.921" layer="51"/>
<rectangle x1="2.3368" y1="1.7145" x2="2.7432" y2="2.921" layer="51"/>
<rectangle x1="-1.4732" y1="1.7145" x2="-1.0668" y2="2.921" layer="51"/>
<rectangle x1="-2.7432" y1="1.7145" x2="-2.3368" y2="2.921" layer="51"/>
<rectangle x1="-0.2032" y1="-2.921" x2="0.2032" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="-1.4732" y1="-2.921" x2="-1.0668" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="-2.7432" y1="-2.921" x2="-2.3368" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="1.0668" y1="-2.921" x2="1.4732" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="2.3368" y1="-2.921" x2="2.7432" y2="-1.7145" layer="51" rot="R180"/>
<smd name="6" x="0" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="8" x="1.27" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="10" x="2.54" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="4" x="-1.27" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="2" x="-2.54" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="1" x="-2.54" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="3" x="-1.27" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="5" x="0" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="7" x="1.27" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="9" x="2.54" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<text x="-1.3462" y="0.4572" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.7018" y="-0.9652" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-PTH-1.27MM" urn="urn:adsk.eagle:footprint:37966/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x5 ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;tDoc (51) layer border represents maximum dimensions of plastic housing.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://portal.fciconnect.com/Comergent//fci/drawing/20021111.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="8" x="1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="6" x="0" y="0.635" drill="0.508" diameter="1"/>
<pad name="4" x="-1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="2" x="-2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="10" x="2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="7" x="1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="5" x="0" y="-0.635" drill="0.508" diameter="1"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.508" diameter="1"/>
<pad name="9" x="2.54" y="-0.635" drill="0.508" diameter="1"/>
<wire x1="-3.403" y1="-1.021" x2="-3.403" y2="-0.259" width="0.254" layer="21"/>
<wire x1="3.175" y1="1.715" x2="-3.175" y2="1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="1.715" x2="-3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.715" x2="3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="3.175" y1="-1.715" x2="3.175" y2="1.715" width="0.127" layer="51"/>
<text x="-1.5748" y="1.9304" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.8288" y="-2.4638" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SAMTECH_FTSH-105-01" urn="urn:adsk.eagle:package:38289/1" type="box" library_version="1">
<description>ARM Cortex Debug Connector (10-pin)
Specifications:
Pin count:10
Pin pitch:0.05"

Datasheet referenced for footprint
Example device(s):
CORTEX_DEBUG
</description>
<packageinstances>
<packageinstance name="SAMTECH_FTSH-105-01"/>
</packageinstances>
</package3d>
<package3d name="2X5-PTH-1.27MM" urn="urn:adsk.eagle:package:38290/1" type="box" library_version="1">
<description>Plated Through Hole - 2x5 ARM Cortex Debug Connector (10-pin)
tDoc (51) layer border represents maximum dimensions of plastic housing.
Specifications:
Pin count:10
Pin pitch:1.27mm

Datasheet referenced for footprint
Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5-PTH-1.27MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CORTEX_DEBUG" urn="urn:adsk.eagle:symbol:37964/1" library_version="1">
<description>&lt;h3&gt;Cortex Debug Connector&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<pin name="VCC" x="-15.24" y="5.08" length="short"/>
<pin name="GND@3" x="-15.24" y="2.54" length="short"/>
<pin name="GND@5" x="-15.24" y="0" length="short"/>
<pin name="KEY" x="-15.24" y="-2.54" length="short"/>
<pin name="GNDDTCT" x="-15.24" y="-5.08" length="short"/>
<pin name="!RESET" x="17.78" y="-5.08" length="short" rot="R180"/>
<pin name="NC/TDI" x="17.78" y="-2.54" length="short" rot="R180"/>
<pin name="SWO/TDO" x="17.78" y="0" length="short" rot="R180"/>
<pin name="SWDCLK/TCK" x="17.78" y="2.54" length="short" rot="R180"/>
<pin name="SWDIO/TMS" x="17.78" y="5.08" length="short" rot="R180"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="15.24" y2="7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="7.62" x2="15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<text x="-12.7" y="7.874" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-12.7" y="-9.906" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CORTEX_DEBUG" urn="urn:adsk.eagle:component:38384/1" prefix="J" library_version="1">
<description>&lt;h3&gt;Cortex Debug Connector - 10 pin&lt;/h3&gt;
&lt;p&gt;Supports JTAG debug, Serial Wire debug, and Serial Wire Viewer.
PTH and SMD connector options available.&lt;/p&gt;
&lt;p&gt; &lt;ul&gt;&lt;a href=”http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf”&gt;General Connector Information&lt;/a&gt;
&lt;p&gt;&lt;b&gt; Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”http://www.digikey.com/product-detail/en/cnc-tech/3220-10-0100-00/1175-1627-ND/3883661”&gt;PTH Connector&lt;/a&gt; -via Digi-Key&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13229”&gt;SparkFun PSoc&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13810”&gt;SparkFun T&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CORTEX_DEBUG" x="0" y="0"/>
</gates>
<devices>
<device name="_SMD" package="SAMTECH_FTSH-105-01">
<connects>
<connect gate="G$1" pin="!RESET" pad="10"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GNDDTCT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38289/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_PTH" package="2X5-PTH-1.27MM">
<connects>
<connect gate="G$1" pin="!RESET" pad="10"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GNDDTCT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38290/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="TACT-SWITCH-KMR6">
<smd name="P$1" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$2" x="2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$3" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$4" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0.2" x2="-2.1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="-0.2" x2="2.1" y2="0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.5" x2="1" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.032" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.5" x2="2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.5" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
</package>
<package name="TACT-SWITCH-SIDE">
<smd name="P$1" x="-1.8" y="0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$2" x="1.8" y="0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$3" x="-1.8" y="-0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$4" x="1.8" y="-0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<wire x1="-0.9" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0.9" y2="0.8" width="0.127" layer="51"/>
<wire x1="-0.9" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0.9" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.45" x2="1.75" y2="-1.45" width="0.127" layer="21"/>
<wire x1="-1.75" y1="1.6" x2="-1" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="0" y2="1.6" width="0.127" layer="21"/>
<wire x1="0" y1="1.6" x2="1" y2="1.6" width="0.127" layer="21"/>
<wire x1="1" y1="1.6" x2="1.75" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="-1" y2="2.3" width="0.127" layer="21"/>
<wire x1="-1" y1="2.3" x2="1" y2="2.3" width="0.127" layer="21"/>
<wire x1="1" y1="2.3" x2="1" y2="1.6" width="0.127" layer="21"/>
</package>
<package name="0805">
<smd name="1" x="-1" y="0" dx="0.8" dy="1.3" layer="1"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.3" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.6" x2="1" y2="0.6" layer="51"/>
<rectangle x1="-0.4" y1="-0.5" x2="0.4" y2="0.5" layer="21"/>
</package>
<package name="0603-CAP">
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.8" dy="0.95" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.8" dy="0.95" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
<rectangle x1="-0.2" y1="-0.3" x2="0.2" y2="0.3" layer="21"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="51"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="2220-C">
<smd name="P$1" x="-2.6" y="0" dx="1.2" dy="5" layer="1"/>
<smd name="P$2" x="2.6" y="0" dx="1.2" dy="5" layer="1"/>
<text x="-1.5" y="3" size="0.6096" layer="125">&gt;NAME</text>
<text x="-1.5" y="-3.5" size="0.6096" layer="127">&gt;VALUE</text>
</package>
<package name="0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<smd name="2" x="0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="0.6" dy="0.9" layer="1"/>
<smd name="2" x="0.85" y="0" dx="0.6" dy="0.9" layer="1"/>
<text x="0" y="1" size="0.8128" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-1" size="0.8128" layer="27" font="vector" ratio="10" align="center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="2-8X4-5_SWITCH" prefix="S">
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TACT-SWITCH-KMR6">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="TACT-SWITCH-SIDE">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2220" package="2220-C">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="comm">
<packages>
<package name="8-MSOP">
<circle x="-2" y="1.75" radius="0.1" width="0.2" layer="21"/>
<circle x="-2" y="1.75" radius="0.1" width="0.2" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<text x="-2.5" y="-2" size="0.8128" layer="27" font="vector" align="top-left">&gt;VALUE</text>
<text x="-2.5" y="2" size="0.8128" layer="25" font="vector">&gt;NAME</text>
<smd name="1" x="-2.2" y="0.975" dx="1.4" dy="0.4" layer="1" roundness="25"/>
<smd name="2" x="-2.2" y="0.325" dx="1.4" dy="0.4" layer="1" roundness="25"/>
<smd name="3" x="-2.2" y="-0.325" dx="1.4" dy="0.4" layer="1" roundness="25"/>
<smd name="4" x="-2.2" y="-0.975" dx="1.4" dy="0.4" layer="1" roundness="25"/>
<smd name="5" x="2.2" y="-0.975" dx="1.4" dy="0.4" layer="1" roundness="25"/>
<smd name="6" x="2.2" y="-0.325" dx="1.4" dy="0.4" layer="1" roundness="25"/>
<smd name="7" x="2.2" y="0.325" dx="1.4" dy="0.4" layer="1" roundness="25"/>
<smd name="8" x="2.2" y="0.975" dx="1.4" dy="0.4" layer="1" roundness="25"/>
</package>
<package name="8-MSOP-FAB">
<circle x="-2" y="1.75" radius="0.1" width="0.2" layer="21"/>
<circle x="-2" y="1.75" radius="0.1" width="0.2" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<text x="-2.5" y="-2" size="0.8128" layer="27" font="vector" align="top-left">&gt;VALUE</text>
<text x="-2.5" y="2" size="0.8128" layer="25" font="vector">&gt;NAME</text>
<smd name="1" x="-2.2" y="0.975" dx="1.4" dy="0.22" layer="1" roundness="25"/>
<smd name="2" x="-2.2" y="0.325" dx="1.4" dy="0.22" layer="1" roundness="25"/>
<smd name="3" x="-2.2" y="-0.325" dx="1.4" dy="0.22" layer="1" roundness="25"/>
<smd name="4" x="-2.2" y="-0.975" dx="1.4" dy="0.22" layer="1" roundness="25"/>
<smd name="5" x="2.2" y="-0.975" dx="1.4" dy="0.22" layer="1" roundness="25"/>
<smd name="6" x="2.2" y="-0.325" dx="1.4" dy="0.22" layer="1" roundness="25"/>
<smd name="7" x="2.2" y="0.325" dx="1.4" dy="0.22" layer="1" roundness="25"/>
<smd name="8" x="2.2" y="0.975" dx="1.4" dy="0.22" layer="1" roundness="25"/>
</package>
</packages>
<symbols>
<symbol name="RS485-ISL83078E">
<pin name="!RE" x="-15.24" y="5.08" length="middle"/>
<pin name="RO" x="-15.24" y="7.62" length="middle"/>
<pin name="DE" x="-15.24" y="-7.62" length="middle"/>
<pin name="DI" x="-15.24" y="-5.08" length="middle"/>
<pin name="GND" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="VCC" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="B/Z" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="A/Y" x="15.24" y="5.08" length="middle" rot="R180"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<text x="-10.16" y="12.7" size="1.27" layer="95" align="top-left">&gt;NAME</text>
<text x="-10.16" y="-12.7" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RS485-ISL83078E" prefix="U">
<gates>
<gate name="G$1" symbol="RS485-ISL83078E" x="0" y="0"/>
</gates>
<devices>
<device name="MSOP" package="8-MSOP">
<connects>
<connect gate="G$1" pin="!RE" pad="2"/>
<connect gate="G$1" pin="A/Y" pad="6"/>
<connect gate="G$1" pin="B/Z" pad="7"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="RO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MSOP-FAB" package="8-MSOP-FAB">
<connects>
<connect gate="G$1" pin="!RE" pad="2"/>
<connect gate="G$1" pin="A/Y" pad="6"/>
<connect gate="G$1" pin="B/Z" pad="7"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="RO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="DX4R005HJ5_100">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
</package>
<package name="DX4R005HJ5">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="21"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="21"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.475" y="-1.1" dx="2.75" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.475" y="-1.1" dx="2.75" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="D-" x="-0.65" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="ID" x="0.65" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="GND" x="1.3" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="25" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="DX4R005HJ5_64">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.21" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.21" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.21" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.21" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.21" dy="1.35" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="USB_MICRO_609-4613-1-ND">
<smd name="HD0" x="-3.8" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD4" x="-3.1" y="2.55" dx="2.1" dy="1.6" layer="1"/>
<smd name="HD5" x="3.1" y="2.55" dx="2.1" dy="1.6" layer="1"/>
<smd name="D+" x="0" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<text x="4.9275" y="1.2125" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-4.3925" y="1.13" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<smd name="HD1" x="-1.05" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD2" x="1.05" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD3" x="3.8" y="0" dx="1.9" dy="1.8" layer="1"/>
<wire x1="-4.7" y1="-1.45" x2="4.7" y2="-1.45" width="0.127" layer="51"/>
<text x="0" y="-1.3" size="0.8128" layer="51" font="vector" align="bottom-center">\\ PCB Edge /</text>
<wire x1="-3.9" y1="3" x2="-3.9" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-3.9" y1="-2.5" x2="3.9" y2="-2.5" width="0.127" layer="51"/>
<wire x1="3.9" y1="-2.5" x2="3.9" y2="3" width="0.127" layer="51"/>
<wire x1="3.9" y1="3" x2="-3.9" y2="3" width="0.127" layer="51"/>
<wire x1="-3.9" y1="1.1" x2="-3.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.9" y1="1.1" x2="3.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.8" y1="3" x2="1.7" y2="3" width="0.127" layer="21"/>
<wire x1="-1.7" y1="3" x2="-1.8" y2="3" width="0.127" layer="21"/>
<wire x1="4.4" y1="3" x2="4.7" y2="3" width="0.127" layer="21"/>
<wire x1="-4.4" y1="3" x2="-4.7" y2="3" width="0.127" layer="21"/>
<wire x1="-3.9" y1="3.6" x2="-3.9" y2="3.8" width="0.127" layer="21"/>
<wire x1="3.9" y1="3.6" x2="3.9" y2="3.8" width="0.127" layer="21"/>
</package>
<package name="MOLEX_1051330021">
<smd name="D+" x="0" y="0" dx="0.4" dy="1.5" layer="1"/>
<smd name="D-" x="-0.65" y="0" dx="0.4" dy="1.5" layer="1"/>
<smd name="VBUS" x="-1.3" y="0" dx="0.4" dy="1.5" layer="1"/>
<smd name="ID" x="0.65" y="0" dx="0.4" dy="1.5" layer="1"/>
<smd name="GND" x="1.3" y="0" dx="0.4" dy="1.5" layer="1"/>
<text x="-4.9" y="-0.5" size="0.6096" layer="25" font="vector" rot="R90" align="center">&gt;Name</text>
<text x="4.8" y="-0.5" size="0.6096" layer="25" font="vector" rot="R90" align="center">&gt;Value</text>
<pad name="P$1" x="-2.8" y="0.25" drill="0.85" shape="square"/>
<pad name="P$2" x="2.8" y="0.25" drill="0.85" shape="square"/>
<pad name="P$3" x="0" y="-1.9" drill="1.25" shape="square"/>
<wire x1="-4.3" y1="1.4" x2="-4.3" y2="-2.4" width="0.1524" layer="51"/>
<wire x1="-4.3" y1="-2.4" x2="4.3" y2="-2.4" width="0.1524" layer="51"/>
<wire x1="4.3" y1="-2.4" x2="4.3" y2="1.4" width="0.1524" layer="51"/>
<wire x1="4.3" y1="1.4" x2="-4.3" y2="1.4" width="0.1524" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="USB-1">
<wire x1="6.35" y1="-2.54" x2="6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.54" y="-1.27" size="2.54" layer="94">USB</text>
<text x="-4.445" y="-1.905" size="1.27" layer="95" font="vector" rot="R90">&gt;Name</text>
<text x="8.255" y="-1.905" size="1.27" layer="96" font="vector" rot="R90">&gt;Value</text>
<pin name="D+" x="5.08" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="D-" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="VBUS" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="GND" x="-2.54" y="5.08" visible="pad" length="short" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB" prefix="X">
<description>SMD micro USB connector as found in the fablab inventory. 
Three footprint variants included: 
&lt;ol&gt;
&lt;li&gt;609-4613-1-ND used by Jake
&lt;li&gt; original, as described by manufacturer's datasheet
&lt;li&gt; for milling with the 1/100" bit
&lt;li&gt; for milling with the 1/64" bit
&lt;/ol&gt;
&lt;p&gt;Made by Zaerc.</description>
<gates>
<gate name="G$1" symbol="USB-1" x="0" y="0"/>
</gates>
<devices>
<device name="_1/100" package="DX4R005HJ5_100">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ORIG" package="DX4R005HJ5">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1/64" package="DX4R005HJ5_64">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="USB_MICRO_609-4613-1-ND">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VERT" package="MOLEX_1051330021">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;, 5 lead</description>
<wire x1="-1.544" y1="0.713" x2="1.544" y2="0.713" width="0.1524" layer="51"/>
<wire x1="1.544" y1="0.713" x2="1.544" y2="-0.712" width="0.1524" layer="51"/>
<wire x1="1.544" y1="-0.712" x2="-1.544" y2="-0.712" width="0.1524" layer="51"/>
<wire x1="-1.544" y1="-0.712" x2="-1.544" y2="0.713" width="0.1524" layer="51"/>
<smd name="5" x="-0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="4" x="0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="1" x="-0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="2" x="0" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="3" x="0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<text x="-1.778" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.048" y="-1.778" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.1875" y1="0.7126" x2="-0.7125" y2="1.5439" layer="51"/>
<rectangle x1="0.7125" y1="0.7126" x2="1.1875" y2="1.5439" layer="51"/>
<rectangle x1="-1.1875" y1="-1.5437" x2="-0.7125" y2="-0.7124" layer="51"/>
<rectangle x1="-0.2375" y1="-1.5437" x2="0.2375" y2="-0.7124" layer="51"/>
<rectangle x1="0.7125" y1="-1.5437" x2="1.1875" y2="-0.7124" layer="51"/>
<wire x1="-1.5" y1="-1.9" x2="-1.5" y2="-1.2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="VREG-AP2112">
<pin name="VIN" x="-12.7" y="2.54" length="middle"/>
<pin name="EN" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="0" y="-10.16" length="middle" rot="R90"/>
<pin name="VOUT" x="12.7" y="2.54" length="middle" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="VREG-AP2112" prefix="U">
<gates>
<gate name="G$1" symbol="VREG-AP2112" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="2X5">
<description>&lt;h3&gt;Plated Through Hole - 2x5&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.1524" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="9.525" y2="3.81" width="0.1524" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.795" y2="3.81" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.81" x2="11.43" y2="3.175" width="0.1524" layer="21"/>
<wire x1="11.43" y1="3.175" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<text x="-1.27" y="3.937" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="22"/>
</package>
<package name="2X5-RA">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Right Angle Male Headers&lt;/h3&gt;
tDocu shows pin location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="2.8" y1="6.3" x2="5.3" y2="6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="6.3" x2="5.3" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="-6.3" x2="2.8" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-6.3" x2="2.8" y2="6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="0" x2="11.3" y2="0" width="0.127" layer="51"/>
<wire x1="5.3" y1="-2.54" x2="11.3" y2="-2.54" width="0.127" layer="51"/>
<wire x1="5.3" y1="-5.08" x2="11.3" y2="-5.08" width="0.127" layer="51"/>
<wire x1="5.3" y1="2.54" x2="11.3" y2="2.54" width="0.127" layer="51"/>
<wire x1="5.3" y1="5.08" x2="11.3" y2="5.08" width="0.127" layer="51"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-6.9" width="0.127" layer="51"/>
<wire x1="13.8" y1="6.3" x2="13.8" y2="-6.3" width="0.127" layer="51"/>
<wire x1="5.3" y1="6.3" x2="13.8" y2="6.3" width="0.127" layer="51"/>
<wire x1="5.3" y1="-6.3" x2="13.8" y2="-6.3" width="0.127" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="22"/>
<text x="2.54" y="6.477" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.54" y="-7.112" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-RAF">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Right Angle Female Header&lt;/h3&gt;
Silk outline of pins
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="2.7" y1="6.3" x2="11.2" y2="6.3" width="0.2032" layer="21"/>
<wire x1="11.2" y1="6.3" x2="11.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="11.2" y1="-6.3" x2="2.7" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-6.3" x2="2.7" y2="6.3" width="0.2032" layer="21"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-6.9" width="0.127" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="3.175" y="6.477" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.175" y="-7.112" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-SHROUDED">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.318" y="10.414" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-SHROUDED_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="-2.2" x2="-4.627" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.627" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="-4.191" y="10.541" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
<wire x1="-4.445" y1="10.16" x2="-4.445" y2="8.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="10.16" x2="-3.175" y2="10.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="10.16" x2="4.445" y2="10.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="10.16" x2="4.445" y2="8.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-8.89" x2="-4.445" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-10.16" x2="-3.175" y2="-10.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="-10.16" x2="4.445" y2="-10.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="-10.16" x2="4.445" y2="-8.89" width="0.127" layer="21"/>
</package>
<package name="2X5-SHROUDED_SMD">
<description>&lt;h3&gt;Surface Mount - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.315" y1="5.715" x2="-5.315" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<smd name="1" x="-2.794" y="5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="2" x="2.794" y="5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="3" x="-2.794" y="2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="4" x="2.794" y="2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="5" x="-2.794" y="0" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="6" x="2.794" y="0" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="7" x="-2.794" y="-2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="8" x="2.794" y="-2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="9" x="-2.794" y="-5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="10" x="2.794" y="-5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-5.353" y1="5.715" x2="-5.353" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5_NOSILK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="51"/>
<text x="-0.889" y="3.81" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.762" y="-2.159" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5_PTH_SILK_.05">
<description>&lt;h3&gt;Plated Through Hole - 2x5&lt;/h3&gt;
Holes are 0.05". 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="0.4318" rot="R90"/>
<pad name="2" x="0" y="1.27" drill="0.4318" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="0.4318" rot="R90"/>
<pad name="4" x="1.27" y="1.27" drill="0.4318" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="0.4318" rot="R90"/>
<pad name="6" x="2.54" y="1.27" drill="0.4318" rot="R90"/>
<pad name="7" x="3.81" y="0" drill="0.4318" rot="R90"/>
<pad name="8" x="3.81" y="1.27" drill="0.4318" rot="R90"/>
<pad name="9" x="5.08" y="0" drill="0.4318" rot="R90"/>
<pad name="10" x="5.08" y="1.27" drill="0.4318" rot="R90"/>
<wire x1="-0.635" y1="0.635" x2="-0.762" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="1.778" width="0.127" layer="21"/>
<wire x1="-0.762" y1="1.778" x2="-0.508" y2="2.032" width="0.127" layer="21"/>
<wire x1="-0.508" y1="2.032" x2="0.508" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.508" y1="2.032" x2="0.635" y2="1.905" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.905" x2="0.762" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.762" y1="2.032" x2="1.778" y2="2.032" width="0.127" layer="21"/>
<wire x1="1.778" y1="2.032" x2="1.905" y2="1.905" width="0.127" layer="21"/>
<wire x1="1.905" y1="1.905" x2="2.032" y2="2.032" width="0.127" layer="21"/>
<wire x1="2.032" y1="2.032" x2="3.048" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.048" y1="2.032" x2="3.175" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.905" x2="3.302" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.302" y1="2.032" x2="4.318" y2="2.032" width="0.127" layer="21"/>
<wire x1="4.318" y1="2.032" x2="4.445" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.905" x2="4.572" y2="2.032" width="0.127" layer="21"/>
<wire x1="4.572" y1="2.032" x2="5.588" y2="2.032" width="0.127" layer="21"/>
<wire x1="5.588" y1="2.032" x2="5.842" y2="1.778" width="0.127" layer="21"/>
<wire x1="5.842" y1="1.778" x2="5.842" y2="0.762" width="0.127" layer="21"/>
<wire x1="5.842" y1="0.762" x2="5.715" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.715" y1="0.635" x2="5.842" y2="0.508" width="0.127" layer="21"/>
<wire x1="5.842" y1="0.508" x2="5.842" y2="-0.508" width="0.127" layer="21"/>
<wire x1="5.842" y1="-0.508" x2="5.588" y2="-0.762" width="0.127" layer="21"/>
<wire x1="5.588" y1="-0.762" x2="4.572" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.572" y1="-0.762" x2="4.445" y2="-0.635" width="0.127" layer="21"/>
<wire x1="4.445" y1="-0.635" x2="4.318" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.318" y1="-0.762" x2="3.302" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.302" y1="-0.762" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.048" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.048" y1="-0.762" x2="2.032" y2="-0.762" width="0.127" layer="21"/>
<wire x1="2.032" y1="-0.762" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="-0.762" x2="0.762" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.508" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.762" x2="-0.508" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="-0.762" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.508" x2="-0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="22"/>
<text x="-0.762" y="2.286" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.762" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-SHROUDED-NS">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header No Silk&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-SHROUDED_LOCK_LATCH">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="16.1" x2="4.5" y2="-16.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-16.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="-2.2" x2="-4.627" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="16.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="16.1" x2="4.4" y2="16.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-16.1" x2="-4.5" y2="-16.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.627" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="-4.191" y="10.541" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
<wire x1="-4.445" y1="16.16" x2="-4.445" y2="14.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="16.16" x2="-3.175" y2="16.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="16.16" x2="4.445" y2="16.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="16.16" x2="4.445" y2="14.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-14.89" x2="-4.445" y2="-16.16" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-16.16" x2="-3.175" y2="-16.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="-16.16" x2="4.445" y2="-16.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="-16.16" x2="4.445" y2="-14.89" width="0.127" layer="21"/>
</package>
<package name="2X5-SHROUDED_SMD_LONGPADS">
<description>&lt;h3&gt;Surface Mount - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.315" y1="5.715" x2="-5.315" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<smd name="1" x="-3.294" y="5.08" dx="5.15" dy="1" layer="1" roundness="50"/>
<smd name="2" x="3.294" y="5.08" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="3" x="-3.294" y="2.54" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="4" x="3.294" y="2.54" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="5" x="-3.294" y="0" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="6" x="3.294" y="0" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="7" x="-3.294" y="-2.54" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="8" x="3.294" y="-2.54" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="9" x="-3.294" y="-5.08" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="10" x="3.294" y="-5.08" dx="5" dy="1" layer="1" roundness="50"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-5.353" y1="5.715" x2="-5.353" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-NOSILK_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<pad name="1" x="-1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="0" y="7.62" size="0.6096" layer="25" font="vector" ratio="20" align="center">&gt;NAME</text>
<text x="0" y="-7.62" size="0.6096" layer="27" font="vector" ratio="20" align="center">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="CONN_05X2">
<description>&lt;h3&gt;10 Pin Connection&lt;/h3&gt;
5x2 pin layout</description>
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-9.906" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-3.81" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="10" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_05X2" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;h3&gt;For AVR SPI programming port, see special device with nice symbol: "AVR_SPI_PROG_5x2.dev"&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;You can populate with any combo of single row headers, but if you'd like an exact match, check these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/778"&gt; 2x5 AVR ICSP Male Header&lt;/a&gt; (PRT-00778)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8506"&gt; 2x5 Pin Shrouded Header&lt;/a&gt; (PRT-08506)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special note: the shrouded connector mates well with our 5x2 ribbon cables:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8535"&gt; 2x5 Pin IDC Ribbon Cable&lt;/a&gt; (PRT-08535)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_05X2" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="2X5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08499" constant="no"/>
<attribute name="SF_ID" value="PRT-0778" constant="no"/>
</technology>
</technologies>
</device>
<device name="RA" package="2X5-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RAF" package="2X5-RAF">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHD" package="2X5-SHROUDED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_LOCK" package="2X5-SHROUDED_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_SMD" package="2X5-SHROUDED_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09508" constant="no"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="2X5_NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.05_IN_PTH_SILK" package="2X5_PTH_SILK_.05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHD-NS" package="2X5-SHROUDED-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_LOCK_LATCH" package="2X5-SHROUDED_LOCK_LATCH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_LONGPADS" package="2X5-SHROUDED_SMD_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_SILK_LOCK" package="2X5-NOSILK_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lights">
<packages>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<wire x1="2.5" y1="0.8" x2="2.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0.5" x2="-0.3" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.3" y1="-0.5" x2="0.3" y2="0" width="0.127" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.5" width="0.127" layer="21"/>
</package>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED0805">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.0778" y1="0.2818" x2="0.1278" y2="0" width="0.127" layer="21"/>
<wire x1="0.1278" y1="0" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
<wire x1="-0.0778" y1="0.2818" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
</package>
<package name="0402-D">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<smd name="2" x="0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1" y1="-0.2" x2="-1" y2="0.2" width="0.127" layer="21"/>
</package>
<package name="LED0603-RIGHTANGLE">
<smd name="1" x="-1" y="0" dx="1.1" dy="1.1" layer="1"/>
<smd name="2" x="1" y="0" dx="1.1" dy="1.1" layer="1"/>
<text x="-0.089" y="1.197" size="0.8128" layer="25" align="center">&gt;NAME</text>
<text x="-0.016" y="-1.313" size="0.8128" layer="27" ratio="10" align="center">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.3818" x2="0.2278" y2="0" width="0.127" layer="21"/>
<wire x1="0.2278" y1="0" x2="-0.1778" y2="-0.3818" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.3818" x2="-0.1778" y2="-0.3818" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.2" x2="1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="1" y1="-0.2" x2="0.4" y2="-0.8" width="0.127" layer="51" curve="-90"/>
<wire x1="0.4" y1="-0.8" x2="-0.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.4" y1="-0.8" x2="-1" y2="-0.2" width="0.127" layer="51" curve="-90"/>
</package>
<package name="LED-5630">
<smd name="P$2" x="2.6" y="0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$3" x="2.6" y="-0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$4" x="-2.6" y="0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$5" x="-2.6" y="-0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<rectangle x1="-2.5" y1="-1.5" x2="2.5" y2="1.5" layer="51"/>
<wire x1="0" y1="1.4" x2="0" y2="1.9" width="0.1016" layer="21"/>
<wire x1="0" y1="-1.4" x2="0" y2="-1.9" width="0.1016" layer="21"/>
<polygon width="0.1524" layer="1" pour="solid">
<vertex x="-2.21640625" y="-1"/>
<vertex x="-2.1884" y="-1.0116"/>
<vertex x="-2" y="-1.2"/>
<vertex x="-1.4" y="-1.2"/>
<vertex x="-1.4" y="1.2"/>
<vertex x="-2" y="1.2"/>
<vertex x="-2.1884" y="1.0116"/>
<vertex x="-2.21640625" y="1"/>
<vertex x="-2.4" y="1"/>
<vertex x="-2.4" y="-1"/>
</polygon>
<polygon width="0.1524" layer="1" pour="solid">
<vertex x="2.4" y="-1"/>
<vertex x="2.4" y="1"/>
<vertex x="2.21640625" y="1"/>
<vertex x="2.1884" y="1.0116"/>
<vertex x="2" y="1.2"/>
<vertex x="-0.8" y="1.2"/>
<vertex x="-0.8" y="-1.2"/>
<vertex x="2" y="-1.2"/>
<vertex x="2.1884" y="-1.0116"/>
<vertex x="2.21640625" y="-1"/>
</polygon>
<polygon width="0.1524" layer="29" pour="solid">
<vertex x="3" y="0.1"/>
<vertex x="3" y="1.1"/>
<vertex x="2.21640625" y="1.1"/>
<vertex x="2.1884" y="1.1116"/>
<vertex x="2" y="1.3"/>
<vertex x="-0.9" y="1.3"/>
<vertex x="-0.9" y="-1.3"/>
<vertex x="2" y="-1.3"/>
<vertex x="2.1884" y="-1.1116"/>
<vertex x="2.21640625" y="-1.1"/>
<vertex x="3" y="-1.1"/>
<vertex x="3" y="-0.1"/>
<vertex x="2.46104375" y="-0.1"/>
<vertex x="2.4330375" y="-0.0884"/>
<vertex x="2.4116" y="-0.0669625"/>
<vertex x="2.4" y="-0.03895625"/>
<vertex x="2.4" y="0.03895625"/>
<vertex x="2.4116" y="0.0669625"/>
<vertex x="2.4330375" y="0.0884"/>
<vertex x="2.46104375" y="0.1"/>
</polygon>
<polygon width="0.1524" layer="29" pour="solid">
<vertex x="-2.21640625" y="-1.1"/>
<vertex x="-2.1884" y="-1.1116"/>
<vertex x="-2" y="-1.3"/>
<vertex x="-1.3" y="-1.3"/>
<vertex x="-1.3" y="1.3"/>
<vertex x="-2" y="1.3"/>
<vertex x="-2.1884" y="1.1116"/>
<vertex x="-2.21640625" y="1.1"/>
<vertex x="-3" y="1.1"/>
<vertex x="-3" y="0.1"/>
<vertex x="-2.56104375" y="0.1"/>
<vertex x="-2.5330375" y="0.0884"/>
<vertex x="-2.5116" y="0.0669625"/>
<vertex x="-2.5" y="0.03895625"/>
<vertex x="-2.5" y="-0.03895625"/>
<vertex x="-2.5116" y="-0.0669625"/>
<vertex x="-2.5330375" y="-0.0884"/>
<vertex x="-2.56104375" y="-0.1"/>
<vertex x="-3" y="-0.1"/>
<vertex x="-3" y="-1.1"/>
</polygon>
<polygon width="0.1524" layer="31" pour="solid">
<vertex x="-2" y="-1.1"/>
<vertex x="-1.5" y="-1.1"/>
<vertex x="-1.5" y="1.1"/>
<vertex x="-2" y="1.1"/>
</polygon>
<polygon width="0.1524" layer="31" pour="solid">
<vertex x="-0.7" y="-1.1"/>
<vertex x="2" y="-1.1"/>
<vertex x="2" y="1.1"/>
<vertex x="-0.7" y="1.1"/>
</polygon>
<wire x1="2.6" y1="-1.2" x2="2.8" y2="-1.4" width="0.1016" layer="21"/>
<wire x1="2.8" y1="-1.4" x2="2.6" y2="-1.6" width="0.1016" layer="21"/>
<wire x1="2.6" y1="-1.6" x2="2.6" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="2.9" y1="-1.2" x2="2.9" y2="-1.6" width="0.1016" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94" pour="solid">
<vertex x="-3.048" y="1.27"/>
<vertex x="-3.429" y="0.381"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94" pour="solid">
<vertex x="-2.921" y="0.127"/>
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5630" package="LED-5630">
<connects>
<connect gate="G$1" pin="A" pad="P$4 P$5"/>
<connect gate="G$1" pin="C" pad="P$2 P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402-D">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603RA" package="LED0603-RIGHTANGLE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="sensor">
<packages>
<package name="TSSOP14">
<description>&lt;b&gt;Thin Shrink Small Outline Plastic 14&lt;/b&gt;</description>
<wire x1="-2.5146" y1="-2.0828" x2="2.5146" y2="-2.0828" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="2.0828" x2="2.5146" y2="-2.0828" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="2.0828" x2="-2.5146" y2="2.0828" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="-2.0828" x2="-2.5146" y2="2.0828" width="0.1524" layer="51"/>
<circle x="-3.0956" y="-1.6192" radius="0.3048" width="0.1524" layer="21"/>
<smd name="1" x="-1.905" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-1.27" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-0.635" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="0" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="0.635" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="1.27" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="1.905" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="-1.905" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="-1.27" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="-0.635" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="0" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="0.635" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="1.27" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="1.905" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-2.8956" y="-2.0828" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.8862" y="-2.0828" size="1.016" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="1.8034" y1="2.1082" x2="2.0066" y2="2.9464" layer="51"/>
<rectangle x1="1.1684" y1="2.1082" x2="1.3716" y2="2.9464" layer="51"/>
<rectangle x1="0.5334" y1="2.1082" x2="0.7366" y2="2.9464" layer="51"/>
<rectangle x1="-0.1016" y1="2.1082" x2="0.1016" y2="2.9464" layer="51"/>
<rectangle x1="-0.7366" y1="2.1082" x2="-0.5334" y2="2.9464" layer="51"/>
<rectangle x1="-1.3716" y1="2.1082" x2="-1.1684" y2="2.9464" layer="51"/>
<rectangle x1="-2.0066" y1="2.1082" x2="-1.8034" y2="2.9464" layer="51"/>
<rectangle x1="-2.0066" y1="-2.921" x2="-1.8034" y2="-2.0828" layer="51"/>
<rectangle x1="-1.3716" y1="-2.921" x2="-1.1684" y2="-2.0828" layer="51"/>
<rectangle x1="-0.7366" y1="-2.921" x2="-0.5334" y2="-2.0828" layer="51"/>
<rectangle x1="-0.1016" y1="-2.921" x2="0.1016" y2="-2.0828" layer="51"/>
<rectangle x1="0.5334" y1="-2.921" x2="0.7366" y2="-2.0828" layer="51"/>
<rectangle x1="1.1684" y1="-2.921" x2="1.3716" y2="-2.0828" layer="51"/>
<rectangle x1="1.8034" y1="-2.921" x2="2.0066" y2="-2.0828" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="AS5047">
<pin name="MOSI" x="-15.24" y="0" length="middle"/>
<pin name="MISO" x="-15.24" y="2.54" length="middle"/>
<pin name="CLK" x="-15.24" y="5.08" length="middle"/>
<pin name="CSN" x="-15.24" y="7.62" length="middle"/>
<pin name="TEST" x="-15.24" y="-2.54" length="middle"/>
<pin name="A" x="-15.24" y="-5.08" length="middle"/>
<pin name="B" x="-15.24" y="-7.62" length="middle"/>
<pin name="I" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="VDD3V" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="VDD" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="U" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="V" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="W" x="15.24" y="-7.62" length="middle" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-12.7" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AS5047" prefix="U">
<gates>
<gate name="G$1" symbol="AS5047" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP14">
<connects>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="CLK" pad="2"/>
<connect gate="G$1" pin="CSN" pad="1"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="I" pad="14"/>
<connect gate="G$1" pin="MISO" pad="3"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="TEST" pad="5"/>
<connect gate="G$1" pin="U" pad="10"/>
<connect gate="G$1" pin="V" pad="9"/>
<connect gate="G$1" pin="VDD" pad="11"/>
<connect gate="G$1" pin="VDD3V" pad="12"/>
<connect gate="G$1" pin="W" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="sensor" urn="urn:adsk.wipprod:fs.file:vf.erO8XLirRvWBRSYvGu7cAQ">
<packages>
<package name="TO-92-AMMO" library_version="1">
<description>&lt;h3&gt;TO-92 3-Pin PTH AMMO package&lt;/h3&gt;
&lt;p&gt;&lt;a href=""&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;
&lt;li&gt;Pin Count: 3&lt;/li&gt;
&lt;li&gt;Dimensions:  3.68 x 4.83 x 4.83 mm&lt;/li&gt;
&lt;li&gt;Pitch: 2.54 mm&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Devices Using:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;DS18B20&lt;/li&gt;
&lt;/ul&gt;</description>
<pad name="3" x="1.27" y="0" drill="0.55"/>
<pad name="2" x="0" y="0" drill="0.55"/>
<pad name="1" x="-1.27" y="0" drill="0.55" shape="square"/>
<text x="0" y="-1.278" size="0.6096" layer="25" font="vector" ratio="20" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0" y="1.294" size="0.6096" layer="27" font="vector" ratio="20" rot="R180" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.05" y1="-1" x2="2.05" y2="0.5" layer="51"/>
<wire x1="0" y1="0" x2="0" y2="-15" width="0.127" layer="51"/>
</package>
<package name="SOT23" library_version="1">
<description>&lt;b&gt;SOT 23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1524" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.8636" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="0.8636" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="0.762" dy="1.016" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="0.762" dy="1.016" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="0.762" dy="1.016" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="HALL" library_version="1">
<pin name="VCC" x="-7.62" y="0" length="middle" rot="R90"/>
<pin name="GND" x="0" y="0" length="middle" rot="R90"/>
<pin name="OUT" x="7.62" y="0" length="middle" rot="R90"/>
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<text x="-10.16" y="17.78" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="17.78" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HALL" prefix="U" library_version="1">
<gates>
<gate name="G$1" symbol="HALL" x="0" y="-5.08"/>
</gates>
<devices>
<device name="" package="TO-92-AMMO">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="OUT" pad="3"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23" package="SOT23">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="microcontrollers" deviceset="ATSAMD21E18A-AF" device="FAB"/>
<part name="J1" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="CORTEX_DEBUG" device="_PTH" package3d_urn="urn:adsk.eagle:package:38290/1"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="S1" library="passives" deviceset="2-8X4-5_SWITCH" device=""/>
<part name="C1" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C2" library="passives" deviceset="CAP" device="0805" value="1uF"/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R1" library="passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="U2" library="comm" deviceset="RS485-ISL83078E" device="MSOP"/>
<part name="U3" library="comm" deviceset="RS485-ISL83078E" device="MSOP"/>
<part name="X1" library="connector" deviceset="USB" device=""/>
<part name="+3V5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="U4" library="power" deviceset="VREG-AP2112" device=""/>
<part name="C3" library="passives" deviceset="CAP" device="0805" value="10uF"/>
<part name="C4" library="passives" deviceset="CAP" device="0805" value="10uF"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="+3V6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J2" library="SparkFun-Connectors" deviceset="CONN_05X2" device="SHD_LOCK_LATCH"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D1" library="lights" deviceset="LED" device="0805"/>
<part name="R2" library="passives" deviceset="RESISTOR" device="0805-RES" value="470R"/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D2" library="lights" deviceset="LED" device="0805"/>
<part name="R3" library="passives" deviceset="RESISTOR" device="0805-RES" value="470R"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="S2" library="passives" deviceset="2-8X4-5_SWITCH" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R4" library="passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="+3V7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="U5" library="sensor" deviceset="AS5047" device=""/>
<part name="C5" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="U6" library="sensor" library_urn="urn:adsk.wipprod:fs.file:vf.erO8XLirRvWBRSYvGu7cAQ" deviceset="HALL" device="SOT23"/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C6" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="C7" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C8" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="177.8" y="76.2" size="1.778" layer="97">A0!</text>
</plain>
<instances>
<instance part="U1" gate="G$1" x="106.68" y="48.26" smashed="yes">
<attribute name="NAME" x="86.3462" y="83.8578" size="1.780409375" layer="95"/>
<attribute name="VALUE" x="86.342" y="12.646" size="1.78115" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="25.4" y="35.56" smashed="yes">
<attribute name="NAME" x="12.7" y="43.434" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="12.7" y="25.654" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="P+1" gate="1" x="25.4" y="109.22" smashed="yes" rot="R270">
<attribute name="VALUE" x="27.94" y="109.22" size="1.778" layer="96"/>
</instance>
<instance part="+3V1" gate="G$1" x="157.48" y="106.68" smashed="yes" rot="R270">
<attribute name="VALUE" x="160.02" y="106.68" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="17.78" y="116.84" smashed="yes" rot="R180">
<attribute name="VALUE" x="20.32" y="119.38" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND2" gate="1" x="2.54" y="20.32" smashed="yes">
<attribute name="VALUE" x="0" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="73.66" y="7.62" smashed="yes">
<attribute name="VALUE" x="71.12" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="+3V2" gate="G$1" x="2.54" y="50.8" smashed="yes">
<attribute name="VALUE" x="2.54" y="53.34" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="S1" gate="G$1" x="63.5" y="22.86" smashed="yes">
<attribute name="NAME" x="57.15" y="20.32" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="58.42" y="17.78" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="60.96" y="73.66" smashed="yes" rot="R270">
<attribute name="NAME" x="63.881" y="72.136" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="58.801" y="72.136" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="56.896" y="72.136" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="55.118" y="72.136" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="53.34" y="72.136" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="C2" gate="G$1" x="60.96" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="63.881" y="64.516" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="58.801" y="64.516" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="56.896" y="64.516" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="55.118" y="64.516" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="53.34" y="64.516" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="+3V3" gate="G$1" x="73.66" y="88.9" smashed="yes">
<attribute name="VALUE" x="73.66" y="91.44" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND4" gate="1" x="48.26" y="66.04" smashed="yes" rot="R270">
<attribute name="VALUE" x="45.72" y="68.58" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V4" gate="G$1" x="60.96" y="55.88" smashed="yes">
<attribute name="VALUE" x="63.5" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="60.96" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="59.4614" y="41.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="64.262" y="41.91" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="67.818" y="41.91" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="66.04" y="41.91" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="U2" gate="G$1" x="132.08" y="106.68" smashed="yes">
<attribute name="NAME" x="121.92" y="119.38" size="1.27" layer="95" align="top-left"/>
<attribute name="VALUE" x="121.92" y="93.98" size="1.27" layer="95"/>
</instance>
<instance part="U3" gate="G$1" x="132.08" y="139.7" smashed="yes">
<attribute name="NAME" x="121.92" y="152.4" size="1.27" layer="95" align="top-left"/>
<attribute name="VALUE" x="121.92" y="127" size="1.27" layer="95"/>
</instance>
<instance part="X1" gate="G$1" x="5.08" y="109.22" smashed="yes" rot="R270">
<attribute name="NAME" x="3.175" y="113.665" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="3.175" y="100.965" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="+3V5" gate="G$1" x="157.48" y="139.7" smashed="yes" rot="R270">
<attribute name="VALUE" x="160.02" y="139.7" size="1.778" layer="96"/>
</instance>
<instance part="U4" gate="G$1" x="17.78" y="78.74" smashed="yes">
<attribute name="NAME" x="15.24" y="86.36" size="1.27" layer="95"/>
<attribute name="VALUE" x="20.32" y="71.12" size="1.27" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="2.54" y="68.58" smashed="yes">
<attribute name="NAME" x="4.064" y="71.501" size="1.778" layer="95"/>
<attribute name="VALUE" x="4.064" y="66.421" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="4.064" y="64.516" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="4.064" y="62.738" size="1.27" layer="97"/>
<attribute name="TYPE" x="4.064" y="60.96" size="1.27" layer="97"/>
</instance>
<instance part="C4" gate="G$1" x="33.02" y="68.58" smashed="yes">
<attribute name="NAME" x="34.544" y="71.501" size="1.778" layer="95"/>
<attribute name="VALUE" x="34.544" y="66.421" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="34.544" y="64.516" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="34.544" y="62.738" size="1.27" layer="97"/>
<attribute name="TYPE" x="34.544" y="60.96" size="1.27" layer="97"/>
</instance>
<instance part="P+2" gate="1" x="2.54" y="88.9" smashed="yes">
<attribute name="VALUE" x="2.54" y="91.44" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V6" gate="G$1" x="33.02" y="88.9" smashed="yes">
<attribute name="VALUE" x="33.02" y="91.44" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="17.78" y="58.42" smashed="yes">
<attribute name="VALUE" x="15.24" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="J2" gate="G$1" x="27.94" y="132.08" smashed="yes">
<attribute name="VALUE" x="24.13" y="122.174" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="24.13" y="140.208" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND6" gate="1" x="7.62" y="132.08" smashed="yes" rot="R270">
<attribute name="VALUE" x="5.08" y="134.62" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND7" gate="1" x="48.26" y="132.08" smashed="yes" rot="R90">
<attribute name="VALUE" x="50.8" y="129.54" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+3" gate="1" x="7.62" y="127" smashed="yes" rot="R90">
<attribute name="VALUE" x="5.08" y="127" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+4" gate="1" x="48.26" y="137.16" smashed="yes" rot="R270">
<attribute name="VALUE" x="50.8" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="152.4" y="127" smashed="yes">
<attribute name="VALUE" x="149.86" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="152.4" y="93.98" smashed="yes">
<attribute name="VALUE" x="149.86" y="91.44" size="1.778" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="185.42" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="187.452" y="36.576" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="187.452" y="38.735" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R2" gate="G$1" x="195.58" y="33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="199.39" y="31.5214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="199.39" y="36.322" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="199.39" y="39.878" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="199.39" y="38.1" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="GND10" gate="1" x="205.74" y="33.02" smashed="yes" rot="R90">
<attribute name="VALUE" x="208.28" y="30.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D2" gate="G$1" x="124.46" y="160.02" smashed="yes" rot="R90">
<attribute name="NAME" x="126.492" y="163.576" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="126.492" y="165.735" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R3" gate="G$1" x="134.62" y="160.02" smashed="yes" rot="R180">
<attribute name="NAME" x="138.43" y="158.5214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="138.43" y="163.322" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="138.43" y="166.878" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="138.43" y="165.1" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="GND11" gate="1" x="157.48" y="160.02" smashed="yes" rot="R90">
<attribute name="VALUE" x="160.02" y="157.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="S2" gate="G$1" x="124.46" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="121.92" y="176.53" size="1.778" layer="95"/>
<attribute name="VALUE" x="129.54" y="175.26" size="1.778" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="157.48" y="167.64" smashed="yes" rot="R90">
<attribute name="VALUE" x="160.02" y="165.1" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R4" gate="G$1" x="71.12" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="69.6214" y="41.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="74.422" y="41.91" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="77.978" y="41.91" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="76.2" y="41.91" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="+3V7" gate="G$1" x="71.12" y="55.88" smashed="yes">
<attribute name="VALUE" x="73.66" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="U5" gate="G$1" x="243.84" y="58.42" smashed="yes">
<attribute name="NAME" x="241.3" y="71.12" size="1.27" layer="95"/>
<attribute name="VALUE" x="241.3" y="45.72" size="1.27" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="60.96" y="81.28" smashed="yes" rot="R270">
<attribute name="NAME" x="63.881" y="79.756" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="58.801" y="79.756" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="56.896" y="79.756" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="55.118" y="79.756" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="53.34" y="79.756" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="U6" gate="G$1" x="246.38" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="228.6" y="17.78" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="228.6" y="30.48" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND13" gate="1" x="261.62" y="27.94" smashed="yes" rot="R90">
<attribute name="VALUE" x="264.16" y="25.4" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V8" gate="G$1" x="261.62" y="20.32" smashed="yes" rot="R270">
<attribute name="VALUE" x="264.16" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="248.92" y="22.86" smashed="yes">
<attribute name="NAME" x="250.444" y="25.781" size="1.778" layer="95"/>
<attribute name="VALUE" x="250.444" y="20.701" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="250.444" y="18.796" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="250.444" y="17.018" size="1.27" layer="97"/>
<attribute name="TYPE" x="250.444" y="15.24" size="1.27" layer="97"/>
</instance>
<instance part="C7" gate="G$1" x="269.24" y="58.42" smashed="yes">
<attribute name="NAME" x="270.764" y="61.341" size="1.778" layer="95"/>
<attribute name="VALUE" x="270.764" y="56.261" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="270.764" y="54.356" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="270.764" y="52.578" size="1.27" layer="97"/>
<attribute name="TYPE" x="270.764" y="50.8" size="1.27" layer="97"/>
</instance>
<instance part="GND14" gate="1" x="279.4" y="63.5" smashed="yes" rot="R90">
<attribute name="VALUE" x="281.94" y="60.96" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V9" gate="G$1" x="279.4" y="55.88" smashed="yes" rot="R270">
<attribute name="VALUE" x="281.94" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="152.4" y="134.62" smashed="yes">
<attribute name="NAME" x="153.924" y="137.541" size="1.778" layer="95"/>
<attribute name="VALUE" x="153.924" y="132.461" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="153.924" y="130.556" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="153.924" y="128.778" size="1.27" layer="97"/>
<attribute name="TYPE" x="153.924" y="127" size="1.27" layer="97"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="2.54" y1="48.26" x2="2.54" y2="40.64" width="0.1524" layer="91"/>
<wire x1="2.54" y1="40.64" x2="10.16" y2="40.64" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="66.04" y1="73.66" x2="73.66" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDDANA"/>
<pinref part="U1" gate="G$1" pin="VDDIN"/>
<wire x1="73.66" y1="73.66" x2="81.28" y2="73.66" width="0.1524" layer="91"/>
<wire x1="81.28" y1="81.28" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
<wire x1="73.66" y1="81.28" x2="73.66" y2="73.66" width="0.1524" layer="91"/>
<junction x="73.66" y="73.66"/>
<wire x1="73.66" y1="81.28" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<junction x="73.66" y="81.28"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="66.04" y1="81.28" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="60.96" y1="50.8" x2="60.96" y2="53.34" width="0.1524" layer="91"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VCC"/>
<wire x1="147.32" y1="139.7" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="152.4" y1="139.7" x2="154.94" y2="139.7" width="0.1524" layer="91"/>
<junction x="152.4" y="139.7"/>
</segment>
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="154.94" y1="106.68" x2="147.32" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="33.02" y1="86.36" x2="33.02" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="33.02" y1="73.66" x2="33.02" y2="81.28" width="0.1524" layer="91"/>
<wire x1="33.02" y1="81.28" x2="30.48" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VOUT"/>
<junction x="33.02" y="81.28"/>
</segment>
<segment>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<wire x1="71.12" y1="53.34" x2="71.12" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="259.08" y1="20.32" x2="248.92" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VCC"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="248.92" y1="20.32" x2="246.38" y2="20.32" width="0.1524" layer="91"/>
<junction x="248.92" y="20.32"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VDD3V"/>
<wire x1="259.08" y1="60.96" x2="264.16" y2="60.96" width="0.1524" layer="91"/>
<wire x1="264.16" y1="60.96" x2="264.16" y2="58.42" width="0.1524" layer="91"/>
<wire x1="264.16" y1="58.42" x2="264.16" y2="55.88" width="0.1524" layer="91"/>
<wire x1="264.16" y1="55.88" x2="269.24" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="269.24" y1="55.88" x2="276.86" y2="55.88" width="0.1524" layer="91"/>
<junction x="269.24" y="55.88"/>
<pinref part="U5" gate="G$1" pin="VDD"/>
<wire x1="259.08" y1="58.42" x2="264.16" y2="58.42" width="0.1524" layer="91"/>
<junction x="264.16" y="58.42"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="GND@3"/>
<wire x1="10.16" y1="38.1" x2="2.54" y2="38.1" width="0.1524" layer="91"/>
<wire x1="2.54" y1="38.1" x2="2.54" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="J1" gate="G$1" pin="GND@5"/>
<wire x1="2.54" y1="35.56" x2="2.54" y2="30.48" width="0.1524" layer="91"/>
<wire x1="2.54" y1="30.48" x2="2.54" y2="22.86" width="0.1524" layer="91"/>
<wire x1="10.16" y1="35.56" x2="2.54" y2="35.56" width="0.1524" layer="91"/>
<junction x="2.54" y="35.56"/>
<pinref part="J1" gate="G$1" pin="GNDDTCT"/>
<wire x1="10.16" y1="30.48" x2="2.54" y2="30.48" width="0.1524" layer="91"/>
<junction x="2.54" y="30.48"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="P1"/>
<wire x1="66.04" y1="17.78" x2="73.66" y2="17.78" width="0.1524" layer="91"/>
<wire x1="73.66" y1="17.78" x2="73.66" y2="10.16" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="73.66" y1="17.78" x2="81.28" y2="17.78" width="0.1524" layer="91"/>
<junction x="73.66" y="17.78"/>
<pinref part="U1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="50.8" y1="66.04" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="53.34" y1="66.04" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<wire x1="58.42" y1="73.66" x2="53.34" y2="73.66" width="0.1524" layer="91"/>
<wire x1="53.34" y1="73.66" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<junction x="53.34" y="66.04"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="58.42" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<wire x1="53.34" y1="81.28" x2="53.34" y2="73.66" width="0.1524" layer="91"/>
<junction x="53.34" y="73.66"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="GND"/>
<wire x1="10.16" y1="111.76" x2="17.78" y2="111.76" width="0.1524" layer="91"/>
<wire x1="17.78" y1="111.76" x2="17.78" y2="114.3" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="17.78" y1="60.96" x2="17.78" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="2.54" y1="66.04" x2="17.78" y2="66.04" width="0.1524" layer="91"/>
<wire x1="17.78" y1="66.04" x2="17.78" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="17.78" y1="66.04" x2="33.02" y2="66.04" width="0.1524" layer="91"/>
<junction x="17.78" y="66.04"/>
<pinref part="C4" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="5"/>
<wire x1="20.32" y1="132.08" x2="10.16" y2="132.08" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="6"/>
<wire x1="35.56" y1="132.08" x2="45.72" y2="132.08" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<wire x1="147.32" y1="132.08" x2="152.4" y2="132.08" width="0.1524" layer="91"/>
<wire x1="152.4" y1="132.08" x2="152.4" y2="129.54" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="C8" gate="G$1" pin="2"/>
<junction x="152.4" y="132.08"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="147.32" y1="99.06" x2="152.4" y2="99.06" width="0.1524" layer="91"/>
<wire x1="152.4" y1="99.06" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="200.66" y1="33.02" x2="203.2" y2="33.02" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="139.7" y1="160.02" x2="154.94" y2="160.02" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="154.94" y1="167.64" x2="129.54" y2="167.64" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="S1"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="259.08" y1="27.94" x2="248.92" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="GND"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="248.92" y1="27.94" x2="246.38" y2="27.94" width="0.1524" layer="91"/>
<junction x="248.92" y="27.94"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GND"/>
<wire x1="259.08" y1="63.5" x2="269.24" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="269.24" y1="63.5" x2="276.86" y2="63.5" width="0.1524" layer="91"/>
<junction x="269.24" y="63.5"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="SWDIO/TMS"/>
<wire x1="43.18" y1="40.64" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<label x="45.72" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA31/TCC1-1/SER1-3/SWDIO"/>
<wire x1="160.02" y1="17.78" x2="175.26" y2="17.78" width="0.1524" layer="91"/>
<label x="162.56" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWDCLK" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="SWDCLK/TCK"/>
<wire x1="43.18" y1="38.1" x2="60.96" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="60.96" y1="40.64" x2="60.96" y2="38.1" width="0.1524" layer="91"/>
<label x="45.72" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA30/TCC1-0/SER1-2/SWDCLK"/>
<wire x1="160.02" y1="20.32" x2="175.26" y2="20.32" width="0.1524" layer="91"/>
<label x="162.56" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="S1"/>
<wire x1="66.04" y1="27.94" x2="66.04" y2="30.48" width="0.1524" layer="91"/>
<wire x1="66.04" y1="30.48" x2="71.12" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="!RESET"/>
<wire x1="71.12" y1="30.48" x2="81.28" y2="30.48" width="0.1524" layer="91"/>
<wire x1="66.04" y1="30.48" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<junction x="66.04" y="30.48"/>
<pinref part="J1" gate="G$1" pin="!RESET"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="71.12" y1="40.64" x2="71.12" y2="30.48" width="0.1524" layer="91"/>
<junction x="71.12" y="30.48"/>
<label x="45.72" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDDCORE"/>
<wire x1="81.28" y1="66.04" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="VBUS"/>
<wire x1="10.16" y1="109.22" x2="22.86" y2="109.22" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="2.54" y1="86.36" x2="2.54" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="EN"/>
<wire x1="5.08" y1="76.2" x2="2.54" y2="76.2" width="0.1524" layer="91"/>
<wire x1="2.54" y1="76.2" x2="2.54" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="U4" gate="G$1" pin="VIN"/>
<wire x1="5.08" y1="81.28" x2="2.54" y2="81.28" width="0.1524" layer="91"/>
<wire x1="2.54" y1="81.28" x2="2.54" y2="76.2" width="0.1524" layer="91"/>
<junction x="2.54" y="81.28"/>
<junction x="2.54" y="76.2"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="9"/>
<wire x1="20.32" y1="127" x2="10.16" y2="127" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="35.56" y1="137.16" x2="45.72" y2="137.16" width="0.1524" layer="91"/>
<pinref part="P+4" gate="1" pin="+5V"/>
</segment>
</net>
<net name="USBDM" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="D-"/>
<wire x1="10.16" y1="106.68" x2="20.32" y2="106.68" width="0.1524" layer="91"/>
<label x="12.7" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA24/TC5-0/TCC1-2/SER3-2/USB-DM"/>
<wire x1="160.02" y1="30.48" x2="175.26" y2="30.48" width="0.1524" layer="91"/>
<label x="162.56" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="USBDP" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA25/TC5-1/TCC1-3/SER3-3/USB-DP"/>
<wire x1="160.02" y1="27.94" x2="175.26" y2="27.94" width="0.1524" layer="91"/>
<label x="162.56" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="D+"/>
<wire x1="10.16" y1="104.14" x2="20.32" y2="104.14" width="0.1524" layer="91"/>
<label x="12.7" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="Y" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="7"/>
<wire x1="20.32" y1="129.54" x2="12.7" y2="129.54" width="0.1524" layer="91"/>
<label x="15.24" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="A/Y"/>
<wire x1="147.32" y1="111.76" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
<label x="149.86" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="20.32" y1="134.62" x2="12.7" y2="134.62" width="0.1524" layer="91"/>
<label x="15.24" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="B/Z"/>
<wire x1="147.32" y1="147.32" x2="154.94" y2="147.32" width="0.1524" layer="91"/>
<label x="149.86" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="35.56" y1="134.62" x2="43.18" y2="134.62" width="0.1524" layer="91"/>
<label x="38.1" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="A/Y"/>
<wire x1="147.32" y1="144.78" x2="154.94" y2="144.78" width="0.1524" layer="91"/>
<label x="149.86" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="Z" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="8"/>
<wire x1="35.56" y1="129.54" x2="43.18" y2="129.54" width="0.1524" layer="91"/>
<label x="38.1" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="B/Z"/>
<wire x1="147.32" y1="114.3" x2="154.94" y2="114.3" width="0.1524" layer="91"/>
<label x="149.86" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="UCBUS_RE" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="!RE"/>
<wire x1="116.84" y1="144.78" x2="101.6" y2="144.78" width="0.1524" layer="91"/>
<label x="101.6" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA18/TC3-0/TCC0-2/SER1-2/SER3-2"/>
<wire x1="160.02" y1="40.64" x2="175.26" y2="40.64" width="0.1524" layer="91"/>
<label x="162.56" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="UCBUS_RX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RO"/>
<wire x1="116.84" y1="147.32" x2="101.6" y2="147.32" width="0.1524" layer="91"/>
<label x="101.6" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA19/TC3-1/TCC0-3/SER1-3/SER3-3"/>
<wire x1="160.02" y1="38.1" x2="175.26" y2="38.1" width="0.1524" layer="91"/>
<label x="162.56" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="UCBUS_TX" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="DI"/>
<wire x1="116.84" y1="101.6" x2="101.6" y2="101.6" width="0.1524" layer="91"/>
<label x="101.6" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA16/TCC2-0/TCC0-6/SER1-0/SER3-0"/>
<wire x1="160.02" y1="45.72" x2="175.26" y2="45.72" width="0.1524" layer="91"/>
<label x="162.56" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="UCBUS_DE" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="DE"/>
<wire x1="116.84" y1="99.06" x2="101.6" y2="99.06" width="0.1524" layer="91"/>
<label x="101.6" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA17/TCC2-1/TCC0-7/SER1-1/SER3-1"/>
<wire x1="160.02" y1="43.18" x2="175.26" y2="43.18" width="0.1524" layer="91"/>
<label x="162.56" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="UCBUS_L" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA22/TC4-0/TCC0-4/SER3-0"/>
<wire x1="160.02" y1="35.56" x2="175.26" y2="35.56" width="0.1524" layer="91"/>
<label x="162.56" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="119.38" y1="160.02" x2="101.6" y2="160.02" width="0.1524" layer="91"/>
<label x="101.6" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="LIGHT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="(ADA_D13)PA23/TC4-1/TCC0-5/SER3-1/USB-SOF"/>
<wire x1="160.02" y1="33.02" x2="180.34" y2="33.02" width="0.1524" layer="91"/>
<label x="162.56" y="33.02" size="1.778" layer="95"/>
<pinref part="D1" gate="G$1" pin="A"/>
</segment>
</net>
<net name="UCBUS_B" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA28"/>
<wire x1="160.02" y1="22.86" x2="175.26" y2="22.86" width="0.1524" layer="91"/>
<label x="162.56" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="S2" gate="G$1" pin="P1"/>
<wire x1="119.38" y1="167.64" x2="101.6" y2="167.64" width="0.1524" layer="91"/>
<label x="101.6" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="187.96" y1="33.02" x2="190.5" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="D1" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="127" y1="160.02" x2="129.54" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="HALL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="(ADA_D1A1)PA02/AIN-0/DAC-0"/>
<wire x1="160.02" y1="76.2" x2="175.26" y2="76.2" width="0.1524" layer="91"/>
<label x="162.56" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="OUT"/>
<wire x1="246.38" y1="35.56" x2="259.08" y2="35.56" width="0.1524" layer="91"/>
<label x="248.92" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="0-0-MOSI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA08/AIN16/TCC0-0/TCC1-2/SER0-0/SER2-0"/>
<wire x1="160.02" y1="60.96" x2="175.26" y2="60.96" width="0.1524" layer="91"/>
<label x="162.56" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="MOSI"/>
<wire x1="228.6" y1="58.42" x2="215.9" y2="58.42" width="0.1524" layer="91"/>
<label x="215.9" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="0-1-CLK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA09/AIN17/TCC0-1/TCC1-3/SER0-1/SER2-1"/>
<wire x1="160.02" y1="58.42" x2="175.26" y2="58.42" width="0.1524" layer="91"/>
<label x="162.56" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="CLK"/>
<wire x1="228.6" y1="63.5" x2="215.9" y2="63.5" width="0.1524" layer="91"/>
<label x="215.9" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="0-2-CS" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA10/AIN18/TCC0-2/TCC1-0/SER0-2/SER2-2"/>
<wire x1="160.02" y1="55.88" x2="175.26" y2="55.88" width="0.1524" layer="91"/>
<label x="162.56" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="CSN"/>
<wire x1="228.6" y1="66.04" x2="215.9" y2="66.04" width="0.1524" layer="91"/>
<label x="215.9" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="0-3-MISO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA11/AIN19/TCC0-3/TCC1-1/SER0-3/SER2-3"/>
<wire x1="160.02" y1="53.34" x2="175.26" y2="53.34" width="0.1524" layer="91"/>
<label x="162.56" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="MISO"/>
<wire x1="228.6" y1="60.96" x2="215.9" y2="60.96" width="0.1524" layer="91"/>
<label x="215.9" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
