<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="connector">
<packages>
<package name="PWRPAD_SC-02_2-45MM">
<pad name="P$1" x="0" y="0" drill="2.45" diameter="4.24" thermals="no"/>
</package>
<package name="PWRPAD_4MM">
<pad name="P$1" x="0" y="0" drill="3.9878" diameter="6.35" thermals="no"/>
</package>
<package name="PWRPAD_3-25MM">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.75" thermals="no"/>
</package>
<package name="PWRPAD_2-65MM">
<pad name="P$1" x="0" y="0" drill="2.65" diameter="4.65" thermals="no"/>
</package>
<package name="PWRPAD_2-05MM">
<pad name="P$1" x="0" y="0" drill="2.05" diameter="3.8" thermals="no"/>
</package>
<package name="PWRPAD_M3_STANDOFF">
<pad name="P$1" x="0" y="0" drill="4.4" diameter="7" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3.6"/>
<vertex x="0.6" y="3.6"/>
<vertex x="0.4" y="2.1"/>
<vertex x="-0.4" y="2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3.6"/>
<vertex x="-0.6" y="-3.6"/>
<vertex x="-0.4" y="-2.1"/>
<vertex x="0.4" y="-2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.6" y="-0.6"/>
<vertex x="-3.6" y="0.6"/>
<vertex x="-2.1" y="0.4"/>
<vertex x="-2.1" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3.6" y="0.6"/>
<vertex x="3.6" y="-0.6"/>
<vertex x="2.1" y="-0.4"/>
<vertex x="2.1" y="0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.95269375" y="2.13136875"/>
<vertex x="-2.104165625" y="2.979896875"/>
<vertex x="-1.19203125" y="1.784921875"/>
<vertex x="-1.75771875" y="1.2192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.99705625" y="-2.12131875"/>
<vertex x="2.148528125" y="-2.969846875"/>
<vertex x="1.23639375" y="-1.774871875"/>
<vertex x="1.80208125" y="-1.2091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.104165625" y="-2.969846875"/>
<vertex x="-2.95269375" y="-2.12131875"/>
<vertex x="-1.75771875" y="-1.2091875"/>
<vertex x="-1.19203125" y="-1.774871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.148528125" y="2.979896875"/>
<vertex x="2.99705625" y="2.13136875"/>
<vertex x="1.80208125" y="1.2192375"/>
<vertex x="1.23639375" y="1.784921875"/>
</polygon>
<circle x="0" y="0" radius="3.5" width="0.125" layer="51"/>
</package>
<package name="PWRPAD_3-25MM-SKINNY">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.35" thermals="no"/>
</package>
<package name="PWRPAD_M25_STANDOFF">
<pad name="P$1" x="0" y="0" drill="3.7" diameter="6" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3"/>
<vertex x="0.6" y="3"/>
<vertex x="0.4" y="1.9"/>
<vertex x="-0.4" y="1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3"/>
<vertex x="-0.6" y="-3"/>
<vertex x="-0.4" y="-1.9"/>
<vertex x="0.4" y="-1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3" y="-0.6"/>
<vertex x="-3" y="0.6"/>
<vertex x="-1.9" y="0.4"/>
<vertex x="-1.9" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3" y="0.5"/>
<vertex x="3" y="-0.7"/>
<vertex x="1.9" y="-0.5"/>
<vertex x="1.9" y="0.3"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.55269375" y="1.73136875"/>
<vertex x="-1.704165625" y="2.579896875"/>
<vertex x="-0.99203125" y="1.584921875"/>
<vertex x="-1.55771875" y="1.0192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.49705625" y="-1.72131875"/>
<vertex x="1.648528125" y="-2.569846875"/>
<vertex x="1.03639375" y="-1.574871875"/>
<vertex x="1.60208125" y="-1.0091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-1.704165625" y="-2.669846875"/>
<vertex x="-2.55269375" y="-1.82131875"/>
<vertex x="-1.55771875" y="-1.1091875"/>
<vertex x="-0.99203125" y="-1.674871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.748528125" y="2.579896875"/>
<vertex x="2.59705625" y="1.73136875"/>
<vertex x="1.60208125" y="1.0192375"/>
<vertex x="1.03639375" y="1.584921875"/>
</polygon>
</package>
<package name="SOLDER_ON">
<smd name="P$1" x="0" y="0" dx="4" dy="7" layer="1" thermals="no"/>
</package>
<package name="UCBUS_MODULE_EXT">
<smd name="P$1" x="3.175" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$2" x="4.445" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$3" x="5.715" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<wire x1="0" y1="0" x2="28.575" y2="0" width="0.127" layer="51"/>
<wire x1="28.575" y1="0" x2="28.575" y2="20.32" width="0.127" layer="51"/>
<wire x1="1.27" y1="20.32" x2="0" y2="19.05" width="0.127" layer="51"/>
<wire x1="0" y1="19.05" x2="0" y2="0" width="0.127" layer="51"/>
<smd name="P$4" x="6.985" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$5" x="8.255" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$6" x="9.525" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$7" x="10.795" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$8" x="12.065" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$9" x="13.335" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$10" x="15.875" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$11" x="17.145" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$12" x="18.415" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$13" x="19.685" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$14" x="20.955" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$15" x="22.225" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$16" x="23.495" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$17" x="24.765" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$18" x="26.035" y="-0.1524" dx="0.889" dy="1.524" layer="1" roundness="50"/>
<smd name="P$19" x="28.7274" y="1.27" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$20" x="28.7274" y="2.54" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$21" x="28.7274" y="3.81" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$22" x="28.7274" y="5.08" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$23" x="28.7274" y="6.35" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$24" x="28.7274" y="7.62" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$25" x="28.7274" y="8.89" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$26" x="28.7274" y="10.16" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$27" x="13.335" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$28" x="12.065" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$29" x="10.795" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$30" x="9.525" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$31" x="8.255" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$32" x="6.985" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$33" x="5.715" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$34" x="4.445" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<smd name="P$35" x="3.175" y="20.4724" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R180"/>
<wire x1="18.796" y1="20.32" x2="19.177" y2="19.939" width="0.127" layer="51"/>
<wire x1="19.177" y1="19.939" x2="26.924" y2="19.939" width="0.127" layer="51"/>
<wire x1="26.924" y1="19.939" x2="27.305" y2="20.32" width="0.127" layer="51"/>
<wire x1="27.305" y1="20.32" x2="28.575" y2="20.32" width="0.127" layer="51"/>
<wire x1="18.796" y1="20.32" x2="1.27" y2="20.32" width="0.127" layer="51"/>
<wire x1="0.127" y1="19.05" x2="1.27" y2="20.193" width="0.127" layer="21"/>
<wire x1="28.448" y1="20.193" x2="28.067" y2="20.193" width="0.127" layer="21"/>
<wire x1="28.448" y1="20.193" x2="28.448" y2="19.812" width="0.127" layer="21"/>
<wire x1="28.448" y1="0.127" x2="28.448" y2="0.508" width="0.127" layer="21"/>
<wire x1="28.448" y1="0.127" x2="28.067" y2="0.127" width="0.127" layer="21"/>
<wire x1="0.127" y1="0.127" x2="0.127" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.127" y1="0.127" x2="0.508" y2="0.127" width="0.127" layer="21"/>
<smd name="P$36" x="0.049784375" y="1.319784375" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$37" x="0.049784375" y="2.589784375" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$38" x="0.049784375" y="3.859784375" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
<smd name="P$39" x="0.049784375" y="5.129784375" dx="0.889" dy="1.524" layer="1" roundness="50" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="PWRPAD">
<pin name="PWRPAD" x="-5.08" y="0" length="middle"/>
</symbol>
<symbol name="UCBUS_MODULE_EXT">
<pin name="GND" x="-12.7" y="-35.56" length="middle"/>
<pin name="+3V3" x="-12.7" y="-27.94" length="middle"/>
<pin name="+5V" x="-12.7" y="-20.32" length="middle"/>
<pin name="PA02/DAC-0/ADC0-1" x="68.58" y="27.94" length="middle" rot="R180"/>
<pin name="PA03/VREFA/ADC0-1" x="68.58" y="25.4" length="middle" rot="R180"/>
<pin name="PA04/VREFB/ACD0-4/TC0-0/SER0-0" x="68.58" y="22.86" length="middle" rot="R180"/>
<pin name="PA05/DAC-1/ADC0-5/TC0-1/SER0-1" x="68.58" y="20.32" length="middle" rot="R180"/>
<pin name="PA06/VREFC/ADC0-6/TC1-0/SER0-2" x="68.58" y="17.78" length="middle" rot="R180"/>
<pin name="PA07/ADC0-7/TC1-1/SER0-3" x="68.58" y="15.24" length="middle" rot="R180"/>
<pin name="PA08/ADC0-8/ADC1-2/TC0-0/TCC0-0/SER0-0/SER2-1" x="68.58" y="12.7" length="middle" rot="R180"/>
<pin name="PA09/ADC0-9/ADC1-3/TC0-1/TCC0-1/SER0-1/SER2-0" x="68.58" y="10.16" length="middle" rot="R180"/>
<pin name="PA10/ADC0-10/TC1-0/TCC0-2/SER0-2/SER2-2" x="68.58" y="7.62" length="middle" rot="R180"/>
<pin name="PA11/ADC0-11/TC1-1/TCC0-3/SER0-3/SER2-3" x="68.58" y="5.08" length="middle" rot="R180"/>
<pin name="PA12/TC2-0/TCC0-6/SER2-0/SER4-1" x="68.58" y="2.54" length="middle" rot="R180"/>
<pin name="PA13/TC2-1/TCC0-7/SER2-1/SER4-0" x="68.58" y="0" length="middle" rot="R180"/>
<pin name="PA20/TC7-0/SER5-2/SER3-2" x="68.58" y="-5.08" length="middle" rot="R180"/>
<pin name="PA21/TC7-1/SER5-3/SER3-3" x="68.58" y="-7.62" length="middle" rot="R180"/>
<pin name="PA22/TC4-0/SER5-1/SER3-0" x="68.58" y="-10.16" length="middle" rot="R180"/>
<pin name="PA23/TC4-1/SER5-0/SER3-1" x="68.58" y="-12.7" length="middle" rot="R180"/>
<pin name="PB06/ADC1-8" x="68.58" y="-17.78" length="middle" rot="R180"/>
<pin name="PB07/AD1-9" x="68.58" y="-20.32" length="middle" rot="R180"/>
<pin name="PB10/TC5-0/TCC0-4/SER4-2" x="68.58" y="-22.86" length="middle" rot="R180"/>
<pin name="PB11/TC5-1/TCC0-5/SER4-3" x="68.58" y="-25.4" length="middle" rot="R180"/>
<pin name="PB12/TC4-0/SER4-0" x="68.58" y="-27.94" length="middle" rot="R180"/>
<pin name="PB13/TC4-1/SER4-1" x="68.58" y="-30.48" length="middle" rot="R180"/>
<pin name="PB14/TC5-0/SER4-2" x="68.58" y="-33.02" length="middle" rot="R180"/>
<pin name="PB15/TC5-1/SER4-3" x="68.58" y="-35.56" length="middle" rot="R180"/>
<pin name="A" x="-12.7" y="27.94" length="middle"/>
<pin name="B" x="-12.7" y="25.4" length="middle"/>
<pin name="Y" x="-12.7" y="20.32" length="middle"/>
<pin name="Z" x="-12.7" y="17.78" length="middle"/>
<pin name="GND_PA04-PA02" x="-12.7" y="-10.16" length="middle"/>
<pin name="GND_PB10-PA20" x="-12.7" y="-7.62" length="middle"/>
<pin name="GND_PA11-PB14" x="-12.7" y="-5.08" length="middle"/>
<wire x1="-7.62" y1="30.48" x2="-7.62" y2="-50.8" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="-50.8" x2="63.5" y2="-50.8" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-50.8" x2="63.5" y2="30.48" width="0.4064" layer="94"/>
<wire x1="63.5" y1="30.48" x2="-7.62" y2="30.48" width="0.4064" layer="94"/>
<pin name="PB23/XOUT1/SER1-3/SER5-3/TC7-1" x="68.58" y="-40.64" length="middle" rot="R180"/>
<pin name="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0" x="68.58" y="-43.18" length="middle" rot="R180"/>
<pin name="PB17/SER5-1/TC6-1" x="68.58" y="-45.72" length="middle" rot="R180"/>
<pin name="PB16/SER5-0/TC6-0" x="68.58" y="-48.26" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PWRPAD" prefix="J">
<gates>
<gate name="G$1" symbol="PWRPAD" x="0" y="0"/>
</gates>
<devices>
<device name="SC-02_2-45MM" package="PWRPAD_SC-02_2-45MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4MM" package="PWRPAD_4MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3" package="PWRPAD_3-25MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5" package="PWRPAD_2-65MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2" package="PWRPAD_2-05MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-STANDOFF" package="PWRPAD_M3_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-SKINNY" package="PWRPAD_3-25MM-SKINNY">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5_STANDOFF" package="PWRPAD_M25_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOLDER" package="SOLDER_ON">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UCBUS_MODULE_EXT" prefix="U">
<gates>
<gate name="G$1" symbol="UCBUS_MODULE_EXT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UCBUS_MODULE_EXT">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$25"/>
<connect gate="G$1" pin="+5V" pad="P$26"/>
<connect gate="G$1" pin="A" pad="P$19"/>
<connect gate="G$1" pin="B" pad="P$20"/>
<connect gate="G$1" pin="GND" pad="P$21 P$22"/>
<connect gate="G$1" pin="GND_PA04-PA02" pad="P$31"/>
<connect gate="G$1" pin="GND_PA11-PB14" pad="P$5"/>
<connect gate="G$1" pin="GND_PB10-PA20" pad="P$14"/>
<connect gate="G$1" pin="PA02/DAC-0/ADC0-1" pad="P$32"/>
<connect gate="G$1" pin="PA03/VREFA/ADC0-1" pad="P$33"/>
<connect gate="G$1" pin="PA04/VREFB/ACD0-4/TC0-0/SER0-0" pad="P$30"/>
<connect gate="G$1" pin="PA05/DAC-1/ADC0-5/TC0-1/SER0-1" pad="P$29"/>
<connect gate="G$1" pin="PA06/VREFC/ADC0-6/TC1-0/SER0-2" pad="P$28"/>
<connect gate="G$1" pin="PA07/ADC0-7/TC1-1/SER0-3" pad="P$27"/>
<connect gate="G$1" pin="PA08/ADC0-8/ADC1-2/TC0-0/TCC0-0/SER0-0/SER2-1" pad="P$1"/>
<connect gate="G$1" pin="PA09/ADC0-9/ADC1-3/TC0-1/TCC0-1/SER0-1/SER2-0" pad="P$2"/>
<connect gate="G$1" pin="PA10/ADC0-10/TC1-0/TCC0-2/SER0-2/SER2-2" pad="P$3"/>
<connect gate="G$1" pin="PA11/ADC0-11/TC1-1/TCC0-3/SER0-3/SER2-3" pad="P$4"/>
<connect gate="G$1" pin="PA12/TC2-0/TCC0-6/SER2-0/SER4-1" pad="P$8"/>
<connect gate="G$1" pin="PA13/TC2-1/TCC0-7/SER2-1/SER4-0" pad="P$9"/>
<connect gate="G$1" pin="PA20/TC7-0/SER5-2/SER3-2" pad="P$15"/>
<connect gate="G$1" pin="PA21/TC7-1/SER5-3/SER3-3" pad="P$16"/>
<connect gate="G$1" pin="PA22/TC4-0/SER5-1/SER3-0" pad="P$17"/>
<connect gate="G$1" pin="PA23/TC4-1/SER5-0/SER3-1" pad="P$18"/>
<connect gate="G$1" pin="PB06/ADC1-8" pad="P$34"/>
<connect gate="G$1" pin="PB07/AD1-9" pad="P$35"/>
<connect gate="G$1" pin="PB10/TC5-0/TCC0-4/SER4-2" pad="P$13"/>
<connect gate="G$1" pin="PB11/TC5-1/TCC0-5/SER4-3" pad="P$12"/>
<connect gate="G$1" pin="PB12/TC4-0/SER4-0" pad="P$11"/>
<connect gate="G$1" pin="PB13/TC4-1/SER4-1" pad="P$10"/>
<connect gate="G$1" pin="PB14/TC5-0/SER4-2" pad="P$6"/>
<connect gate="G$1" pin="PB15/TC5-1/SER4-3" pad="P$7"/>
<connect gate="G$1" pin="PB16/SER5-0/TC6-0" pad="P$36"/>
<connect gate="G$1" pin="PB17/SER5-1/TC6-1" pad="P$37"/>
<connect gate="G$1" pin="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0" pad="P$38"/>
<connect gate="G$1" pin="PB23/XOUT1/SER1-3/SER5-3/TC7-1" pad="P$39"/>
<connect gate="G$1" pin="Y" pad="P$23"/>
<connect gate="G$1" pin="Z" pad="P$24"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="2X5">
<description>&lt;h3&gt;Plated Through Hole - 2x5&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.1524" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="9.525" y2="3.81" width="0.1524" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.795" y2="3.81" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.81" x2="11.43" y2="3.175" width="0.1524" layer="21"/>
<wire x1="11.43" y1="3.175" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<text x="-1.27" y="3.937" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="22"/>
</package>
<package name="2X5-RA">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Right Angle Male Headers&lt;/h3&gt;
tDocu shows pin location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="2.8" y1="6.3" x2="5.3" y2="6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="6.3" x2="5.3" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="-6.3" x2="2.8" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-6.3" x2="2.8" y2="6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="0" x2="11.3" y2="0" width="0.127" layer="51"/>
<wire x1="5.3" y1="-2.54" x2="11.3" y2="-2.54" width="0.127" layer="51"/>
<wire x1="5.3" y1="-5.08" x2="11.3" y2="-5.08" width="0.127" layer="51"/>
<wire x1="5.3" y1="2.54" x2="11.3" y2="2.54" width="0.127" layer="51"/>
<wire x1="5.3" y1="5.08" x2="11.3" y2="5.08" width="0.127" layer="51"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-6.9" width="0.127" layer="51"/>
<wire x1="13.8" y1="6.3" x2="13.8" y2="-6.3" width="0.127" layer="51"/>
<wire x1="5.3" y1="6.3" x2="13.8" y2="6.3" width="0.127" layer="51"/>
<wire x1="5.3" y1="-6.3" x2="13.8" y2="-6.3" width="0.127" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="22"/>
<text x="2.54" y="6.477" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.54" y="-7.112" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-RAF">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Right Angle Female Header&lt;/h3&gt;
Silk outline of pins
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="2.7" y1="6.3" x2="11.2" y2="6.3" width="0.2032" layer="21"/>
<wire x1="11.2" y1="6.3" x2="11.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="11.2" y1="-6.3" x2="2.7" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-6.3" x2="2.7" y2="6.3" width="0.2032" layer="21"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-6.9" width="0.127" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="3.175" y="6.477" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.175" y="-7.112" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-SHROUDED">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.318" y="10.414" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-SHROUDED_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="-2.2" x2="-4.627" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.627" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="-4.191" y="10.541" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
<wire x1="-4.445" y1="10.16" x2="-4.445" y2="8.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="10.16" x2="-3.175" y2="10.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="10.16" x2="4.445" y2="10.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="10.16" x2="4.445" y2="8.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-8.89" x2="-4.445" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-10.16" x2="-3.175" y2="-10.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="-10.16" x2="4.445" y2="-10.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="-10.16" x2="4.445" y2="-8.89" width="0.127" layer="21"/>
</package>
<package name="2X5-SHROUDED_SMD">
<description>&lt;h3&gt;Surface Mount - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.315" y1="5.715" x2="-5.315" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<smd name="1" x="-2.794" y="5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="2" x="2.794" y="5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="3" x="-2.794" y="2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="4" x="2.794" y="2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="5" x="-2.794" y="0" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="6" x="2.794" y="0" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="7" x="-2.794" y="-2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="8" x="2.794" y="-2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="9" x="-2.794" y="-5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="10" x="2.794" y="-5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-5.353" y1="5.715" x2="-5.353" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5_NOSILK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="51"/>
<text x="-0.889" y="3.81" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.762" y="-2.159" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5_PTH_SILK_.05">
<description>&lt;h3&gt;Plated Through Hole - 2x5&lt;/h3&gt;
Holes are 0.05". 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="0.4318" rot="R90"/>
<pad name="2" x="0" y="1.27" drill="0.4318" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="0.4318" rot="R90"/>
<pad name="4" x="1.27" y="1.27" drill="0.4318" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="0.4318" rot="R90"/>
<pad name="6" x="2.54" y="1.27" drill="0.4318" rot="R90"/>
<pad name="7" x="3.81" y="0" drill="0.4318" rot="R90"/>
<pad name="8" x="3.81" y="1.27" drill="0.4318" rot="R90"/>
<pad name="9" x="5.08" y="0" drill="0.4318" rot="R90"/>
<pad name="10" x="5.08" y="1.27" drill="0.4318" rot="R90"/>
<wire x1="-0.635" y1="0.635" x2="-0.762" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="1.778" width="0.127" layer="21"/>
<wire x1="-0.762" y1="1.778" x2="-0.508" y2="2.032" width="0.127" layer="21"/>
<wire x1="-0.508" y1="2.032" x2="0.508" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.508" y1="2.032" x2="0.635" y2="1.905" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.905" x2="0.762" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.762" y1="2.032" x2="1.778" y2="2.032" width="0.127" layer="21"/>
<wire x1="1.778" y1="2.032" x2="1.905" y2="1.905" width="0.127" layer="21"/>
<wire x1="1.905" y1="1.905" x2="2.032" y2="2.032" width="0.127" layer="21"/>
<wire x1="2.032" y1="2.032" x2="3.048" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.048" y1="2.032" x2="3.175" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.905" x2="3.302" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.302" y1="2.032" x2="4.318" y2="2.032" width="0.127" layer="21"/>
<wire x1="4.318" y1="2.032" x2="4.445" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.905" x2="4.572" y2="2.032" width="0.127" layer="21"/>
<wire x1="4.572" y1="2.032" x2="5.588" y2="2.032" width="0.127" layer="21"/>
<wire x1="5.588" y1="2.032" x2="5.842" y2="1.778" width="0.127" layer="21"/>
<wire x1="5.842" y1="1.778" x2="5.842" y2="0.762" width="0.127" layer="21"/>
<wire x1="5.842" y1="0.762" x2="5.715" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.715" y1="0.635" x2="5.842" y2="0.508" width="0.127" layer="21"/>
<wire x1="5.842" y1="0.508" x2="5.842" y2="-0.508" width="0.127" layer="21"/>
<wire x1="5.842" y1="-0.508" x2="5.588" y2="-0.762" width="0.127" layer="21"/>
<wire x1="5.588" y1="-0.762" x2="4.572" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.572" y1="-0.762" x2="4.445" y2="-0.635" width="0.127" layer="21"/>
<wire x1="4.445" y1="-0.635" x2="4.318" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.318" y1="-0.762" x2="3.302" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.302" y1="-0.762" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.048" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.048" y1="-0.762" x2="2.032" y2="-0.762" width="0.127" layer="21"/>
<wire x1="2.032" y1="-0.762" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="-0.762" x2="0.762" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.508" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.762" x2="-0.508" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="-0.762" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.508" x2="-0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="22"/>
<text x="-0.762" y="2.286" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.762" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-SHROUDED-NS">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header No Silk&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-SHROUDED_LOCK_LATCH">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="16.1" x2="4.5" y2="-16.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-16.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="-2.2" x2="-4.627" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="16.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="16.1" x2="4.4" y2="16.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-16.1" x2="-4.5" y2="-16.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.627" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="-4.191" y="10.541" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
<wire x1="-4.445" y1="16.16" x2="-4.445" y2="14.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="16.16" x2="-3.175" y2="16.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="16.16" x2="4.445" y2="16.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="16.16" x2="4.445" y2="14.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-14.89" x2="-4.445" y2="-16.16" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-16.16" x2="-3.175" y2="-16.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="-16.16" x2="4.445" y2="-16.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="-16.16" x2="4.445" y2="-14.89" width="0.127" layer="21"/>
</package>
<package name="2X5-SHROUDED_SMD_LONGPADS">
<description>&lt;h3&gt;Surface Mount - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.315" y1="5.715" x2="-5.315" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<smd name="1" x="-3.294" y="5.08" dx="5.15" dy="1" layer="1" roundness="50"/>
<smd name="2" x="3.294" y="5.08" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="3" x="-3.294" y="2.54" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="4" x="3.294" y="2.54" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="5" x="-3.294" y="0" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="6" x="3.294" y="0" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="7" x="-3.294" y="-2.54" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="8" x="3.294" y="-2.54" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="9" x="-3.294" y="-5.08" dx="5" dy="1" layer="1" roundness="50"/>
<smd name="10" x="3.294" y="-5.08" dx="5" dy="1" layer="1" roundness="50"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-5.353" y1="5.715" x2="-5.353" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="1X02">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="3.15" x2="5.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD">
<description>&lt;h3&gt;JST-Right Angle Male Header SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;li&gt;JST_2MM_MALE&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.397" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="0.635" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_BIG">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.15"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD-VERT">
<description>&lt;h3&gt;JST-Vertical Male Header SMT &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="-3.81" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-3.81" y="2.21" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
</package>
<package name="SCREWTERMINAL-5MM-2">
<description>&lt;h3&gt;Screw Terminal  5mm Pitch -2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 5mm/197mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<description>&lt;h3&gt;Plated Through Hole - Locking Footprint&lt;/h3&gt;
Holes are staggered by 0.005" from center to hold pins while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2_LOCK">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole Locking Footprint&lt;/h3&gt;
Holes are offset from center by 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - Long Pads with Locking Footprint&lt;/h3&gt;
Pins are staggered by 0.005" from center to hold pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="51"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="51"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="51"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH Locking&lt;/h3&gt;
Holes are offset from center 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - Long Pads without Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.397" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="2.73" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.6" x2="-2.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.6" x2="2.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="1X02_XTRA_BIG">
<description>&lt;h3&gt;Plated Through Hole - 0.1" holes&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.2"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
<text x="-5.08" y="2.667" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-5.08" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_PP_HOLES_ONLY">
<description>&lt;h3&gt;Pogo Pins Connector - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole- No Silk&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; No silk outline of connector. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole - KIT&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad.
&lt;br&gt; This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<description>&lt;h3&gt;Spring Terminal- PCB Mount 2 Pin PTH&lt;/h3&gt;
tDocu marks the spring arms
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/SpringTerminal.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<description>&lt;h3&gt;2 Pin Screw Terminal - 2.54mm&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<text x="-1.27" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_POKEHOME">
<description>2 pin poke-home connector

part number 2062-2P from STA</description>
<wire x1="-7" y1="-4" x2="-7" y2="2" width="0.2032" layer="21"/>
<wire x1="-7" y1="2" x2="-7" y2="4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="4" x2="4.7" y2="-4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="-4" x2="-7" y2="-4" width="0.2032" layer="21"/>
<smd name="P2" x="5.25" y="-2" dx="3.5" dy="2" layer="1"/>
<smd name="P1" x="5.25" y="2" dx="3.5" dy="2" layer="1"/>
<smd name="P4" x="-4" y="-2" dx="6" dy="2" layer="1"/>
<smd name="P3" x="-4" y="2" dx="6" dy="2" layer="1"/>
<wire x1="-7" y1="4" x2="4.7" y2="4" width="0.2032" layer="21"/>
<text x="0.635" y="-3.175" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.635" y="-1.905" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-7.239" y="-1.397" size="0.3048" layer="51" rot="R90">BOARD EDGE</text>
</package>
<package name="1X02_RA_PTH_FEMALE">
<wire x1="-2.79" y1="4.25" x2="-2.79" y2="-4.25" width="0.1778" layer="21"/>
<text x="-1.397" y="0.762" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="2.79" y1="4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="4.25" x2="2.79" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="-4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<pad name="2" x="-1.27" y="-5.85" drill="0.8"/>
<pad name="1" x="1.27" y="-5.85" drill="0.8"/>
</package>
</packages>
<symbols>
<symbol name="CONN_05X2">
<description>&lt;h3&gt;10 Pin Connection&lt;/h3&gt;
5x2 pin layout</description>
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-9.906" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-3.81" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="10" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CONN_02">
<description>&lt;h3&gt;2 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_05X2" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;h3&gt;For AVR SPI programming port, see special device with nice symbol: "AVR_SPI_PROG_5x2.dev"&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;You can populate with any combo of single row headers, but if you'd like an exact match, check these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/778"&gt; 2x5 AVR ICSP Male Header&lt;/a&gt; (PRT-00778)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8506"&gt; 2x5 Pin Shrouded Header&lt;/a&gt; (PRT-08506)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special note: the shrouded connector mates well with our 5x2 ribbon cables:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8535"&gt; 2x5 Pin IDC Ribbon Cable&lt;/a&gt; (PRT-08535)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_05X2" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="2X5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08499" constant="no"/>
<attribute name="SF_ID" value="PRT-0778" constant="no"/>
</technology>
</technologies>
</device>
<device name="RA" package="2X5-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RAF" package="2X5-RAF">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHD" package="2X5-SHROUDED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_LOCK" package="2X5-SHROUDED_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_SMD" package="2X5-SHROUDED_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09508" constant="no"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="2X5_NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.05_IN_PTH_SILK" package="2X5_PTH_SILK_.05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHD-NS" package="2X5-SHROUDED-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_LOCK_LATCH" package="2X5-SHROUDED_LOCK_LATCH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_LONGPADS" package="2X5-SHROUDED_SMD_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_02" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08432</description>
<gates>
<gate name="G$1" symbol="CONN_02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_SKU" value="PRT-08432" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09863" constant="no"/>
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALL_POKEHOME" package="1X02_POKEHOME">
<connects>
<connect gate="G$1" pin="1" pad="P1 P3"/>
<connect gate="G$1" pin="2" pad="P2 P4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13512"/>
</technology>
</technologies>
</device>
<device name="PTH_RA_FEMALE" package="1X02_RA_PTH_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13700"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
<package name="0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<smd name="2" x="0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.8" dy="0.95" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.8" dy="0.95" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="51"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="2220-C">
<smd name="P$1" x="-2.6" y="0" dx="1.2" dy="5" layer="1"/>
<smd name="P$2" x="2.6" y="0" dx="1.2" dy="5" layer="1"/>
<text x="-1.5" y="3" size="0.6096" layer="125">&gt;NAME</text>
<text x="-1.5" y="-3.5" size="0.6096" layer="127">&gt;VALUE</text>
</package>
<package name="SOD123">
<description>&lt;b&gt;SMALL OUTLINE DIODE&lt;/b&gt;</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.321" y1="0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="1.321" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="-1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="1.321" y1="-0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1" y1="0" x2="0" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="-0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="CATHODE" x="-1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<smd name="ANODE" x="1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.9558" y1="-0.3048" x2="-1.3716" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="1.3716" y1="-0.3048" x2="1.9558" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
<wire x1="-2.667" y1="0.889" x2="-2.667" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="0.889" x2="-2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="-0.889" x2="2.794" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="-0.889" x2="2.794" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="0.889" x2="-2.921" y2="0.889" width="0.127" layer="21"/>
</package>
<package name="0805-DIODE">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="0.3048" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.3048" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.4318" x2="0.1778" y2="0" width="0.127" layer="21"/>
<wire x1="0.1778" y1="0" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.4318" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
</package>
<package name="SOD-123HE">
<smd name="P$1" x="0.8" y="0" dx="2.4" dy="1.4" layer="1"/>
<smd name="P$2" x="-1.55" y="0" dx="0.9" dy="1.4" layer="1"/>
<wire x1="-1.4" y1="-0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="-0.9" x2="1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.9" y2="0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="-1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.5" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.9" x2="0.5" y2="-0.9" width="0.127" layer="21"/>
<text x="0" y="1.8" size="1.27" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="SMA-403D">
<smd name="P$1" x="-2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<smd name="P$2" x="2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.2" x2="-2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="1.6" x2="-1.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.2" x2="-2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.6" x2="-1.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-1.6" x2="2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="1.6" x2="2" y2="1.6" width="0.127" layer="21"/>
<text x="0" y="2.4" size="1.27" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="SMD-ELEC-CAP">
<wire x1="-3.25" y1="-3.3" x2="-3.25" y2="2.3" width="0.127" layer="51"/>
<wire x1="-3.25" y1="2.3" x2="-2.25" y2="3.3" width="0.127" layer="51"/>
<wire x1="-2.25" y1="3.3" x2="2.25" y2="3.3" width="0.127" layer="51"/>
<wire x1="2.25" y1="3.3" x2="3.25" y2="2.3" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.3" x2="3.25" y2="-3.3" width="0.127" layer="51"/>
<wire x1="3.25" y1="-3.3" x2="-3.25" y2="-3.3" width="0.127" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.08" layer="51"/>
<smd name="A" x="0" y="2.65" dx="3.6" dy="1.8" layer="1" rot="R90" thermals="no"/>
<smd name="C" x="0" y="-2.65" dx="3.6" dy="1.8" layer="1" rot="R90" thermals="no"/>
<rectangle x1="-3.25" y1="-3.3" x2="3.25" y2="-2.3" layer="51"/>
<text x="-3.25" y="5.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.25" y="-6.3" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.2" y1="2.35" x2="-2.3" y2="3.25" width="0.127" layer="21"/>
<wire x1="3.2" y1="2.35" x2="2.3" y2="3.25" width="0.127" layer="21"/>
</package>
<package name="0805">
<smd name="1" x="-1" y="0" dx="0.8" dy="1.3" layer="1"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.3" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.6" x2="1" y2="0.6" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="CAP-ELECTROLYTIC">
<pin name="C" x="0" y="-2.54" visible="off" length="short" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<wire x1="-2.032" y1="1.778" x2="0" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="2.032" y2="1.778" width="0.254" layer="94"/>
<wire x1="-2.032" y1="0.762" x2="2.032" y2="0.762" width="0.254" layer="94" curve="-33.398492"/>
<wire x1="0" y1="1.016" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="1.778" y2="3.048" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.556" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2220" package="2220-C">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
high speed (Philips)</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="SOD123" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DIODE" package="0805-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD123HE" package="SOD-123HE">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMA403D" package="SMA-403D">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP-POL" prefix="C">
<gates>
<gate name="G$1" symbol="CAP-ELECTROLYTIC" x="0" y="0"/>
</gates>
<devices>
<device name="CAP_POL_SMD6.6X6.6" package="SMD-ELEC-CAP">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+24V" urn="urn:adsk.eagle:symbol:26935/1">
<wire x1="1.27" y1="-0.635" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V" urn="urn:adsk.eagle:component:26964/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="DSOP-ADVANCE">
<smd name="P$1" x="-1.905" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$2" x="-0.635" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$3" x="0.635" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$4" x="1.905" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$5" x="0" y="0.95" dx="5.5" dy="4.7" layer="1"/>
<circle x="-2.7" y="-3.8" radius="0.22360625" width="0.127" layer="21"/>
<wire x1="-2.5" y1="3" x2="2.5" y2="3" width="0.127" layer="48"/>
<wire x1="2.5" y1="3" x2="2.5" y2="-3" width="0.127" layer="48"/>
<wire x1="2.5" y1="-3" x2="-2.5" y2="-3" width="0.127" layer="48"/>
<wire x1="-2.5" y1="-3" x2="-2.5" y2="3" width="0.127" layer="48"/>
</package>
<package name="TRANS_AON7408">
<circle x="-2.1625" y="0.975" radius="0.1" width="0.2" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-0.535" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="0.535" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-0.535" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="0.535" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.9" y1="1.855" x2="1.9" y2="1.855" width="0.05" layer="39"/>
<wire x1="1.9" y1="1.855" x2="1.9" y2="-1.855" width="0.05" layer="39"/>
<wire x1="1.9" y1="-1.855" x2="-1.9" y2="-1.855" width="0.05" layer="39"/>
<wire x1="-1.9" y1="-1.855" x2="-1.9" y2="1.855" width="0.05" layer="39"/>
<text x="-2.5" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5" y="-2" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
<circle x="-2.1625" y="0.975" radius="0.1" width="0.2" layer="51"/>
<rectangle x1="-0.265" y1="-0.775" x2="0.985" y2="0.775" layer="31"/>
<polygon width="0.01" layer="1">
<vertex x="-0.63" y="1.225"/>
<vertex x="-0.215" y="1.225"/>
<vertex x="-0.215" y="1.605"/>
<vertex x="0.215" y="1.605"/>
<vertex x="0.215" y="1.225"/>
<vertex x="1.35" y="1.225"/>
<vertex x="1.35" y="1.175"/>
<vertex x="1.65" y="1.175"/>
<vertex x="1.65" y="0.775"/>
<vertex x="1.35" y="0.775"/>
<vertex x="1.35" y="0.525"/>
<vertex x="1.65" y="0.525"/>
<vertex x="1.65" y="0.125"/>
<vertex x="1.35" y="0.125"/>
<vertex x="1.35" y="-0.125"/>
<vertex x="1.65" y="-0.125"/>
<vertex x="1.65" y="-0.525"/>
<vertex x="1.35" y="-0.525"/>
<vertex x="1.35" y="-0.775"/>
<vertex x="1.65" y="-0.775"/>
<vertex x="1.65" y="-1.175"/>
<vertex x="1.35" y="-1.175"/>
<vertex x="1.35" y="-1.225"/>
<vertex x="0.215" y="-1.225"/>
<vertex x="0.215" y="-1.605"/>
<vertex x="-0.215" y="-1.605"/>
<vertex x="-0.215" y="-1.225"/>
<vertex x="-0.63" y="-1.225"/>
</polygon>
<rectangle x1="-0.215" y1="1.23" x2="0.215" y2="1.605" layer="31"/>
<rectangle x1="1.355" y1="0.775" x2="1.65" y2="1.175" layer="31"/>
<rectangle x1="1.355" y1="0.125" x2="1.65" y2="0.525" layer="31"/>
<rectangle x1="1.355" y1="-0.525" x2="1.65" y2="-0.125" layer="31"/>
<rectangle x1="1.355" y1="-1.175" x2="1.65" y2="-0.775" layer="31"/>
<rectangle x1="-0.215" y1="-1.605" x2="0.215" y2="-1.23" layer="31" rot="R180"/>
<polygon width="0.01" layer="29">
<vertex x="-0.725" y="1.325"/>
<vertex x="-0.325" y="1.325"/>
<vertex x="-0.325" y="1.7"/>
<vertex x="0.3" y="1.7"/>
<vertex x="0.3" y="1.325"/>
<vertex x="1.45" y="1.325"/>
<vertex x="1.45" y="1.275"/>
<vertex x="1.75" y="1.275"/>
<vertex x="1.75" y="0.675"/>
<vertex x="1.45" y="0.675"/>
<vertex x="1.45" y="0.625"/>
<vertex x="1.75" y="0.625"/>
<vertex x="1.75" y="0.025"/>
<vertex x="1.45" y="0.025"/>
<vertex x="1.45" y="-0.025"/>
<vertex x="1.75" y="-0.025"/>
<vertex x="1.75" y="-0.625"/>
<vertex x="1.45" y="-0.625"/>
<vertex x="1.45" y="-0.675"/>
<vertex x="1.75" y="-0.675"/>
<vertex x="1.75" y="-1.25"/>
<vertex x="1.45" y="-1.25"/>
<vertex x="1.45" y="-1.325"/>
<vertex x="0.325" y="-1.325"/>
<vertex x="0.325" y="-1.7"/>
<vertex x="-0.325" y="-1.7"/>
<vertex x="-0.325" y="-1.325"/>
<vertex x="-0.725" y="-1.325"/>
</polygon>
<smd name="2" x="-1.4" y="0.325" dx="0.5" dy="0.35" layer="1"/>
<smd name="3" x="-1.4" y="-0.325" dx="0.5" dy="0.35" layer="1"/>
<smd name="1" x="-1.4" y="0.975" dx="0.5" dy="0.35" layer="1"/>
<smd name="4" x="-1.4" y="-0.975" dx="0.5" dy="0.35" layer="1"/>
<smd name="9" x="0.36" y="0" dx="1" dy="1" layer="1" stop="no" thermals="no" cream="no"/>
</package>
<package name="SOD-323">
<description>SOD-323 (Small Outline Diode)</description>
<wire x1="-1.77" y1="0.625" x2="-1.77" y2="-0.625" width="0.2032" layer="21"/>
<smd name="C" x="-1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<smd name="A" x="1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-0.9" y1="0.625" x2="0.9" y2="0.625" width="0.2032" layer="21"/>
<wire x1="-0.9" y1="-0.625" x2="0.9" y2="-0.625" width="0.2032" layer="21"/>
</package>
<package name="SOD-523">
<description>SOD-523 (Small Outline Diode)</description>
<smd name="C" x="0.7" y="0" dx="0.4" dy="0.4" layer="1"/>
<smd name="A" x="-0.7" y="0" dx="0.4" dy="0.4" layer="1"/>
<wire x1="-0.625" y1="-0.425" x2="0.625" y2="-0.425" width="0.2032" layer="21"/>
<wire x1="0.625" y1="0.425" x2="-0.625" y2="0.425" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="-0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.4" x2="0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.4" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.1" x2="0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.4" x2="0.3" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="-0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.4" x2="-0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.1" x2="0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.1" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="1.1176" y1="0.3048" x2="1.1176" y2="-0.3048" width="0.2032" layer="21"/>
</package>
<package name="SMA-DIODE">
<description>&lt;B&gt;Diode&lt;/B&gt;
&lt;p&gt;Basic SMA packaged diode. Good for reverse polarization protection. Common part #: MBRA140&lt;/p&gt;
&lt;p&gt;SMA is the smallest package in the DO-214 standard (DO-214AC)&lt;/p&gt;</description>
<wire x1="-2.3" y1="1" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1" x2="3.175" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2.15" y="0" dx="1.27" dy="1.47" layer="1" rot="R180"/>
<smd name="C" x="2.15" y="0" dx="1.27" dy="1.47" layer="1"/>
<text x="0" y="1.651" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.651" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT23-3">
<description>SOT23-3</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="3" x="0" y="1.1" dx="0.8" dy="0.9" layer="1"/>
<text x="-1.651" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.651" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="DIODE-MICRO-SMP">
<wire x1="-1.3462" y1="0.4572" x2="-1.3462" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="1.3462" y1="-0.381" x2="1.3462" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-1.3462" y1="-0.7112" x2="1.3462" y2="-0.7112" width="0.1524" layer="51"/>
<wire x1="1.3462" y1="-0.7112" x2="1.3462" y2="0.7112" width="0.1524" layer="51"/>
<wire x1="1.3462" y1="0.7112" x2="-1.3462" y2="0.7112" width="0.1524" layer="51"/>
<wire x1="-1.3462" y1="0.7112" x2="-1.3462" y2="-0.7112" width="0.1524" layer="51"/>
<smd name="C" x="-0.6985" y="0" dx="2.0066" dy="1.102359375" layer="1"/>
<smd name="A" x="1.2192" y="0" dx="0.8128" dy="0.8128" layer="1"/>
<text x="0" y="1.016" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.6002" size="0.6096" layer="27" font="vector" ratio="20" align="bottom-center">&gt;Value</text>
<wire x1="-2.0574" y1="-0.6096" x2="-2.0574" y2="0.5842" width="0.127" layer="21"/>
</package>
<package name="DIODE_PMEG045T100EPDZ">
<wire x1="-2.95" y1="-2.2" x2="-2.95" y2="2.2" width="0.127" layer="51"/>
<wire x1="-2.95" y1="2.2" x2="2.95" y2="2.2" width="0.127" layer="51"/>
<wire x1="2.95" y1="2.2" x2="2.95" y2="-2.2" width="0.127" layer="51"/>
<wire x1="2.95" y1="-2.2" x2="-2.95" y2="-2.2" width="0.127" layer="51"/>
<wire x1="-3.75" y1="-2.45" x2="-3.75" y2="2.45" width="0.05" layer="39"/>
<wire x1="-3.75" y1="2.45" x2="3.75" y2="2.45" width="0.05" layer="39"/>
<wire x1="3.75" y1="2.45" x2="3.75" y2="-2.45" width="0.05" layer="39"/>
<wire x1="3.75" y1="-2.45" x2="-3.75" y2="-2.45" width="0.05" layer="39"/>
<wire x1="2.95" y1="-2.2" x2="-2.95" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-2.95" y1="2.2" x2="2.95" y2="2.2" width="0.127" layer="21"/>
<circle x="-4" y="-1.2" radius="0.1" width="0.2" layer="21"/>
<circle x="-4" y="-1.2" radius="0.1" width="0.2" layer="51"/>
<rectangle x1="-3.45" y1="-1.765" x2="-2.11" y2="-0.365" layer="31"/>
<rectangle x1="-3.45" y1="0.365" x2="-2.11" y2="1.765" layer="31"/>
<rectangle x1="1.95" y1="-0.3" x2="4.35" y2="0.3" layer="31" rot="R90"/>
<rectangle x1="-1.25" y1="-1.9" x2="0.45" y2="0" layer="31" rot="R90"/>
<rectangle x1="0.85" y1="-1.9" x2="2.55" y2="0" layer="31" rot="R90"/>
<rectangle x1="-1.25" y1="0" x2="0.45" y2="1.9" layer="31" rot="R90"/>
<rectangle x1="0.85" y1="0" x2="2.55" y2="1.9" layer="31" rot="R90"/>
<text x="-3" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.9" size="1.27" layer="27">&gt;VALUE</text>
<smd name="1" x="-2.78" y="-1.065" dx="1.6" dy="1.44" layer="1" rot="R90" cream="no"/>
<smd name="2" x="-2.78" y="1.065" dx="1.6" dy="1.44" layer="1" rot="R90" cream="no"/>
<smd name="3" x="1.02" y="0" dx="3.8" dy="4.96" layer="1" rot="R90" cream="no"/>
</package>
</packages>
<symbols>
<symbol name="NFET-TPW4R008NH">
<description>MOSFET N-channel - Enhancement mode</description>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.254" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.254" width="0" layer="94"/>
<text x="-11.43" y="3.81" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
<text x="-11.43" y="1.27" size="1.778" layer="95" rot="MR180">&gt;NAME</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.508"/>
<vertex x="2.032" y="-0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<wire x1="3.302" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
</symbol>
<symbol name="DIODE-SCHOTTKY">
<description>&lt;h3&gt; Schottky Diode&lt;/h3&gt;
Diode with low voltage drop</description>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.778" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.1524" layer="94"/>
<text x="-2.54" y="2.032" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.27" x2="0.762" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="1.016" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-1.27" y="1.27"/>
<vertex x="1.27" y="0"/>
<vertex x="-1.27" y="-1.27"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="NFET" prefix="Q">
<gates>
<gate name="1" symbol="NFET-TPW4R008NH" x="0" y="0"/>
</gates>
<devices>
<device name="TPW4R008NH" package="DSOP-ADVANCE">
<connects>
<connect gate="1" pin="D" pad="P$5"/>
<connect gate="1" pin="G" pad="P$4"/>
<connect gate="1" pin="S" pad="P$1 P$2 P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AON7524" package="TRANS_AON7408">
<connects>
<connect gate="1" pin="D" pad="9"/>
<connect gate="1" pin="G" pad="4"/>
<connect gate="1" pin="S" pad="1 2 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE-SCHOTTKY" prefix="D" uservalue="yes">
<description>&lt;h3&gt;Schottky diode&lt;/h3&gt;
&lt;p&gt;A Schottky diode is a semiconductor diode which has a low forward voltage drop and a very fast switching action.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE-SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="-BAT20J" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11623"/>
<attribute name="VALUE" value="1A/23V/620mV"/>
</technology>
</technologies>
</device>
<device name="-RB751S40" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11018"/>
<attribute name="VALUE" value="120mA/40V/370mV"/>
</technology>
</technologies>
</device>
<device name="-SS14" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08053"/>
<attribute name="VALUE" value="1A/40V/500mV"/>
</technology>
</technologies>
</device>
<device name="-PMEG4005EJ" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-10955"/>
<attribute name="VALUE" value="0.5A/40V/420mV"/>
</technology>
</technologies>
</device>
<device name="-B340A" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-09886"/>
<attribute name="VALUE" value="3A/40V/500mV"/>
</technology>
</technologies>
</device>
<device name="-ZLLS500" package="SOT23-3">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08411"/>
<attribute name="VALUE" value="700mA/40V/533mV"/>
</technology>
</technologies>
</device>
<device name="-MSS1P6-M3/89A" package="DIODE-MICRO-SMP">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-13919" constant="no"/>
<attribute name="VALUE" value="1A/60V/520mV" constant="no"/>
</technology>
</technologies>
</device>
<device name="-BAT60A" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-14072"/>
<attribute name="VALUE" value="3A/10V/280mV"/>
</technology>
</technologies>
</device>
<device name="PMEG045V100EPDAZ" package="DIODE_PMEG045T100EPDZ">
<connects>
<connect gate="G$1" pin="A" pad="1 2"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lights">
<packages>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED0805">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.0778" y1="0.2818" x2="0.1278" y2="0" width="0.127" layer="21"/>
<wire x1="0.1278" y1="0" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
<wire x1="-0.0778" y1="0.2818" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
</package>
<package name="LED-5630">
<smd name="P$2" x="2.6" y="0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$3" x="2.6" y="-0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$4" x="-2.6" y="0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$5" x="-2.6" y="-0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<rectangle x1="-2.5" y1="-1.5" x2="2.5" y2="1.5" layer="51"/>
<wire x1="0" y1="1.4" x2="0" y2="1.9" width="0.1016" layer="21"/>
<wire x1="0" y1="-1.4" x2="0" y2="-1.9" width="0.1016" layer="21"/>
<polygon width="0" layer="1">
<vertex x="-2.4" y="1"/>
<vertex x="-2.2" y="1"/>
<vertex x="-2" y="1.2"/>
<vertex x="-1.4" y="1.2"/>
<vertex x="-1.4" y="-1.2"/>
<vertex x="-2" y="-1.2"/>
<vertex x="-2.2" y="-1"/>
<vertex x="-2.4" y="-1"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="2.4" y="1"/>
<vertex x="2.2" y="1"/>
<vertex x="2" y="1.2"/>
<vertex x="-0.8" y="1.2"/>
<vertex x="-0.8" y="-1.2"/>
<vertex x="2" y="-1.2"/>
<vertex x="2.2" y="-1"/>
<vertex x="2.4" y="-1"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="2.2" y="1.1"/>
<vertex x="2" y="1.3"/>
<vertex x="-0.9" y="1.3"/>
<vertex x="-0.9" y="-1.3"/>
<vertex x="2" y="-1.3"/>
<vertex x="2.2" y="-1.1"/>
<vertex x="3" y="-1.1"/>
<vertex x="3" y="-0.1"/>
<vertex x="2.4" y="-0.1"/>
<vertex x="2.4" y="0.1"/>
<vertex x="3" y="0.1"/>
<vertex x="3" y="1.1"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-3" y="1.1"/>
<vertex x="-2.2" y="1.1"/>
<vertex x="-2" y="1.3"/>
<vertex x="-1.3" y="1.3"/>
<vertex x="-1.3" y="-1.3"/>
<vertex x="-2" y="-1.3"/>
<vertex x="-2.2" y="-1.1"/>
<vertex x="-3" y="-1.1"/>
<vertex x="-3" y="-0.1"/>
<vertex x="-2.5" y="-0.1"/>
<vertex x="-2.5" y="0.1"/>
<vertex x="-3" y="0.1"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-2" y="1.1"/>
<vertex x="-2" y="-1.1"/>
<vertex x="-1.5" y="-1.1"/>
<vertex x="-1.5" y="1.1"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-0.7" y="1.1"/>
<vertex x="2" y="1.1"/>
<vertex x="2" y="-1.1"/>
<vertex x="-0.7" y="-1.1"/>
</polygon>
<wire x1="2.6" y1="-1.2" x2="2.8" y2="-1.4" width="0.1016" layer="21"/>
<wire x1="2.8" y1="-1.4" x2="2.6" y2="-1.6" width="0.1016" layer="21"/>
<wire x1="2.6" y1="-1.6" x2="2.6" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="2.9" y1="-1.2" x2="2.9" y2="-1.6" width="0.1016" layer="21"/>
</package>
<package name="0402-D">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<smd name="2" x="0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1" y1="-0.2" x2="-1" y2="0.2" width="0.127" layer="21"/>
</package>
<package name="LED0603-RIGHTANGLE">
<smd name="1" x="-1" y="0" dx="1.1" dy="1.1" layer="1"/>
<smd name="2" x="1" y="0" dx="1.1" dy="1.1" layer="1"/>
<text x="-0.089" y="1.197" size="0.8128" layer="25" align="center">&gt;NAME</text>
<text x="-0.016" y="-1.313" size="0.8128" layer="27" ratio="10" align="center">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.3818" x2="0.2278" y2="0" width="0.127" layer="21"/>
<wire x1="0.2278" y1="0" x2="-0.1778" y2="-0.3818" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.3818" x2="-0.1778" y2="-0.3818" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.2" x2="1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="1" y1="-0.2" x2="0.4" y2="-0.8" width="0.127" layer="51" curve="-90"/>
<wire x1="0.4" y1="-0.8" x2="-0.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.4" y1="-0.8" x2="-1" y2="-0.2" width="0.127" layer="51" curve="-90"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5630" package="LED-5630">
<connects>
<connect gate="G$1" pin="A" pad="P$4 P$5"/>
<connect gate="G$1" pin="C" pad="P$2 P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402-D">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603RA" package="LED0603-RIGHTANGLE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J1" library="SparkFun-Connectors" deviceset="CONN_05X2" device="SHD_LOCK_LATCH"/>
<part name="J4" library="connector" deviceset="PWRPAD" device="M3-SKINNY"/>
<part name="J5" library="connector" deviceset="PWRPAD" device="M3-SKINNY"/>
<part name="J6" library="connector" deviceset="PWRPAD" device="M3-SKINNY"/>
<part name="R5" library="passives" deviceset="RESISTOR" device="2010" value="0.2R"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="P+4" library="supply1" deviceset="+5V" device=""/>
<part name="P+5" library="supply1" deviceset="+5V" device=""/>
<part name="P+3" library="supply1" deviceset="+24V" device=""/>
<part name="P+6" library="supply1" deviceset="+24V" device=""/>
<part name="D1" library="passives" deviceset="DIODE" device="SMA403D"/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="P+8" library="supply1" deviceset="+24V" device=""/>
<part name="R17" library="passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="R18" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="P+10" library="supply1" deviceset="+24V" device=""/>
<part name="R19" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="C22" library="passives" deviceset="CAP-POL" device="CAP_POL_SMD6.6X6.6" value="150uF 35v"/>
<part name="U1" library="connector" deviceset="UCBUS_MODULE_EXT" device=""/>
<part name="P+7" library="supply1" deviceset="+5V" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="C1" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="Q1" library="power" deviceset="NFET" device="AON7524"/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="R1" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="C2" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="D2" library="power" deviceset="DIODE-SCHOTTKY" device="PMEG045V100EPDAZ"/>
<part name="P+1" library="supply1" deviceset="+24V" device=""/>
<part name="J2" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK_LONGPADS"/>
<part name="R2" library="passives" deviceset="RESISTOR" device="0805-RES" value="120R"/>
<part name="J3" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="R3" library="passives" deviceset="RESISTOR" device="2010" value="0.2R"/>
<part name="Q2" library="power" deviceset="NFET" device="AON7524"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="R4" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="C3" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="D3" library="power" deviceset="DIODE-SCHOTTKY" device="PMEG045V100EPDAZ"/>
<part name="P+2" library="supply1" deviceset="+24V" device=""/>
<part name="J7" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK_LONGPADS"/>
<part name="R6" library="passives" deviceset="RESISTOR" device="0805-RES" value="120R"/>
<part name="J8" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="Q3" library="power" deviceset="NFET" device="AON7524"/>
<part name="R7" library="passives" deviceset="RESISTOR" device="0805-RES" value="120R"/>
<part name="J9" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK_LONGPADS"/>
<part name="P+9" library="supply1" deviceset="+24V" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="Q4" library="power" deviceset="NFET" device="AON7524"/>
<part name="R8" library="passives" deviceset="RESISTOR" device="0805-RES" value="120R"/>
<part name="J10" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK_LONGPADS"/>
<part name="P+11" library="supply1" deviceset="+24V" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="Q5" library="power" deviceset="NFET" device="AON7524"/>
<part name="R9" library="passives" deviceset="RESISTOR" device="0805-RES" value="120R"/>
<part name="J11" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK_LONGPADS"/>
<part name="P+12" library="supply1" deviceset="+24V" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="Q6" library="power" deviceset="NFET" device="AON7524"/>
<part name="R10" library="passives" deviceset="RESISTOR" device="0805-RES" value="120R"/>
<part name="J12" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK_LONGPADS"/>
<part name="P+13" library="supply1" deviceset="+24V" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="J13" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK_LONGPADS"/>
<part name="R11" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="C4" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="J14" library="SparkFun-Connectors" deviceset="CONN_02" device="LOCK_LONGPADS"/>
<part name="R12" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="+3V4" library="supply1" deviceset="+3V3" device=""/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="C5" library="passives" deviceset="CAP" device="0805" value="0.1uF"/>
<part name="R14" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="D5" library="lights" deviceset="LED" device="0805"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="R13" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="D4" library="lights" deviceset="LED" device="0805"/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="D6" library="passives" deviceset="DIODE" device="SOD123HE"/>
<part name="R15" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="D7" library="lights" deviceset="LED" device="0805"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="D8" library="passives" deviceset="DIODE" device="SOD123HE"/>
<part name="D9" library="passives" deviceset="DIODE" device="SOD123HE"/>
<part name="D10" library="passives" deviceset="DIODE" device="SOD123HE"/>
<part name="R16" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="D11" library="lights" deviceset="LED" device="0805"/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="R20" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="D12" library="lights" deviceset="LED" device="0805"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="R21" library="passives" deviceset="RESISTOR" device="0805-RES" value="1k"/>
<part name="D13" library="lights" deviceset="LED" device="0805"/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="167.64" y="71.12" size="1.778" layer="97">A0</text>
<text x="167.64" y="66.04" size="1.778" layer="97">A4</text>
<text x="167.64" y="63.5" size="1.778" layer="97">A1</text>
<text x="167.64" y="60.96" size="1.778" layer="97">A5</text>
<text x="167.64" y="38.1" size="1.778" layer="97">TC7</text>
<text x="167.64" y="33.02" size="1.778" layer="97">TC4</text>
<text x="167.64" y="20.32" size="1.778" layer="97">TC5</text>
<text x="167.64" y="-5.08" size="1.778" layer="97">TC6</text>
<text x="167.64" y="45.72" size="1.778" layer="97">TC2</text>
</plain>
<instances>
<instance part="J1" gate="G$1" x="22.86" y="66.04" smashed="yes">
<attribute name="VALUE" x="19.05" y="56.134" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="19.05" y="74.168" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="J4" gate="G$1" x="358.14" y="58.42" smashed="yes"/>
<instance part="J5" gate="G$1" x="358.14" y="63.5" smashed="yes"/>
<instance part="J6" gate="G$1" x="358.14" y="68.58" smashed="yes"/>
<instance part="R5" gate="G$1" x="246.38" y="63.5" smashed="yes" rot="R270">
<attribute name="NAME" x="247.8786" y="67.31" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="243.078" y="67.31" size="1.778" layer="96" rot="R270"/>
<attribute name="PRECISION" x="239.522" y="67.31" size="1.27" layer="97" rot="R270"/>
<attribute name="PACKAGE" x="241.3" y="67.31" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="GND10" gate="1" x="40.64" y="66.04" smashed="yes" rot="R90">
<attribute name="VALUE" x="43.18" y="63.5" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND11" gate="1" x="5.08" y="66.04" smashed="yes" rot="R270">
<attribute name="VALUE" x="2.54" y="68.58" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+4" gate="1" x="40.64" y="71.12" smashed="yes" rot="R270">
<attribute name="VALUE" x="43.18" y="71.12" size="1.778" layer="96"/>
</instance>
<instance part="P+5" gate="1" x="5.08" y="60.96" smashed="yes" rot="R90">
<attribute name="VALUE" x="2.54" y="60.96" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+3" gate="1" x="5.08" y="71.12" smashed="yes" rot="R90">
<attribute name="VALUE" x="2.54" y="71.12" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+6" gate="1" x="40.64" y="60.96" smashed="yes" rot="R270">
<attribute name="VALUE" x="43.18" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="99.06" y="124.46" smashed="yes">
<attribute name="NAME" x="101.6" y="124.9426" size="1.778" layer="95"/>
<attribute name="VALUE" x="101.6" y="122.1486" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="81.28" y="124.46" smashed="yes" rot="R270">
<attribute name="VALUE" x="78.74" y="127" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+8" gate="1" x="116.84" y="124.46" smashed="yes" rot="R270">
<attribute name="VALUE" x="119.38" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="137.16" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="135.6614" y="107.95" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="140.462" y="107.95" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R18" gate="G$1" x="137.16" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="135.6614" y="92.71" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="140.462" y="92.71" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND26" gate="1" x="137.16" y="83.82" smashed="yes">
<attribute name="VALUE" x="134.62" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="P+10" gate="1" x="137.16" y="124.46" smashed="yes">
<attribute name="VALUE" x="134.62" y="119.38" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R19" gate="G$1" x="124.46" y="104.14" smashed="yes">
<attribute name="NAME" x="120.65" y="105.6386" size="1.778" layer="95"/>
<attribute name="VALUE" x="120.65" y="100.838" size="1.778" layer="96"/>
</instance>
<instance part="C22" gate="G$1" x="99.06" y="116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="104.14" y="114.3" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="96.52" y="114.3" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U1" gate="G$1" x="78.74" y="43.18" smashed="yes"/>
<instance part="P+7" gate="1" x="53.34" y="22.86" smashed="yes" rot="R90">
<attribute name="VALUE" x="50.8" y="22.86" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+3V1" gate="G$1" x="53.34" y="15.24" smashed="yes" rot="R90">
<attribute name="VALUE" x="58.42" y="12.7" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="53.34" y="7.62" smashed="yes" rot="R270">
<attribute name="VALUE" x="50.8" y="10.16" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V2" gate="G$1" x="190.5" y="68.58" smashed="yes" rot="R270">
<attribute name="VALUE" x="185.42" y="71.12" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="180.34" y="63.5" smashed="yes">
<attribute name="NAME" x="181.864" y="66.421" size="1.778" layer="95"/>
<attribute name="VALUE" x="181.864" y="61.341" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="180.34" y="55.88" smashed="yes">
<attribute name="VALUE" x="177.8" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="Q1" gate="1" x="243.84" y="78.74" smashed="yes">
<attribute name="VALUE" x="248.92" y="78.74" size="1.778" layer="96" rot="MR90"/>
<attribute name="NAME" x="248.92" y="76.2" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="GND3" gate="1" x="246.38" y="53.34" smashed="yes">
<attribute name="VALUE" x="243.84" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="220.98" y="71.12" smashed="yes">
<attribute name="NAME" x="217.17" y="72.6186" size="1.778" layer="95"/>
<attribute name="VALUE" x="217.17" y="67.818" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="G$1" x="228.6" y="66.04" smashed="yes">
<attribute name="NAME" x="231.14" y="66.04" size="1.778" layer="95"/>
<attribute name="VALUE" x="231.14" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="228.6" y="53.34" smashed="yes">
<attribute name="VALUE" x="226.06" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="254" y="86.36" smashed="yes">
<attribute name="NAME" x="251.46" y="88.392" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="251.46" y="84.328" size="1.778" layer="96" font="vector" align="top-left"/>
</instance>
<instance part="P+1" gate="1" x="266.7" y="88.9" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="88.9" size="1.778" layer="96"/>
</instance>
<instance part="J2" gate="G$1" x="246.38" y="106.68" smashed="yes" rot="R270">
<attribute name="VALUE" x="241.554" y="109.22" size="1.778" layer="96" font="vector" rot="R270"/>
<attribute name="NAME" x="251.968" y="109.22" size="1.778" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="R2" gate="G$1" x="228.6" y="76.2" smashed="yes">
<attribute name="NAME" x="224.79" y="77.6986" size="1.778" layer="95"/>
<attribute name="VALUE" x="224.79" y="72.898" size="1.778" layer="96"/>
</instance>
<instance part="J3" gate="G$1" x="228.6" y="106.68" smashed="yes" rot="R270">
<attribute name="VALUE" x="223.774" y="109.22" size="1.778" layer="96" font="vector" rot="R270"/>
<attribute name="NAME" x="234.188" y="109.22" size="1.778" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="GND5" gate="1" x="228.6" y="86.36" smashed="yes">
<attribute name="VALUE" x="226.06" y="83.82" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="246.38" y="0" smashed="yes" rot="R270">
<attribute name="NAME" x="247.8786" y="3.81" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="243.078" y="3.81" size="1.778" layer="96" rot="R270"/>
<attribute name="PRECISION" x="239.522" y="3.81" size="1.27" layer="97" rot="R270"/>
<attribute name="PACKAGE" x="241.3" y="3.81" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="Q2" gate="1" x="243.84" y="15.24" smashed="yes">
<attribute name="VALUE" x="248.92" y="15.24" size="1.778" layer="96" rot="MR90"/>
<attribute name="NAME" x="248.92" y="12.7" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="GND6" gate="1" x="246.38" y="-10.16" smashed="yes">
<attribute name="VALUE" x="243.84" y="-12.7" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="220.98" y="7.62" smashed="yes">
<attribute name="NAME" x="217.17" y="9.1186" size="1.778" layer="95"/>
<attribute name="VALUE" x="217.17" y="4.318" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="228.6" y="2.54" smashed="yes">
<attribute name="NAME" x="231.14" y="2.54" size="1.778" layer="95"/>
<attribute name="VALUE" x="231.14" y="0" size="1.778" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="228.6" y="-10.16" smashed="yes">
<attribute name="VALUE" x="226.06" y="-12.7" size="1.778" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="254" y="22.86" smashed="yes">
<attribute name="NAME" x="251.46" y="24.892" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="251.46" y="20.828" size="1.778" layer="96" font="vector" align="top-left"/>
</instance>
<instance part="P+2" gate="1" x="266.7" y="25.4" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="J7" gate="G$1" x="246.38" y="43.18" smashed="yes" rot="R270">
<attribute name="VALUE" x="241.554" y="45.72" size="1.778" layer="96" font="vector" rot="R270"/>
<attribute name="NAME" x="251.968" y="45.72" size="1.778" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="R6" gate="G$1" x="228.6" y="12.7" smashed="yes">
<attribute name="NAME" x="224.79" y="14.1986" size="1.778" layer="95"/>
<attribute name="VALUE" x="224.79" y="9.398" size="1.778" layer="96"/>
</instance>
<instance part="J8" gate="G$1" x="228.6" y="43.18" smashed="yes" rot="R270">
<attribute name="VALUE" x="223.774" y="45.72" size="1.778" layer="96" font="vector" rot="R270"/>
<attribute name="NAME" x="234.188" y="45.72" size="1.778" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="GND8" gate="1" x="228.6" y="22.86" smashed="yes">
<attribute name="VALUE" x="226.06" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="Q3" gate="1" x="243.84" y="-48.26" smashed="yes">
<attribute name="VALUE" x="248.92" y="-48.26" size="1.778" layer="96" rot="MR90"/>
<attribute name="NAME" x="248.92" y="-50.8" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="R7" gate="G$1" x="228.6" y="-50.8" smashed="yes">
<attribute name="NAME" x="224.79" y="-49.3014" size="1.778" layer="95"/>
<attribute name="VALUE" x="224.79" y="-54.102" size="1.778" layer="96"/>
</instance>
<instance part="J9" gate="G$1" x="246.38" y="-27.94" smashed="yes" rot="R270">
<attribute name="VALUE" x="241.554" y="-25.4" size="1.778" layer="96" font="vector" rot="R270"/>
<attribute name="NAME" x="251.968" y="-25.4" size="1.778" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="P+9" gate="1" x="266.7" y="-38.1" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="-38.1" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="246.38" y="-63.5" smashed="yes">
<attribute name="VALUE" x="243.84" y="-66.04" size="1.778" layer="96"/>
</instance>
<instance part="Q4" gate="1" x="243.84" y="-93.98" smashed="yes">
<attribute name="VALUE" x="248.92" y="-93.98" size="1.778" layer="96" rot="MR90"/>
<attribute name="NAME" x="248.92" y="-96.52" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="R8" gate="G$1" x="228.6" y="-96.52" smashed="yes">
<attribute name="NAME" x="224.79" y="-95.0214" size="1.778" layer="95"/>
<attribute name="VALUE" x="224.79" y="-99.822" size="1.778" layer="96"/>
</instance>
<instance part="J10" gate="G$1" x="246.38" y="-73.66" smashed="yes" rot="R270">
<attribute name="VALUE" x="241.554" y="-71.12" size="1.778" layer="96" font="vector" rot="R270"/>
<attribute name="NAME" x="251.968" y="-71.12" size="1.778" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="P+11" gate="1" x="266.7" y="-83.82" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="-83.82" size="1.778" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="246.38" y="-109.22" smashed="yes">
<attribute name="VALUE" x="243.84" y="-111.76" size="1.778" layer="96"/>
</instance>
<instance part="Q5" gate="1" x="243.84" y="-139.7" smashed="yes">
<attribute name="VALUE" x="248.92" y="-139.7" size="1.778" layer="96" rot="MR90"/>
<attribute name="NAME" x="248.92" y="-142.24" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="R9" gate="G$1" x="228.6" y="-142.24" smashed="yes">
<attribute name="NAME" x="224.79" y="-140.7414" size="1.778" layer="95"/>
<attribute name="VALUE" x="224.79" y="-145.542" size="1.778" layer="96"/>
</instance>
<instance part="J11" gate="G$1" x="246.38" y="-119.38" smashed="yes" rot="R270">
<attribute name="VALUE" x="241.554" y="-116.84" size="1.778" layer="96" font="vector" rot="R270"/>
<attribute name="NAME" x="251.968" y="-116.84" size="1.778" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="P+12" gate="1" x="266.7" y="-129.54" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="-129.54" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="246.38" y="-154.94" smashed="yes">
<attribute name="VALUE" x="243.84" y="-157.48" size="1.778" layer="96"/>
</instance>
<instance part="Q6" gate="1" x="243.84" y="-185.42" smashed="yes">
<attribute name="VALUE" x="248.92" y="-185.42" size="1.778" layer="96" rot="MR90"/>
<attribute name="NAME" x="248.92" y="-187.96" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="R10" gate="G$1" x="228.6" y="-187.96" smashed="yes">
<attribute name="NAME" x="224.79" y="-186.4614" size="1.778" layer="95"/>
<attribute name="VALUE" x="224.79" y="-191.262" size="1.778" layer="96"/>
</instance>
<instance part="J12" gate="G$1" x="246.38" y="-165.1" smashed="yes" rot="R270">
<attribute name="VALUE" x="241.554" y="-162.56" size="1.778" layer="96" font="vector" rot="R270"/>
<attribute name="NAME" x="251.968" y="-162.56" size="1.778" layer="95" font="vector" rot="R270"/>
</instance>
<instance part="P+13" gate="1" x="266.7" y="-175.26" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="-175.26" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="246.38" y="-200.66" smashed="yes">
<attribute name="VALUE" x="243.84" y="-203.2" size="1.778" layer="96"/>
</instance>
<instance part="J13" gate="G$1" x="325.12" y="88.9" smashed="yes" rot="R180">
<attribute name="VALUE" x="327.66" y="93.726" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="327.66" y="83.312" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="R11" gate="G$1" x="299.72" y="99.06" smashed="yes" rot="R270">
<attribute name="NAME" x="301.2186" y="102.87" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="296.418" y="102.87" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V3" gate="G$1" x="299.72" y="109.22" smashed="yes">
<attribute name="VALUE" x="297.18" y="104.14" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND15" gate="1" x="299.72" y="68.58" smashed="yes">
<attribute name="VALUE" x="297.18" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="299.72" y="81.28" smashed="yes">
<attribute name="NAME" x="302.26" y="81.28" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.26" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="J14" gate="G$1" x="325.12" y="25.4" smashed="yes" rot="R180">
<attribute name="VALUE" x="327.66" y="30.226" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="327.66" y="19.812" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="R12" gate="G$1" x="299.72" y="35.56" smashed="yes" rot="R270">
<attribute name="NAME" x="301.2186" y="39.37" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="296.418" y="39.37" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V4" gate="G$1" x="299.72" y="45.72" smashed="yes">
<attribute name="VALUE" x="297.18" y="40.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND16" gate="1" x="299.72" y="5.08" smashed="yes">
<attribute name="VALUE" x="297.18" y="2.54" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="299.72" y="17.78" smashed="yes">
<attribute name="NAME" x="302.26" y="17.78" size="1.778" layer="95"/>
<attribute name="VALUE" x="302.26" y="15.24" size="1.778" layer="96"/>
</instance>
<instance part="R14" gate="G$1" x="213.36" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="211.8614" y="92.71" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="216.662" y="92.71" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D5" gate="G$1" x="213.36" y="86.36" smashed="yes" rot="R180">
<attribute name="NAME" x="209.804" y="88.392" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="207.645" y="88.392" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND17" gate="1" x="213.36" y="106.68" smashed="yes" rot="R180">
<attribute name="VALUE" x="215.9" y="109.22" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R13" gate="G$1" x="213.36" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="211.8614" y="29.21" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="216.662" y="29.21" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D4" gate="G$1" x="213.36" y="22.86" smashed="yes" rot="R180">
<attribute name="NAME" x="209.804" y="24.892" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="207.645" y="24.892" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND19" gate="1" x="213.36" y="43.18" smashed="yes" rot="R180">
<attribute name="VALUE" x="215.9" y="45.72" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D6" gate="G$1" x="254" y="-43.18" smashed="yes">
<attribute name="NAME" x="256.54" y="-42.6974" size="1.778" layer="95"/>
<attribute name="VALUE" x="256.54" y="-45.4914" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="213.36" y="-40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="211.8614" y="-44.45" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="216.662" y="-44.45" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D7" gate="G$1" x="213.36" y="-27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="209.804" y="-25.908" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="207.645" y="-25.908" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND20" gate="1" x="213.36" y="-20.32" smashed="yes" rot="R180">
<attribute name="VALUE" x="215.9" y="-17.78" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D8" gate="G$1" x="254" y="-88.9" smashed="yes">
<attribute name="NAME" x="256.54" y="-88.4174" size="1.778" layer="95"/>
<attribute name="VALUE" x="256.54" y="-91.2114" size="1.778" layer="96"/>
</instance>
<instance part="D9" gate="G$1" x="254" y="-134.62" smashed="yes">
<attribute name="NAME" x="256.54" y="-134.1374" size="1.778" layer="95"/>
<attribute name="VALUE" x="256.54" y="-136.9314" size="1.778" layer="96"/>
</instance>
<instance part="D10" gate="G$1" x="254" y="-180.34" smashed="yes">
<attribute name="NAME" x="256.54" y="-179.8574" size="1.778" layer="95"/>
<attribute name="VALUE" x="256.54" y="-182.6514" size="1.778" layer="96"/>
</instance>
<instance part="R16" gate="G$1" x="213.36" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="211.8614" y="-90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="216.662" y="-90.17" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D11" gate="G$1" x="213.36" y="-73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="209.804" y="-71.628" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="207.645" y="-71.628" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND21" gate="1" x="213.36" y="-66.04" smashed="yes" rot="R180">
<attribute name="VALUE" x="215.9" y="-63.5" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R20" gate="G$1" x="213.36" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="211.8614" y="-135.89" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="216.662" y="-135.89" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D12" gate="G$1" x="213.36" y="-119.38" smashed="yes" rot="R180">
<attribute name="NAME" x="209.804" y="-117.348" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="207.645" y="-117.348" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND22" gate="1" x="213.36" y="-111.76" smashed="yes" rot="R180">
<attribute name="VALUE" x="215.9" y="-109.22" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R21" gate="G$1" x="213.36" y="-177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="211.8614" y="-181.61" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="216.662" y="-181.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D13" gate="G$1" x="213.36" y="-165.1" smashed="yes" rot="R180">
<attribute name="NAME" x="209.804" y="-163.068" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="207.645" y="-163.068" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND23" gate="1" x="213.36" y="-157.48" smashed="yes" rot="R180">
<attribute name="VALUE" x="215.9" y="-154.94" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="6"/>
<wire x1="30.48" y1="66.04" x2="38.1" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="5"/>
<wire x1="15.24" y1="66.04" x2="7.62" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="96.52" y1="124.46" x2="93.98" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="C"/>
<wire x1="93.98" y1="124.46" x2="83.82" y2="124.46" width="0.1524" layer="91"/>
<wire x1="96.52" y1="116.84" x2="93.98" y2="116.84" width="0.1524" layer="91"/>
<wire x1="93.98" y1="116.84" x2="93.98" y2="124.46" width="0.1524" layer="91"/>
<junction x="93.98" y="124.46"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="137.16" y1="86.36" x2="137.16" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="66.04" y1="7.62" x2="55.88" y2="7.62" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="180.34" y1="58.42" x2="180.34" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="246.38" y1="55.88" x2="246.38" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="228.6" y1="55.88" x2="228.6" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="228.6" y1="99.06" x2="228.6" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="246.38" y1="-7.62" x2="246.38" y2="-5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="228.6" y1="-7.62" x2="228.6" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="1"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="228.6" y1="35.56" x2="228.6" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="Q3" gate="1" pin="S"/>
<wire x1="246.38" y1="-60.96" x2="246.38" y2="-53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="Q4" gate="1" pin="S"/>
<wire x1="246.38" y1="-106.68" x2="246.38" y2="-99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="Q5" gate="1" pin="S"/>
<wire x1="246.38" y1="-152.4" x2="246.38" y2="-144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="Q6" gate="1" pin="S"/>
<wire x1="246.38" y1="-198.12" x2="246.38" y2="-190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="299.72" y1="71.12" x2="299.72" y2="76.2" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="2"/>
<wire x1="299.72" y1="76.2" x2="299.72" y2="78.74" width="0.1524" layer="91"/>
<wire x1="317.5" y1="86.36" x2="309.88" y2="86.36" width="0.1524" layer="91"/>
<wire x1="309.88" y1="86.36" x2="309.88" y2="76.2" width="0.1524" layer="91"/>
<wire x1="309.88" y1="76.2" x2="299.72" y2="76.2" width="0.1524" layer="91"/>
<junction x="299.72" y="76.2"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="299.72" y1="7.62" x2="299.72" y2="12.7" width="0.1524" layer="91"/>
<pinref part="J14" gate="G$1" pin="2"/>
<wire x1="299.72" y1="12.7" x2="299.72" y2="15.24" width="0.1524" layer="91"/>
<wire x1="317.5" y1="22.86" x2="309.88" y2="22.86" width="0.1524" layer="91"/>
<wire x1="309.88" y1="22.86" x2="309.88" y2="12.7" width="0.1524" layer="91"/>
<wire x1="309.88" y1="12.7" x2="299.72" y2="12.7" width="0.1524" layer="91"/>
<junction x="299.72" y="12.7"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="213.36" y1="101.6" x2="213.36" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="213.36" y1="38.1" x2="213.36" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D7" gate="G$1" pin="C"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="213.36" y1="-25.4" x2="213.36" y2="-22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D11" gate="G$1" pin="C"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="213.36" y1="-71.12" x2="213.36" y2="-68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D12" gate="G$1" pin="C"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="213.36" y1="-116.84" x2="213.36" y2="-114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D13" gate="G$1" pin="C"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="213.36" y1="-162.56" x2="213.36" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="+3V3"/>
<wire x1="66.04" y1="15.24" x2="55.88" y2="15.24" width="0.1524" layer="91"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA03/VREFA/ADC0-1"/>
<wire x1="147.32" y1="68.58" x2="180.34" y2="68.58" width="0.1524" layer="91"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="180.34" y1="68.58" x2="187.96" y2="68.58" width="0.1524" layer="91"/>
<junction x="180.34" y="68.58"/>
</segment>
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="299.72" y1="106.68" x2="299.72" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="299.72" y1="43.18" x2="299.72" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="15.24" y1="71.12" x2="7.62" y2="71.12" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+24V"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="10"/>
<wire x1="30.48" y1="60.96" x2="38.1" y2="60.96" width="0.1524" layer="91"/>
<pinref part="P+6" gate="1" pin="+24V"/>
</segment>
<segment>
<pinref part="P+8" gate="1" pin="+24V"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="114.3" y1="124.46" x2="106.68" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="A"/>
<wire x1="106.68" y1="124.46" x2="101.6" y2="124.46" width="0.1524" layer="91"/>
<wire x1="104.14" y1="116.84" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
<wire x1="106.68" y1="116.84" x2="106.68" y2="124.46" width="0.1524" layer="91"/>
<junction x="106.68" y="124.46"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="P+10" gate="1" pin="+24V"/>
<wire x1="137.16" y1="116.84" x2="137.16" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+1" gate="1" pin="+24V"/>
<wire x1="259.08" y1="88.9" x2="264.16" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="248.92" y1="99.06" x2="248.92" y2="96.52" width="0.1524" layer="91"/>
<wire x1="248.92" y1="96.52" x2="259.08" y2="96.52" width="0.1524" layer="91"/>
<wire x1="259.08" y1="96.52" x2="259.08" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="256.54" y1="86.36" x2="259.08" y2="86.36" width="0.1524" layer="91"/>
<wire x1="259.08" y1="86.36" x2="259.08" y2="88.9" width="0.1524" layer="91"/>
<junction x="259.08" y="88.9"/>
</segment>
<segment>
<pinref part="P+2" gate="1" pin="+24V"/>
<wire x1="259.08" y1="25.4" x2="264.16" y2="25.4" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="248.92" y1="35.56" x2="248.92" y2="33.02" width="0.1524" layer="91"/>
<wire x1="248.92" y1="33.02" x2="259.08" y2="33.02" width="0.1524" layer="91"/>
<wire x1="259.08" y1="33.02" x2="259.08" y2="25.4" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="256.54" y1="22.86" x2="259.08" y2="22.86" width="0.1524" layer="91"/>
<wire x1="259.08" y1="22.86" x2="259.08" y2="25.4" width="0.1524" layer="91"/>
<junction x="259.08" y="25.4"/>
</segment>
<segment>
<pinref part="P+9" gate="1" pin="+24V"/>
<wire x1="264.16" y1="-38.1" x2="259.08" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="259.08" y1="-38.1" x2="248.92" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-38.1" x2="248.92" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="256.54" y1="-43.18" x2="259.08" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-43.18" x2="259.08" y2="-38.1" width="0.1524" layer="91"/>
<junction x="259.08" y="-38.1"/>
</segment>
<segment>
<pinref part="P+11" gate="1" pin="+24V"/>
<wire x1="264.16" y1="-83.82" x2="259.08" y2="-83.82" width="0.1524" layer="91"/>
<pinref part="J10" gate="G$1" pin="2"/>
<wire x1="259.08" y1="-83.82" x2="248.92" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-83.82" x2="248.92" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="C"/>
<wire x1="256.54" y1="-88.9" x2="259.08" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-88.9" x2="259.08" y2="-83.82" width="0.1524" layer="91"/>
<junction x="259.08" y="-83.82"/>
</segment>
<segment>
<pinref part="P+12" gate="1" pin="+24V"/>
<wire x1="264.16" y1="-129.54" x2="259.08" y2="-129.54" width="0.1524" layer="91"/>
<pinref part="J11" gate="G$1" pin="2"/>
<wire x1="259.08" y1="-129.54" x2="248.92" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-129.54" x2="248.92" y2="-127" width="0.1524" layer="91"/>
<pinref part="D9" gate="G$1" pin="C"/>
<wire x1="256.54" y1="-134.62" x2="259.08" y2="-134.62" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-134.62" x2="259.08" y2="-129.54" width="0.1524" layer="91"/>
<junction x="259.08" y="-129.54"/>
</segment>
<segment>
<pinref part="P+13" gate="1" pin="+24V"/>
<wire x1="264.16" y1="-175.26" x2="259.08" y2="-175.26" width="0.1524" layer="91"/>
<pinref part="J12" gate="G$1" pin="2"/>
<wire x1="259.08" y1="-175.26" x2="248.92" y2="-175.26" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-175.26" x2="248.92" y2="-172.72" width="0.1524" layer="91"/>
<pinref part="D10" gate="G$1" pin="C"/>
<wire x1="256.54" y1="-180.34" x2="259.08" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-180.34" x2="259.08" y2="-175.26" width="0.1524" layer="91"/>
<junction x="259.08" y="-175.26"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="30.48" y1="71.12" x2="38.1" y2="71.12" width="0.1524" layer="91"/>
<pinref part="P+4" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="9"/>
<wire x1="15.24" y1="60.96" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<pinref part="P+5" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="+5V"/>
<wire x1="66.04" y1="22.86" x2="55.88" y2="22.86" width="0.1524" layer="91"/>
<pinref part="P+7" gate="1" pin="+5V"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="30.48" y1="68.58" x2="38.1" y2="68.58" width="0.1524" layer="91"/>
<label x="33.02" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="A"/>
<wire x1="66.04" y1="71.12" x2="55.88" y2="71.12" width="0.1524" layer="91"/>
<label x="58.42" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="15.24" y1="68.58" x2="7.62" y2="68.58" width="0.1524" layer="91"/>
<label x="10.16" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="B"/>
<wire x1="66.04" y1="68.58" x2="55.88" y2="68.58" width="0.1524" layer="91"/>
<label x="58.42" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="Z" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="8"/>
<wire x1="30.48" y1="63.5" x2="38.1" y2="63.5" width="0.1524" layer="91"/>
<label x="33.02" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="Z"/>
<wire x1="66.04" y1="60.96" x2="55.88" y2="60.96" width="0.1524" layer="91"/>
<label x="58.42" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="Y" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="7"/>
<wire x1="15.24" y1="63.5" x2="7.62" y2="63.5" width="0.1524" layer="91"/>
<label x="10.16" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="Y"/>
<wire x1="66.04" y1="63.5" x2="55.88" y2="63.5" width="0.1524" layer="91"/>
<label x="58.42" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="VSENSE" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="119.38" y1="104.14" x2="104.14" y2="104.14" width="0.1524" layer="91"/>
<label x="104.14" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA02/DAC-0/ADC0-1"/>
<wire x1="147.32" y1="71.12" x2="165.1" y2="71.12" width="0.1524" layer="91"/>
<label x="152.4" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="137.16" y1="106.68" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="137.16" y1="104.14" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<wire x1="129.54" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<junction x="137.16" y="104.14"/>
</segment>
</net>
<net name="LS_A" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="Q1" gate="1" pin="S"/>
<wire x1="246.38" y1="68.58" x2="246.38" y2="71.12" width="0.1524" layer="91"/>
<wire x1="246.38" y1="71.12" x2="246.38" y2="73.66" width="0.1524" layer="91"/>
<wire x1="246.38" y1="71.12" x2="228.6" y2="71.12" width="0.1524" layer="91"/>
<junction x="246.38" y="71.12"/>
<label x="231.14" y="71.12" size="1.778" layer="95"/>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="226.06" y1="71.12" x2="228.6" y2="71.12" width="0.1524" layer="91"/>
<junction x="228.6" y="71.12"/>
</segment>
</net>
<net name="LSS_A" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="215.9" y1="71.12" x2="203.2" y2="71.12" width="0.1524" layer="91"/>
<label x="203.2" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="231.14" y1="99.06" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<label x="231.14" y="96.52" size="1.778" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA06/VREFC/ADC0-6/TC1-0/SER0-2"/>
<wire x1="147.32" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<label x="152.4" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="D_A" class="0">
<segment>
<pinref part="Q1" gate="1" pin="D"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="246.38" y1="83.82" x2="246.38" y2="86.36" width="0.1524" layer="91"/>
<label x="246.38" y="86.36" size="1.778" layer="95" rot="R90"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="246.38" y1="86.36" x2="246.38" y2="99.06" width="0.1524" layer="91"/>
<wire x1="251.46" y1="86.36" x2="246.38" y2="86.36" width="0.1524" layer="91"/>
<junction x="246.38" y="86.36"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="Q1" gate="1" pin="G"/>
<wire x1="233.68" y1="76.2" x2="241.3" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="G_A" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<label x="203.2" y="76.2" size="1.778" layer="95"/>
<wire x1="223.52" y1="76.2" x2="213.36" y2="76.2" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="213.36" y1="76.2" x2="203.2" y2="76.2" width="0.1524" layer="91"/>
<wire x1="213.36" y1="76.2" x2="213.36" y2="81.28" width="0.1524" layer="91"/>
<junction x="213.36" y="76.2"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA22/TC4-0/SER5-1/SER3-0"/>
<wire x1="147.32" y1="33.02" x2="162.56" y2="33.02" width="0.1524" layer="91"/>
<label x="152.4" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="Q2" gate="1" pin="G"/>
<wire x1="233.68" y1="12.7" x2="241.3" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="G_B" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<label x="203.2" y="12.7" size="1.778" layer="95"/>
<wire x1="223.52" y1="12.7" x2="213.36" y2="12.7" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="213.36" y1="12.7" x2="203.2" y2="12.7" width="0.1524" layer="91"/>
<wire x1="213.36" y1="17.78" x2="213.36" y2="12.7" width="0.1524" layer="91"/>
<junction x="213.36" y="12.7"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA20/TC7-0/SER5-2/SER3-2"/>
<wire x1="147.32" y1="38.1" x2="162.56" y2="38.1" width="0.1524" layer="91"/>
<label x="152.4" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="LSS_B" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="215.9" y1="7.62" x2="203.2" y2="7.62" width="0.1524" layer="91"/>
<label x="203.2" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="2"/>
<wire x1="231.14" y1="35.56" x2="231.14" y2="25.4" width="0.1524" layer="91"/>
<label x="231.14" y="33.02" size="1.778" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA08/ADC0-8/ADC1-2/TC0-0/TCC0-0/SER0-0/SER2-1"/>
<wire x1="147.32" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<label x="152.4" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="LS_B" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="Q2" gate="1" pin="S"/>
<wire x1="246.38" y1="5.08" x2="246.38" y2="7.62" width="0.1524" layer="91"/>
<wire x1="246.38" y1="7.62" x2="246.38" y2="10.16" width="0.1524" layer="91"/>
<wire x1="246.38" y1="7.62" x2="228.6" y2="7.62" width="0.1524" layer="91"/>
<junction x="246.38" y="7.62"/>
<label x="231.14" y="7.62" size="1.778" layer="95"/>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="226.06" y1="7.62" x2="228.6" y2="7.62" width="0.1524" layer="91"/>
<junction x="228.6" y="7.62"/>
</segment>
</net>
<net name="D_B" class="0">
<segment>
<pinref part="Q2" gate="1" pin="D"/>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="246.38" y1="20.32" x2="246.38" y2="22.86" width="0.1524" layer="91"/>
<label x="246.38" y="22.86" size="1.778" layer="95" rot="R90"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="246.38" y1="22.86" x2="246.38" y2="35.56" width="0.1524" layer="91"/>
<wire x1="246.38" y1="22.86" x2="251.46" y2="22.86" width="0.1524" layer="91"/>
<junction x="246.38" y="22.86"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="Q3" gate="1" pin="G"/>
<wire x1="233.68" y1="-50.8" x2="241.3" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D_C" class="0">
<segment>
<pinref part="Q3" gate="1" pin="D"/>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-43.18" x2="246.38" y2="-35.56" width="0.1524" layer="91"/>
<label x="246.38" y="-40.64" size="1.778" layer="95" rot="R90"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="246.38" y1="-43.18" x2="251.46" y2="-43.18" width="0.1524" layer="91"/>
<junction x="246.38" y="-43.18"/>
</segment>
</net>
<net name="G_C" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<label x="203.2" y="-50.8" size="1.778" layer="95"/>
<wire x1="223.52" y1="-50.8" x2="213.36" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="213.36" y1="-50.8" x2="203.2" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-45.72" x2="213.36" y2="-50.8" width="0.1524" layer="91"/>
<junction x="213.36" y="-50.8"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB10/TC5-0/TCC0-4/SER4-2"/>
<wire x1="147.32" y1="20.32" x2="162.56" y2="20.32" width="0.1524" layer="91"/>
<label x="152.4" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="Q4" gate="1" pin="G"/>
<wire x1="233.68" y1="-96.52" x2="241.3" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="Q5" gate="1" pin="G"/>
<wire x1="233.68" y1="-142.24" x2="241.3" y2="-142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="Q6" gate="1" pin="G"/>
<wire x1="233.68" y1="-187.96" x2="241.3" y2="-187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="G_D" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="223.52" y1="-96.52" x2="213.36" y2="-96.52" width="0.1524" layer="91"/>
<label x="203.2" y="-96.52" size="1.778" layer="95"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="213.36" y1="-96.52" x2="203.2" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-91.44" x2="213.36" y2="-96.52" width="0.1524" layer="91"/>
<junction x="213.36" y="-96.52"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB11/TC5-1/TCC0-5/SER4-3"/>
<wire x1="147.32" y1="17.78" x2="162.56" y2="17.78" width="0.1524" layer="91"/>
<label x="152.4" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="G_E" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="223.52" y1="-142.24" x2="213.36" y2="-142.24" width="0.1524" layer="91"/>
<label x="203.2" y="-142.24" size="1.778" layer="95"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="213.36" y1="-142.24" x2="203.2" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-137.16" x2="213.36" y2="-142.24" width="0.1524" layer="91"/>
<junction x="213.36" y="-142.24"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB17/SER5-1/TC6-1"/>
<wire x1="147.32" y1="-2.54" x2="162.56" y2="-2.54" width="0.1524" layer="91"/>
<label x="152.4" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="G_F" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="223.52" y1="-187.96" x2="213.36" y2="-187.96" width="0.1524" layer="91"/>
<label x="203.2" y="-187.96" size="1.778" layer="95"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="213.36" y1="-187.96" x2="203.2" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-182.88" x2="213.36" y2="-187.96" width="0.1524" layer="91"/>
<junction x="213.36" y="-187.96"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA12/TC2-0/TCC0-6/SER2-0/SER4-1"/>
<wire x1="147.32" y1="45.72" x2="162.56" y2="45.72" width="0.1524" layer="91"/>
<label x="152.4" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="D_D" class="0">
<segment>
<pinref part="Q4" gate="1" pin="D"/>
<pinref part="J10" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-88.9" x2="246.38" y2="-81.28" width="0.1524" layer="91"/>
<label x="246.38" y="-86.36" size="1.778" layer="95" rot="R90"/>
<pinref part="D8" gate="G$1" pin="A"/>
<wire x1="251.46" y1="-88.9" x2="246.38" y2="-88.9" width="0.1524" layer="91"/>
<junction x="246.38" y="-88.9"/>
</segment>
</net>
<net name="D_E" class="0">
<segment>
<pinref part="Q5" gate="1" pin="D"/>
<pinref part="J11" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-134.62" x2="246.38" y2="-127" width="0.1524" layer="91"/>
<label x="246.38" y="-132.08" size="1.778" layer="95" rot="R90"/>
<pinref part="D9" gate="G$1" pin="A"/>
<wire x1="251.46" y1="-134.62" x2="246.38" y2="-134.62" width="0.1524" layer="91"/>
<junction x="246.38" y="-134.62"/>
</segment>
</net>
<net name="D_F" class="0">
<segment>
<pinref part="Q6" gate="1" pin="D"/>
<pinref part="J12" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-180.34" x2="246.38" y2="-172.72" width="0.1524" layer="91"/>
<label x="246.38" y="-177.8" size="1.778" layer="95" rot="R90"/>
<pinref part="D10" gate="G$1" pin="A"/>
<wire x1="251.46" y1="-180.34" x2="246.38" y2="-180.34" width="0.1524" layer="91"/>
<junction x="246.38" y="-180.34"/>
</segment>
</net>
<net name="THERM_A" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="299.72" y1="93.98" x2="299.72" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="1"/>
<wire x1="299.72" y1="88.9" x2="317.5" y2="88.9" width="0.1524" layer="91"/>
<wire x1="299.72" y1="88.9" x2="284.48" y2="88.9" width="0.1524" layer="91"/>
<junction x="299.72" y="88.9"/>
<label x="284.48" y="88.9" size="1.778" layer="95"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="299.72" y1="86.36" x2="299.72" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA04/VREFB/ACD0-4/TC0-0/SER0-0"/>
<wire x1="147.32" y1="66.04" x2="165.1" y2="66.04" width="0.1524" layer="91"/>
<label x="152.4" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="THERM_B" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="299.72" y1="30.48" x2="299.72" y2="25.4" width="0.1524" layer="91"/>
<pinref part="J14" gate="G$1" pin="1"/>
<wire x1="299.72" y1="25.4" x2="317.5" y2="25.4" width="0.1524" layer="91"/>
<wire x1="299.72" y1="25.4" x2="284.48" y2="25.4" width="0.1524" layer="91"/>
<junction x="299.72" y="25.4"/>
<label x="284.48" y="25.4" size="1.778" layer="95"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="299.72" y1="22.86" x2="299.72" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA05/DAC-1/ADC0-5/TC0-1/SER0-1"/>
<wire x1="147.32" y1="63.5" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<label x="152.4" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="213.36" y1="88.9" x2="213.36" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="213.36" y1="25.4" x2="213.36" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="213.36" y1="-35.56" x2="213.36" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="D11" gate="G$1" pin="A"/>
<wire x1="213.36" y1="-81.28" x2="213.36" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="D12" gate="G$1" pin="A"/>
<wire x1="213.36" y1="-127" x2="213.36" y2="-124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="D13" gate="G$1" pin="A"/>
<wire x1="213.36" y1="-172.72" x2="213.36" y2="-170.18" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
