## Online Measurement for Parameter Discovery in FFF 

In order to automatically guess at print parameters (flowrate and nozzle temperatures), we developed an extruder that features a filament sensor (to detect real flowrates) and a pressure sensor. We used it to measure a swath of that parameter space using a simple routine, fit that data to a predictive function, and use a meta-heuristic to extract real parameters from those functions. We were then able to automatically determine print parameters for a number of new filaments that we had not previously tested. 

![extruder](images/extruder-labelled.png)
> Above is our extruder, featuring a loadcell to measure pressure (C), a filament sensor (A) that measures filament width and linear feedrate, and a COTS hotend (D) and drive gears (B). 

![individuals](images/individual-fits-colorblind.png)
> We collect data with a simple routine that sets the nozzle at its maximum temperature, then begins flowing plastic at a set rate, and turns off the heater. Pressure and temperature data are simultaneously collected as the nozzle cools (and as pressure increases). 

![contour](images/contour-fit-abs-08-colorblind.png)
> We can expand this fit into a full contour of the parameter space, and then select operating points using a meta heuristic. 

![method](images/methods-summary.png)

![outputs](images/outputs.png)
> Using this method, we were able to produce a series of test articles using filaments that we had not previously tested. 

### In this Repo

**Analysis and Data**

We have data an analysis in the [analysis/](analysis) folder, and data sets stored in [data/](analysis/flows/spindown). Analysis codes (Jupyter Notebooks) run with these data. 

Data there is organized into `material-nozzleSize` folders, with raw data in `.json` format and cleaned data sets available as python pickles, which import as pandas dataframes. Data was cleaned using [this script](analysis/data-cleanup.ipynb) and analyzed using [this one](analysis/data-fitting.ipynb). 

**Experimental System**

The [system folder](system) contains the javascript controller as well as firmwares for the relevant hardware. 

**The Paper**

A draft of the paper is located [here](paper/Online_Measurement_for_Parameter_Discovery_in_FFF_2023-11-01.pdf).

